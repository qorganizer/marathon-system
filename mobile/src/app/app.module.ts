import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";

import { APP_CONFIG, ApplicationConfig } from "./app.config";

import { NgModule, ErrorHandler, isDevMode } from "@angular/core";
import { HttpModule, Http } from "@angular/http";
import { BrowserModule } from "@angular/platform-browser";
import { IonicApp, IonicModule, IonicErrorHandler, DeepLinkConfig } from "ionic-angular";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { MyApp } from "./app.component";

import { SocketStompConfig, SockJStompService } from "../../../common/src/services/socket";
import { AppConfigurer } from "../../../common/src/config/app";
import { RESTClient, SocketClient, ModelStore, QNavigator, BusinessService, OAuthService } from "../../../common/src/services";
import { IonicStorageModule } from '@ionic/storage';
import { providers, components } from "../../../common/src/common.module";

import { SessionsTeacher } from "../../../common/src/views/sessions/sessions.teacher";
import { SessionsStudent } from "../../../common/src/views/sessions/sessions.student";
import { SessionsAdmin } from "../../../common/src/views/sessions/sessions.admin";
import { SessionTeacher } from "../../../common/src/views/session/session.teacher";
import { SessionStudent } from "../../../common/src/views/session/session.student";
import { SessionAdmin } from "../../../common/src/views/session/session.admin";
import { SessionAttendance } from "../../../common/src/views/session-attendance";
import { DashboardTeacher } from "../../../common/src/views/dashboard/dashboard.teacher";
import { DashboardStudent } from "../../../common/src/views/dashboard/dashboard.student";
import { DashboardAdmin } from "../../../common/src/views/dashboard/dashboard.admin";
import { Login, Main, CoursesView, CourseView, BlockView, Register, Users, CreateSessionView, UserView, SessionDisplay, ActivitiesView, ActivityEditView, TrueFalseView, SingleChoiceView, MultipleChoiceView, SliderView, RubricView } from "../../../common/src/views";
import { QPresenter } from "../../../common/src/components/q-presenter";
import { QuestionChooser } from "../../../common/src/components/question-chooser";
import { ActivityChooser } from "../../../common/src/components/activity-chooser";
import { ADALService } from "../services/adal-service";

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: Http) {
	return new TranslateHttpLoader(http, ApplicationConfig.i18n_prefix, ApplicationConfig.i18n_suffix);
}

let cmps = [
	MyApp,
	Login,
	Main,
	CoursesView,
	CourseView,
	Register,
	BlockView,
	Users,
	QPresenter,
	DashboardStudent,
	DashboardTeacher,
	DashboardAdmin,
	SessionsTeacher,
	SessionsStudent,
	SessionsAdmin,
	SessionTeacher,
	SessionStudent,
	SessionAdmin,
	CreateSessionView,
	UserView,
	SessionAttendance,
	SessionDisplay,
	ActivitiesView, 
	ActivityEditView, 
	TrueFalseView, 
	SingleChoiceView, 
	MultipleChoiceView, 
	SliderView, 
	RubricView
];

let provs = [
	{ provide: ErrorHandler, useClass: IonicErrorHandler },
	{ provide: OAuthService, useClass: ADALService },
	StatusBar,
	SplashScreen,
	{
		provide: APP_CONFIG,
		useValue: ApplicationConfig
	},
	RESTClient,
	SocketClient,
	ModelStore,
	QNavigator,
	AppConfigurer,
	BusinessService
];

@NgModule({
	declarations: cmps,
	imports: [
		BrowserModule,
		IonicModule.forRoot(MyApp),
		IonicStorageModule.forRoot({
			name: 'qdb'
		}),
		HttpModule,
		TranslateModule.forRoot({
			loader: {
				provide: TranslateLoader,
				useFactory: HttpLoaderFactory,
				deps: [Http]
			}
		})
	],
	bootstrap: [IonicApp],
	entryComponents: cmps,
	providers: provs
})
export class AppModule { }
