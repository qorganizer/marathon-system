import { InjectionToken } from "@angular/core";
export interface AppConfig{
	restUrl: any;
	socketUrl: any;
	i18n_prefix: any;
	i18n_suffix: any;
	oauth: any;

}
export let APP_CONFIG = new InjectionToken<AppConfig>("app.config");
export const ApplicationConfig:AppConfig = {"restUrl":"https://52.174.178.62/api","socketUrl":"https://52.174.178.62/qplayer","i18n_prefix":"https://52.174.178.62/assets/i18n/","i18n_suffix":".json","oauth":{"tenant":"common","clientId":"62d3f3c2-a637-4195-85c5-422835cc404a","appIdUri":"https://mirceaappniv.onmicrosoft.com/c3c25396-085b-4ba9-a6ce-b49f41e9104d","authority":"https://login.windows.net/common","resourceUri":"https://graph.windows.net","redirectUrl":"https://52.174.178.62"}};
