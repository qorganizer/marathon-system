import { Component, Inject, ViewChild } from "@angular/core";
import { Platform, Nav } from "ionic-angular";
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import {Login, Main} from "../../../common/src/views";
import {AppConfigurer} from "../../../common/src/config/app";
import { APP_CONFIG, AppConfig } from "./app.config";
import {QNavigator} from "../../../common/src/services";
@Component({
	templateUrl: "app.html"
})
export class MyApp {
	
	@ViewChild(Nav) nav: Nav;

	constructor(platform: Platform, 
				private navigator: QNavigator,
				@Inject(APP_CONFIG) private config: AppConfig,
				private setupper: AppConfigurer,
				statusBar: StatusBar, splashScreen: SplashScreen) {
		this.setupper.initializeWithConfig(this.config);
		platform.ready().then(() => {
			 //do something else
			statusBar.styleDefault();
			splashScreen.hide();
		});
	}

	ngAfterViewInit(){ 
		this.setupper.isAuthenticated().subscribe((isAuth) => {
			if(!isAuth){
				this.navigator.setRoot(this.nav, Login);
			}else{
				this.navigator.setRoot(this.nav, Main);
			}
		})
	}
}