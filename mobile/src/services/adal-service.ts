import { Injectable } from "@angular/core";
import { Storage } from '@ionic/storage';
import { Observable, Observer, BehaviorSubject } from "rxjs/Rx";

@Injectable()
export class ADALService {
	private config: any;
	private authContext: Microsoft.ADAL.AuthenticationContext;

	constructor(private storage: Storage) {

	}

	initialize(config: any) {
		if(!this.authContext){
			this.config = config;
			this.authContext = new Microsoft.ADAL.AuthenticationContext(config.authority);
		}
	}

	private authenticate(authCompletedCallback, errorCallback) {
		let t = this;
		// Attempt to authorize user silently
		t.authContext.acquireTokenSilentAsync(t.config.resourceUri, t.config.clientId, null)
			.then(authCompletedCallback, function () {
				// We require user credentials so triggers authentication dialog
				t.authContext.acquireTokenAsync(t.config.resourceUri, t.config.clientId, t.config.redirectUrl)
					.then(authCompletedCallback, errorCallback);
			});
	};

	signIn(): Observable<any>{
		return new Observable<any>( obs => {
			let s = this.storage;
			this.authenticate((resp) => {
				console.log(JSON.stringify(resp, null, 2));			
				s.ready().then(()=>{
					s.set("oauth", 1);
					s.set("auth-token", "Azure_Token " + resp.accessToken);
					s.set("auth-error-desc", "");
					s.set("auth-error", "");
					obs.next("Azure_Token " + resp.accessToken);
					obs.complete();
				});
			}, (err) => {
				obs.error(err);
			});
		});		
	}

	signOut(){
		this.authContext.tokenCache.clear();
	}

	isOauthEnabled(): Observable<boolean>{
		return new Observable<boolean>(obs => {
			this.storage.get("oauth").then(oauth => {
				obs.next(oauth == 1);
				obs.complete();
			});
		});
	}

	isAuthenticated(): Observable<boolean>{
		return new Observable<boolean>(obs => {
			this.authContext.tokenCache.readItems().then(function (items) {
				obs.next(items.length > 0);
				obs.complete();
			});
		});
	}
}