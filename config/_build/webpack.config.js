var path = require('path');
var webpack = require('webpack');
var TsConfigPathsPlugin = require('awesome-typescript-loader').TsConfigPathsPlugin;
var ionicWebpackFactory = require(process.env.IONIC_WEBPACK_FACTORY);
module.exports = {
	entry: process.env.IONIC_APP_ENTRY_POINT,
	output: {
		path: '{{BUILD}}',
		publicPath: 'build/',
		filename: process.env.IONIC_OUTPUT_JS_FILE_NAME,
		devtoolModuleFilenameTemplate: ionicWebpackFactory.getSourceMapperFunction(),
	},
	devtool: process.env.IONIC_SOURCE_MAP_TYPE,

	stats: "verbose",

	resolve: {
		extensions: ['.ts', '.js', '.json'],
		modules: [path.resolve('../node_modules')],
		alias: {
			commons: path.resolve('../common/src')
		}
	},

	module: {
		loaders: [
			{
				test: /\.json$/,
				loader: 'json-loader'
			},
			{
				test: /\.ts$/,
				loader: process.env.IONIC_WEBPACK_LOADER
			},
			{
				test: /\.js$/,
				loader: process.env.IONIC_WEBPACK_TRANSPILE_LOADER
			}
		]
	},

	plugins: [
		ionicWebpackFactory.getIonicEnvironmentPlugin(),
		new webpack.LoaderOptionsPlugin({
			debug: true
		})
	],

	// Some libraries import Node modules but don't use them in the browser.
	// Tell Webpack to provide empty mocks for them so importing them works.
	node: {
		fs: 'empty',
		net: 'empty',
		tls: 'empty'
	}
};
