#!/usr/bin/env node
var path = require('path');
var process = require('process');
var fs = require('fs');
var target = path.basename(process.cwd());

class Environment {
  constructor(args) {
    args = JSON.parse(args).original;
    const defaultEnv = 'dev'; //Set default environment
    let env;
    
    if(args.includes('build') || args.includes("build:prod")) {
      let envFlags = args.filter((arg) => arg.includes("--env"));
      env = (envFlags.length === 1) ? envFlags[0].substr(envFlags[0].indexOf("=") + 1) : defaultEnv;
    } else {
      let index = args.indexOf('--env');
      env = (index > -1) ? args[index+1] : defaultEnv;
    }

    console.log(`Using environment config: ${env}`);
    this.setEnvironment(env);
  }

  setEnvironment(env) {
    let config;
    
    try {
      config = require(path.resolve(path.join(process.cwd(), '../', 'config', 'env', target, env + '.json')));
    } catch(e) {
      throw new Error(`The config file for this environment is missing (${e.message})`);
    }

    var wstream = fs.createWriteStream(path.resolve('./src/app/app.config.ts'));
    wstream.write(`import { InjectionToken } from "@angular/core";
export interface AppConfig{
${this.writeInterfaceMembers(config)}
}
export let APP_CONFIG = new InjectionToken<AppConfig>("app.config");
export const ApplicationConfig:AppConfig = ${JSON.stringify(config)};
export function configHasRedirect(name: string){
	return ApplicationConfig.redirects && ApplicationConfig.redirects[name];
}
`);
    wstream.end();
  }
  writeInterfaceMembers(config){
	  let result = "";
	  Object.keys(config).forEach(function(entry) {
			result += `\t${entry}: any;\r\n`;

	  });
	  return result;
  }
}

new Environment(process.env.npm_config_argv);