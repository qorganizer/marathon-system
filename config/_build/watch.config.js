var watch = require('@ionic/app-scripts/dist/watch');
console.log("Watch");
console.log(watch);
module.exports = {
	srcFiles: {
		paths: ['{{SRC}}/**/*.(ts|html|s(c|a)ss)', "{{SRC}}/../../common/src/**/*.(ts|html|s(c|a)ss)"],
		options: { ignored: ['{{SRC}}/**/*.spec.ts', '{{SRC}}/**/*.e2e.ts', '**/*.DS_Store', '{{SRC}}/index.html'] },
		callback: watch.buildUpdate
	}
};