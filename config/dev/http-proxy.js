var fs = require('fs'),
	https = require("https"),
    httpProxy = require('http-proxy'),
	forge = require('node-forge');

var proxy = httpProxy.createServer({
  target: {
    host: 'localhost',
    port: 8101
  },
  ssl: {
    key: fs.readFileSync('./src/main/resources/qplayer.key.pem', 'utf8'),
    cert: fs.readFileSync('./src/main/resources/qplayer.crt.pem', 'utf8')
  }
});
proxy.listen(8102);
proxy.on('error', function(err, req, res){
	if(err) console.log(err);
	res.writeHead(500);
	res.end('Ooops, something went very wrong on port 8102...');
});
console.log("Proxy started on 8102");

/*proxy = httpProxy.createServer({
  target: 'wss://localhost:35729',
  ws: true
});
proxy.on('error', function(err, req, res){
	if(err) console.log(err);
	res.writeHead(500);
	res.end('Ooops, something went very wrong on port 35739...');
})*/

setTimeout(function(){
	
	var proxy = new httpProxy.createProxyServer({
	target: "wss://localhost:35729",
	ws: true
	});

	proxy.on('error', function(e) {
		console.error(e);	
	});

	var proxyServer = https.createServer({
		key: fs.readFileSync('./src/main/resources/qplayer.key.pem', 'utf8'),
		cert: fs.readFileSync('./src/main/resources/qplayer.crt.pem', 'utf8')
	}, function (req, res) {
		proxy.web(req, res);
	});

	//
	// Listen to the `upgrade` event and proxy the
	// WebSocket requests as well.
	//
	proxyServer.on('upgrade', function (req, socket, head) {
		proxy.ws(req, socket, head);
	});

	proxyServer.on('error', function(err, req, res){
		if(err) console.log(err);
		res.writeHead(500);
		res.end('Ooops, something went very wrong on port 35739...');
	})

	proxyServer.listen(35739);

	console.log("Second proxy started on 35739");
}, 20000);
