import {Block, Task} from "./";

export class LearningObject{
	activityType: string;
	author: string;
	blockId: number;
	color: string;
	displayName: string;
	id: string;
	json: string;
	learningObjectId: number;
	sortOrder: number;
	state: string;
	taskId: number;
	task: Task;
	block: Block;
}