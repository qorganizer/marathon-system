export class Answer{
	public activityId: string;
	public questionIndex: number;
	public sessionId: string;
	public sessionMode: string;
	public nativeQuiz: boolean;
	public timer: number = 0;
	public answerKeys: number[];
	public answered?: boolean;
}