import { Activity } from './quiz';
export class DashNotifier{
	time: number;
	timeDisplay: string;
	notificationReceived: boolean;

	constructor(time: number, timeDisplay: string, notificationReceived?: boolean){
		this.time = time;
		this.timeDisplay = timeDisplay;
		this.notificationReceived = notificationReceived == true;
	}
}

export class SessionMenuStructure{
	public menus: Menu[];
	public ledOn: boolean = true;
}

export class Menu{
	public id: string;
	public handler?: Function;
	public icon: string;
	public cssClass: string;
	public subMenus: SubMenu[];
	public state: MenuState = MenuState.ENABLED;
}

export class SubMenu{
	public id: string;
	public handler: Function;
	public icon: string;
	public cssClass?: string;
	public state: MenuState = MenuState.ENABLED;
}

export class MenuState{
	public static SELECTED: MenuState = new MenuState("selected");
	public static ENABLED: MenuState = new MenuState("enabled");
	public static DISABLED: MenuState = new MenuState("disabled");
	public static IN: MenuState = new MenuState("in");
	
	constructor(public name: string){

	}

	public toString(){
		return this.name;
	}
}


export class EmitterMessage{
	public static EXIT: string = "exit";
	public static GOTOTYPE: string = "goToType";
	public static SAVE: string = "save";
	public static OUT: string = "out";
	public static PRESENT: string = "present";
	public static RESULTS: string = "results";
	public static DASH: string = "dash";

	public static exit(): EmitterMessage{
		return new EmitterMessage(EmitterMessage.EXIT, null);
	}

	public static dash(data: DashNotifier): EmitterMessage{
		return new EmitterMessage(EmitterMessage.DASH, data);
	}

	public static out(): EmitterMessage{
		return new EmitterMessage(EmitterMessage.OUT, null);
	}

	public static save(activity: Activity): EmitterMessage{
		return new EmitterMessage(EmitterMessage.SAVE, activity);
	}

	public static present(activity: Activity): EmitterMessage{
		return new EmitterMessage(EmitterMessage.PRESENT, activity);
	}

	public static results(activity: Activity): EmitterMessage{
		return new EmitterMessage(EmitterMessage.RESULTS, activity);
	}

	public static goToType(data: any): EmitterMessage{
		return new EmitterMessage(EmitterMessage.GOTOTYPE, data);
	}

	constructor(public type: string, public data: any){

	}
}