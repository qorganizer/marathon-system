export class Course{
	public courseId : string;
    public id : string;
    public level : string;
    public levelId : number;
    public stream : string;
    public streamId : number;
    public subject : string;
    public subjectId : number;
}