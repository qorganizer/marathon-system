import { Course} from "./course";

export class Session{
	public active: true;
	public course: Course;
	public created: string;
	public creatorId: string;
	public id: string;
	public network: string;
	public school: string;
	public sessionCode: string;
	public sessionName: string;
	public teachers: string[];
}

export class SessionMode{
	public static Individual = new SessionMode("INDIVIDUAL");
	public static Class = new SessionMode("CLASS");

	constructor(private name: string){

	}

	public toString(): string{
		return this.name;
	}
}