export class Block{
	public id: string;
	public blockId: number;
	public course: string;
	public subjectId: number;
	public levelId: number;
	public streamId: number;
	public title: string;
	public displayName: string;
	public created: Date;
	public modified: Date;
}