import { Question, Activity, LearningObject, UserProfile } from "./";

export class ActivitySummary {
	public averageCompletion: number = 0;
	public averageScore: number = 0;
	public percentageOfResponders: number = 0;
}

export class QuestionSummary {
	public answerDistribution: {[key: number]: number};
	public question: Question;
	public sliderOptionsAnswerDistribution: {[key: number]: {[key: number]: number}};
	public sliderOptionsStudentsDistribution: {[key: number]: {[key: number]: number}};
	public studentsDistribution: {[key: number]: UserProfile};
}

export class AnswerEvaluation {
	public answered: boolean;
	public keySelection: number[];
	public score: Score;
	public timer: number;
}

export class Score {
	public scorePercentage: number;
	public selectedCorrect: number;
	public selectedIncorrect: number;
	public totalCorrect: number;
}

export class UserEvaluation {
	public activity: Activity;
	public answers: AnswerEvaluation[];
	public completed: boolean;
	public contentQuiz: LearningObject;
	public createdAt: string;
	public deleted: boolean;
	public finalScorePercentage: number;
	public id: string;
	public nativeQuiz: boolean;
	public responderId: string;
	public sessionId: string;
	public sessionMode: string;
	public totalTimer: number;
	public updatedAt: string;
}