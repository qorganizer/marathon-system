export class Profile{
  public address: string;
  public avatar: string;
  public civilId: string;
  public classes: string[];
  public dob: string;
  public email: string;
  public firstName: string;
  public gender: string;
  public id: ProfileId
  public lastName: string;
  public network: string;
  public password: string;
  public phone: string;
  public role: any;
  public school: string;
  public stream: string;
  public userName: string;
}

export class ProfileId{
	public counter: number;
    public machineIdentifier: number;
    public processIdentifier: number;
    public timestamp: number;
}