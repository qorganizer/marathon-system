export class PagedResponse<T>{
	content: T[];
	totalElements: number;
	totalPages: number;
	last: boolean;
	size: number;
	number: 0;
	sort: SortInfo[];
	numberOfElements: number;
	first: boolean
}

class SortInfo {
	direction: string;
	property: string;
	ignoreCase: boolean;
	nullHandling: string;
	ascending: boolean;
	descending: boolean
}