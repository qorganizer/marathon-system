export class UserRequest{
	public firstName: string;
	public lastName: string;
	public password: string;
	public roleId: string;
	public userName: string;
}