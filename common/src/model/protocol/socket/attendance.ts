import {UserProfile} from "../../user-profile";

export class AttendanceMessage{

	constructor(public online: boolean, public profile: UserProfile){

	}
}