import { LearningObject } from '../../learning-object';
import {AttentionMessage} from "./attention";
import {AttendanceMessage} from "./attendance";
import {UserProfile} from "../../user-profile";
import {Activity} from "../../quiz";

export class NotificationMessage {

	public static fromString(str: string): NotificationMessage{
		let json = JSON.parse(str);
		let msg = new NotificationMessage(null, null, json.active == true);
		msg.type = NotificationMessageType.fromString(json.type);
		console.log(json.value);
		try{
			msg.value = JSON.parse(json.value);
		}catch(e){
			msg.value = json.value;
		}
		return msg;
	}

	public static attention(active: boolean): NotificationMessage{
		return new NotificationMessage(NotificationMessageType.ATTENTION, new AttentionMessage(), active);
	}

	public static shareScreen(lo: LearningObject, active: boolean): NotificationMessage{
		return new NotificationMessage(NotificationMessageType.SHARE_SCREEN, lo.id, active);
	}

	public static attendance(online: boolean, profile: UserProfile): NotificationMessage{
		return new NotificationMessage(NotificationMessageType.ATTENDANCE, new AttendanceMessage(online, profile));
	}

	public static activity(active: boolean, activity: Activity): NotificationMessage{
		return new NotificationMessage(NotificationMessageType.ACTIVITY, activity.id, active);
	}

	

	type: NotificationMessageType;
	value: any;
	active: boolean;

	constructor(type : NotificationMessageType, value: any, active?: boolean) {
		this.type = type;
		this.value = value;		
		this.active = active;
	}

	stringify(): string{
		return JSON.stringify({type: this.type.toString(), value: JSON.stringify(this.value), active: this.active})
	}
	
}

export class NotificationMessageType{

	public static fromString(str: string): NotificationMessageType{
		return NotificationMessageType.map[str];
	}

	private static map: {[value: string]: NotificationMessageType} = {};

	public static ATTENTION = new NotificationMessageType("ATTENTION");
	public static SHARE_SCREEN = new NotificationMessageType("SHARE_SCREEN");
	public static ACTIVITY = new NotificationMessageType("ACTIVITY");
	public static ATTENDANCE = new NotificationMessageType("ATTENDANCE");
	public static ANSWER = new NotificationMessageType("ANSWER");

	constructor(private name: string){
		NotificationMessageType.map[name] = this;
	}

	public toString(): string{
		return this.name;
	}
}