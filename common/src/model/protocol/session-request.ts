export class SessionRequest{
	course: CourseRequest = new CourseRequest();
	network: string;
	school: string;
	teachers: string[] = [];
	session: string;
}

export class CourseRequest{
	level: string;
	stream: string;
	subject: string;
}