import {Block} from "./";

export class Task{
	public id: string;
	public blockId: number;
	public block: Block;
	public title: string;
	public displayName: string;
	public created: Date;
	public modified: Date;
}