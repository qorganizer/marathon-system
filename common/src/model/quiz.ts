export class Activity{
	activityType: "Quiz" | "LikeDislike" | "Poll" | "CheckList" | "Slider" | "Rubric";
	id: string;
	authorId: string;
	contextId: string;
	sessionId: string;
	questions: Question[] = [];
	createdAt: string;
	updatedAt: string;
}

export class ActivityType{
	public static QUIZ_TYPE: string = "Quiz";
	public static LIKEDISLIKE_TYPE: string = "LikeDislike";
	public static POLL_TYPE: string = "Poll";
	public static CHECKLIST_TYPE: string = "CheckList";
	public static SLIDER_TYPE: string = "Slider";
	public static RUBRIC_TYPE: string = "Rubric";
	
	public static QUIZ: ActivityType = new ActivityType(ActivityType.QUIZ_TYPE, "Quiz", true);
	public static LIKEDISLIKE: ActivityType = new ActivityType(ActivityType.LIKEDISLIKE_TYPE, "Like/Dislike", false);
	public static POLL: ActivityType = new ActivityType(ActivityType.POLL_TYPE, "Poll", false);
	public static CHECKLIST: ActivityType = new ActivityType(ActivityType.CHECKLIST_TYPE, "Check list", false);
	public static SLIDER: ActivityType = new ActivityType(ActivityType.SLIDER_TYPE, "Slider", false);
	public static RUBRIC: ActivityType = new ActivityType(ActivityType.RUBRIC_TYPE, "Rubric", false);


	public static asArray(): ActivityType[]{
		return [
			ActivityType.QUIZ,
			ActivityType.LIKEDISLIKE,
			ActivityType.POLL,
			ActivityType.CHECKLIST,
			ActivityType.SLIDER,
			ActivityType.RUBRIC
		]
	}

	public static labelFromName(name: string): string{
		let type = ActivityType.asArray().find((val, index, list) => {
			return val.name == name;
		});
		return type ? type.label : "";
	}

	constructor(private name: string, public label: string, public hasMultipleQuestions: boolean){

	}

	public toString(): string{
		return this.name;
	}
}

export class QuestionType{
	public static TRUE_FALSE_TYPE: string = "TrueFalse";
	public static SINGLE_CHOICE_TYPE: string = "SingleChoice";
	public static MULTIPLE_CHOICE_TYPE: string = "MultipleChoice";
	public static SLIDER_TYPE: string = "Slider";
	public static RUBRIC_TYPE: string = "Rubric";
	public static LIKEDISLIKE_TYPE: string = "LikeDislike";
	public static POLL_TYPE: string = "Poll";
	public static CHECKLIST_TYPE: string = "CheckList";
	
	public static TRUE_FALSE: QuestionType = new QuestionType(QuestionType.TRUE_FALSE_TYPE, "True or False");
	public static SINGLE_CHOICE: QuestionType = new QuestionType(QuestionType.SINGLE_CHOICE_TYPE, "Single Choice");
	public static MULTIPLE_CHOICE: QuestionType = new QuestionType(QuestionType.MULTIPLE_CHOICE_TYPE, "Multiple Choice");
	public static SLIDER: QuestionType = new QuestionType(QuestionType.SLIDER_TYPE, "Slider");
	public static RUBRIC: QuestionType = new QuestionType(QuestionType.RUBRIC_TYPE, "Rubric");
	public static LIKEDISLIKE: QuestionType = new QuestionType(QuestionType.LIKEDISLIKE_TYPE, "Like Dislike");
	public static POLL: QuestionType = new QuestionType(QuestionType.POLL_TYPE, "Poll");
	public static CHECKLIST: QuestionType = new QuestionType(QuestionType.CHECKLIST_TYPE, "Check List");

	public static asArray(type: string): QuestionType[]{
		switch(type){
			case ActivityType.CHECKLIST_TYPE:
				return [
					QuestionType.CHECKLIST];
			case ActivityType.RUBRIC_TYPE:
				return [
					QuestionType.RUBRIC];
			case ActivityType.LIKEDISLIKE_TYPE:
				return [
					QuestionType.LIKEDISLIKE];
			case ActivityType.POLL_TYPE:
				return [
					QuestionType.POLL];
			case ActivityType.SLIDER_TYPE:
				return [
					QuestionType.SLIDER
				];
			default:
				return [
					QuestionType.TRUE_FALSE,
					QuestionType.SINGLE_CHOICE,
					QuestionType.MULTIPLE_CHOICE
				]
		}
	}

	constructor(private name: string, public label: string){

	}

	public toString(): string{
		return this.name;
	}


}

export class Question{
	question: string;
	questionType: "TrueFalse" | "SingleChoice" | "MultipleChoice";
	distracters: Distracter[];
}

export class Distracter{
	key: number = null;
	distracter: string = "";
	correct: boolean = false;
	feedback?: string = "";
	sliderRubricOptions?: SliderRubric[] = null;
}

export class SliderRubric{
	key: number;
	label: string = "";
	correct: boolean;
	feedback?: string;
}

export class ActivityChangeHistory{
	activity: Activity;
	edited: ChangeItem[] = [];
	added: ChangeItem[] = [];
	deleted: ChangeItem[] = [];
}

export class ChangeItem{
	question: Question;
	index: number;
}