import { UserRole } from "./role"; 

export class UserProfile {
	public address: string;
	public avatar: string;
	public civilId: string;
	public dob: string;
	public email: string;
	public firstName: string;
	public gender: string;
	public lastName: string;
	public password: string;
	public phone: string;
	public role: UserRole;
	public network: string;
	public school: string;
	public stream: string;
	public userName: string;
	public id: string;
	public inSession?: boolean;
	public external: boolean;
	public accesibleLevelIds: number[] = [];
	public sessions: string[] = [];
}