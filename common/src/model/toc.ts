export class TableOfContents{
	blocks: TOCBlock[];
}

export class TOCBlock{
 	blockId: string;
	title: string;
	description:string;
	subject: string;
	stream: string;
	level: string;
	tasks: TOCTask[] = [];
}

export class TOCTask{
	taskId: string;
	title: string;
	description: string;
	los: TOCLearningObject[] = [];
}

export class TOCLearningObject{
	loId: string;
	title: string;
	index: number;
	color: string;
}