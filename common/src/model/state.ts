import {LearningObject} from "./";

export class SessionState{
	currentLO: LearningObject;
}

export class SessionActiveState{
	active: boolean;
	presenterId: string;
	sessionId: string;
	state: SessionActiveStateEntry[];
}

export class SessionActiveStateEntry{
	active: boolean;
	type: string;
	value: any;
}