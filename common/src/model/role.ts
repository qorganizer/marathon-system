export class UserRole{
	id: string;
    permissions: string[];
    publisher: boolean;
    roleName: string;
}