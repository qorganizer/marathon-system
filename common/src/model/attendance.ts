import { UserProfile } from "./user-profile";

export class Attendance{
	activeUsers: UserProfile[];
	inactiveUsers: UserProfile[];
}