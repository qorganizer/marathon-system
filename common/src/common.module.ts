import { NgModule } from "@angular/core";
import { HttpModule } from "@angular/http";
import { BrowserModule } from "@angular/platform-browser";
import { IonicPageModule, DeepLinkConfig } from "ionic-angular";

import { SessionsTeacher } from "./views/sessions/sessions.teacher";
import { SessionsStudent } from "./views/sessions/sessions.student";
import { SessionsAdmin } from "./views/sessions/sessions.admin";
import { SessionTeacher } from "./views/session/session.teacher";
import { SessionStudent } from "./views/session/session.student";
import { SessionAdmin } from "./views/session/session.admin";
import { SessionAttendance } from "./views/session-attendance";
import { DashboardTeacher } from "./views/dashboard/dashboard.teacher";
import { DashboardStudent } from "./views/dashboard/dashboard.student";
import { DashboardAdmin } from "./views/dashboard/dashboard.admin";
import { Login, Main, CoursesView, CourseView, BlockView, Register, Users, CreateSessionView, UserView, SessionDisplay, ActivitiesView, ActivityEditView, TrueFalseView, SingleChoiceView, MultipleChoiceView, SliderView, RubricView } from "./views";
import { AppConfigurer } from "./config/app";
import { QPresenter } from "./components/q-presenter";
import { RESTClient, SocketClient, ModelStore, QNavigator, BusinessService } from "./services";
import { SocketStompConfig, GetSocketConfig, SockJStompService } from "./services/socket";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";

export const components: any[] = [
	Login,
	Main,
	CoursesView,
	CourseView,
	Register,
	BlockView,
	Users,
	QPresenter,
	DashboardStudent,
	DashboardTeacher,
	DashboardAdmin,
	SessionsTeacher,
	SessionsStudent,
	SessionsAdmin,
	SessionTeacher,
	SessionStudent,
	SessionAdmin,
	CreateSessionView,
	UserView,
	SessionAttendance,
	SessionDisplay,
	ActivitiesView, 
	ActivityEditView, 
	TrueFalseView, 
	SingleChoiceView, 
	MultipleChoiceView, 
	SliderView, 
	RubricView
];

export const providers: any[] = [
	RESTClient,
	SocketClient,
	ModelStore,
	QNavigator,
	AppConfigurer,
	BusinessService
];

@NgModule({
	declarations: components,
	imports: [
		BrowserModule,
		IonicPageModule.forChild(Login),
		HttpModule,
		TranslateModule.forChild()
	],
	entryComponents: components,
	providers: providers
})
export class CommonModule { }

export const deepLinkConfig: DeepLinkConfig = {
	links: [
		{ component: Login, name: "Login Page", segment: "login" },
		{ component: Register, name: "Register Page", segment: "register" },
		{ component: Users, name: "Users Page", segment: "users" },
		{ component: CoursesView, name: "Courses Page", segment: "courses" },
		{ component: CourseView, name: "Course Page", segment: "courses/:cid", defaultHistory: [CoursesView] },
		{ component: BlockView, name: "Course Page", segment: "courses/:cid/blocks/:bid", defaultHistory: [CoursesView, CourseView] },
		{ component: DashboardAdmin, name: "Home Page", segment: "home" },
		{ component: DashboardTeacher, name: "Home Page", segment: "home" },
		{ component: DashboardStudent, name: "Home Page", segment: "home" },
		{ component: SessionsAdmin, name: "Sessions Page", segment: "sessions" },
		{ component: SessionsTeacher, name: "Sessions Page", segment: "sessions" },
		{ component: SessionsStudent, name: "Sessions Page", segment: "sessions" },
		{ component: SessionAdmin, name: "Session Page", segment: "sessions/:sid" },
		{ component: SessionTeacher, name: "Session Page", segment: "sessions/:sid" },
		{ component: SessionStudent, name: "Session Page", segment: "sessions/:sid" },
		{ component: ActivitiesView, name: "Quizes Page", segment: "quizes" },
		{ component: SessionAttendance, name: "Attendance", segment: "attendance"},
		{ component: SessionDisplay, name: "Session Display", segment: "presenter" }
	]

};
