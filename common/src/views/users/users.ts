import { Component } from "@angular/core";
import { ModalController, ActionSheetController, NavController, AlertController } from "ionic-angular";
import { RESTClient, ModelStore} from "../../services";
import { UserProfile } from "../../model";
import { AuthenticatedView } from "../auth-view";
import { UserView } from "../";

@Component({
  selector: "users",
  templateUrl: "users.html"
})
export class Users extends AuthenticatedView{

	loading: boolean = true;

	users: UserProfile[];

	constructor(private client: RESTClient, 
				private model: ModelStore, 
				private navCtrl: NavController,
				private modalCtrl: ModalController,
				private sheetCtrl: ActionSheetController,
				private alertCtrl: AlertController){
		super();
	}

	ionViewDidEnter(){
		this.refresh();
	}

	refresh(){
		this.loading = true;
		this.client.getAllUsers({}).subscribe(resp => {
			this.loading = false;
			this.users = resp.content;
		}, (err) => {
			this.loading = false;
		})
	}

	addUser(){
		this.navCtrl.push(UserView, {user: null});
	}

	userSelected(user: UserProfile){
		let sheet = this.sheetCtrl.create({
			title: user.lastName + " " + user.firstName,
			subTitle: user.email,
			buttons: [
				{
					text: 'Edit',
					icon: "create",
					handler: () => {
						this.navCtrl.push(UserView, {user: user});
					}
				},{
					text: 'Reset password',
					icon: "lock",
					handler: () => {
						let alert = this.alertCtrl.create({
							title: "Reset password",
							message: "Set a new password for user " + user.firstName + " " + user.lastName,
							inputs: [
								{
									name: 'password',
									placeholder: 'New Password',
									type: 'password'
								}
							],
							buttons: [
								{
									text: "Reset",
									handler: (data) => {
										if(data.password){
											this.client.resetPassword(user, data.password).subscribe(d => {
												alert.dismiss();
											});
										}
										return false;
									}
								},
								{
									text: "Cancel",
									role: "cancel"
								}
							]
						});
						alert.present();
					}
				}, {
					text: 'Delete',
					icon: "trash",
					role: "destructive",
					handler: () => {
						let alert = this.alertCtrl.create({
							title: "Delete",
							message: "Are you sure you want to delete user " + user.firstName + " " + user.lastName + "?",
							buttons: [
								{
									text: "Delete",
									role: "destructive",
									handler: (data) => {
										this.client.deleteUser(user).subscribe(d => {
											alert.dismiss();
										})
										return false;
									}
								},
								{
									text: "Cancel",
									role: "cancel"
								}
							]
						});
						alert.present();
												
					}
				}, {
					text: 'Cancel',
					role: 'cancel',
					handler: () => {
						//nothing
					}
				}
			]
		});
		sheet.present();
	}
}