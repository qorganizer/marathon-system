import { Component } from "@angular/core";
import { Session, SessionMode } from "../../model";
import { RESTClient, ModelStore, QNavigator } from "../../services";
import { AlertController, ActionSheetController, NavController } from "ionic-angular";
import { SessionView } from "../session";

@Component({
	selector: "sessions",
	templateUrl: "sessions.student.html"
})
export class SessionsStudent {
	loading = false;

	sessions: Session[];

	constructor(private client: RESTClient,
		private model: ModelStore,
		private alertCtrl: AlertController,
		private sheetCtrl: ActionSheetController,
		private qnav: QNavigator,
		private nav: NavController) {

	}

	ionViewDidEnter() {
		this.refresh();
	}

	refresh() {
		this.loading = true;
		this.client.getStudentSessions().subscribe(sessions => {
			this.loading = false;
			this.sessions = sessions;
		}, (err) => {
			this.loading = false;
		})
	}

	joinSession() {
		let alert = this.alertCtrl.create({
			title: 'Join a session',
			message: 'Enter the 4 digit code of the session you want to join',
			inputs: [
				{
					name: 'sessionCode',
					placeholder: 'Session Code'
				}
			],
			buttons: [
				{
					text: 'Cancel',
					role: 'cancel',
					handler: () => {

					}
				},
				{
					text: 'Join session',
					handler: (data) => {
						if(data.sessionCode && data.sessionCode.length == 4){
							this.client.joinSession(data.sessionCode).subscribe(()=>{
								alert.dismiss().then(()=>{
									this.refresh();
								});
							});
						}
						return false;
					}
				}
			]
		});
		alert.present();
	}

	sessionSelected(session: Session) {
		if(session.active){
			this.goToSessionView(session);
		}
	}

	goToSessionView(session: Session){
		this.client.startSession(session, SessionMode.Class).subscribe(() => {
			this.qnav.push(this.nav, SessionView, {session: session, sid: session.id});
		});		
	}
}