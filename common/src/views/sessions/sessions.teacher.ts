import { Component } from "@angular/core";
import { ActionSheetController, NavController } from "ionic-angular";
import { Session, SessionMode } from "../../model";
import { RESTClient, ModelStore, QNavigator } from "../../services";
import { SessionView } from "../session";

@Component({
	selector: "sessions",
	templateUrl: "sessions.teacher.html"
})
export class SessionsTeacher {
	loading = false;

	sessions: Session[] = [];

	constructor(private client: RESTClient, 
				private model: ModelStore, 
				private sheetCtrl: ActionSheetController,
				private nav: NavController,
				private qnav: QNavigator) {

	}

	ionViewDidEnter() {
		this.refresh();
	}

	refresh(goTo?: any) {
		this.loading = true;
		this.client.getTeacherSessions().subscribe(sessions => {
			this.loading = false;
			this.sessions = sessions;
			
		}, (err) => {
			this.loading = false;
		})
	}

	goToAttendance(session: Session){
		this.qnav.push(this.nav, SessionView, {session: session, sid: session.id});
	}

	sessionSelected(session: Session) {
		let buttons: any[] = [
			{
				text: 'Cancel',
				role: 'cancel',
				handler: () => {

				}
			}
		];
		if (session.active) {
			buttons.splice(0, 0, 
				{
					text: 'Join session',
					handler: () => {
						this.goToAttendance(session);
					}
				},
				{
					text: 'End session',
					role: 'destructive',
					handler: () => {
						this.client.endSession(session, SessionMode.Class).subscribe(()=> {
							this.refresh();
						});
					}
				}
			);
		} else {
			buttons.splice(0, 0, 
				{
					text: 'Start session',
					handler: () => {
						this.client.startSession(session, SessionMode.Class).subscribe(()=>{
							this.refresh();
							this.goToAttendance(session);
						});
					}
				}
			);
		}
		let sheet = this.sheetCtrl.create({
			title: session.sessionName + " - " + session.sessionCode,
			buttons: buttons
		});
		sheet.present();
	}
}