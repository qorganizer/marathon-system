import { SessionsTeacher } from "./sessions.teacher";
import { SessionsStudent} from "./sessions.student";
import { SessionsAdmin} from "./sessions.admin";
import { UserRole, Permissions } from "../../model";
import { ViewResolver } from "../../utils";

export class Sessions extends ViewResolver{
	public get(role: UserRole): any{
		if(this.hasPermissions(role, Permissions.QPLAYER_STUDENT, Permissions.QPLAYER_TEACHER, Permissions.USER_MANAGEMENT)){
			return SessionsAdmin;
		}else if(this.hasPermission(role, Permissions.QPLAYER_TEACHER)){
			return SessionsTeacher;
		}else if(this.hasPermission(role, Permissions.QPLAYER_STUDENT)){
			return SessionsStudent;
		}
		return null;
	}
}