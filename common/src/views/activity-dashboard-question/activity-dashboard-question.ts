import { EmitterMessage } from '../../model/ui';
import { Subscription } from 'rxjs/Rx';
import { Component, ElementRef, EventEmitter, ViewChild } from '@angular/core';
import { NavController, NavParams, PopoverController, ActionSheetController } from "ionic-angular";
import { RESTClient, ModelStore, SocketClient } from "../../services";
import { Session, QuestionSummary, SessionMode, Activity, ActivityType, QuestionType, Question, ActivityChangeHistory, DashNotifier } from "../../model";
import { Chart } from 'chart.js';

@Component({
	selector: "activity-dashboard-question",
	templateUrl: "activity-dashboard-question.html"
})
export class ActivityDashboardQuestionView {

	loading: boolean = true;
	session: Session;
	activity: Activity;
	subs: Subscription[] = [];
	emitter: EventEmitter<EmitterMessage>;
	index = 1;
	total = null;
	currentQuestion: Question = <any>{};
	summary: QuestionSummary = <any>{};

	@ViewChild("chart") chart: ElementRef;
	chartjs: any;

	private labels = ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"];

	constructor(public navCtrl: NavController,
		private params: NavParams,
		private client: RESTClient,
		private sock: SocketClient,
		private store: ModelStore,
		private popoverCtrl: PopoverController,
		private actionCtrl: ActionSheetController) {
	}

	ionViewDidLoad() {
		this.chartjs = new Chart(
			this.chart.nativeElement,
			{
				type: 'pie',
				data: {
					labels: [],
					datasets: [{
						data: [],
						backgroundColor: [],
						hoverBackgroundColor: []
					}]
				},
				options: {
					responsive: true, 
					maintainAspectRatio: false,
					legend: {
						display: true,
						position: "left"
					}
				}
			}
		);
	}

	ionViewWillEnter() {
		this.session = this.params.get("session");
		this.activity = this.params.get("activity");
		this.emitter = this.params.get("emitter");
		this.index = 1;
		this.total = this.activity.questions.length;
		this.subs.push(this.emitter.subscribe(data => {
			data && this.handleEmitter(data);
		}));
		this.showQuestion(this.index, true);
	}

	ionViewWillLeave() {
		this.subs.forEach(sub => {
			sub.unsubscribe();
		});
	}

	handleEmitter(data: EmitterMessage) {
		if(data.type != EmitterMessage.DASH){
			return;
		}
		let dash: DashNotifier = data.data;
		if(dash.notificationReceived){
			this.client.getQuestionEvaluation(this.activity, this.session, this.index, this.session.active ? SessionMode.Class : SessionMode.Individual).subscribe(evaluation => {
				let shouldRecreateLabels = this.chartjs.data.labels.length == 1;
				shouldRecreateLabels && this.chartjs.data.labels.splice(0, this.chartjs.data.labels.length);
				this.chartjs.data.datasets[0].data.splice(0, this.chartjs.data.datasets[0].data.length);
				for (let index in evaluation.answerDistribution) {
					shouldRecreateLabels && this.chartjs.data.labels.push(<any>index == 0 ? "No answer" : this.activity.questions[this.index-1].distracters[<any>index-1].distracter);
					this.chartjs.data.datasets[0].data.push(evaluation.answerDistribution[index]);
				}
				this.chartjs.update();
			});
		}
	}

	goBack() {
		if (this.index > 1) {
			this.index--;
			this.showQuestion(this.index, false);
		}
	}

	goForward() {
		if (this.index < this.total) {
			this.index++;
			this.showQuestion(this.index, true);
		}
	}

	showQuestion(index: number, forward: boolean) {
		let question = this.activity.questions[index - 1];
		this.currentQuestion = question;
		this.client.getQuestionEvaluation(this.activity, this.session, this.activity.questions.indexOf(this.currentQuestion) + 1, SessionMode.Class).subscribe(data => {
			this.summary = data;
			this.chartjs.data.labels.splice(0, this.chartjs.data.labels.length);
			this.chartjs.data.datasets.splice(0, this.chartjs.data.datasets.length);
			let dataset = {
				data: [
				],
				backgroundColor: [
					"red",
					"orange",
					"yellow",
					"green",
					"blue",
				],
				label: 'Dataset 1'
			};
			let hasSome = false;
			let index:any = 0;
			for (index in data.answerDistribution) {
				hasSome = true;
				this.chartjs.data.labels.push(index == 0 ? "No answer" : question.distracters[index-1].distracter);
				dataset.data.push(data.answerDistribution[index]);
			}
			if(!hasSome){
				this.chartjs.data.labels.push("Waiting for answers");
				dataset.data.push(100);
			}
			this.chartjs.data.datasets.push(dataset);
			this.chartjs.update();
		});
	}
}
