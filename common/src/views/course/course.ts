import { Component } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { RESTClient } from "../../services";
import { Course, Block } from "../../model";
import { BlockView } from "../block";

@Component({
	selector: "course",
	templateUrl: "course.html"
})
export class CourseView {

	course: Course;

	blocks: Block[] = [];

	loading: boolean = true;

	constructor(public navCtrl: NavController, private params: NavParams,  private client: RESTClient) {
		this.course = this.params.get("course");
		let cid = this.params.get("cid");
		if(!this.course && cid){
			this.course = <any> {};
			this.client.getCourseById(cid).subscribe((c) => {
				this.course = c;
			})
		} 
	}

	ionViewDidEnter(){
		this.client.getBlocksInCourse(this.course.id).subscribe((blocks) => {
			this.loading = false;
			this.blocks = blocks;
		});
	}

	goToBlock(block: Block){
		this.navCtrl.push(BlockView, {course: this.course, block: block, cid: this.course.id, bid: block.id});
	}
}
