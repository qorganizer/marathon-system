import { Component, ViewChild, NgZone } from "@angular/core";
import { NavController, Nav, App, NavParams } from "ionic-angular";
import { Subscription } from 'rxjs/Rx';
import { Attendance, Session, TableOfContents, TOCBlock, UserProfile, SessionMode, SessionState } from '../../model';
import { LearningObject } from '../../model/learning-object';
import { ModelStore, QNavigator, RESTClient, SocketClient } from '../../services';
import { isAdmin, isStudent, isTeacher } from '../../utils';
import { AuthenticatedView } from '../auth-view';
import { CoursesView } from '../courses';
import { Dashboard } from '../dashboard';
import { Login } from '../login';
import { SessionAttendance } from '../session-attendance';
import { Sessions } from '../sessions';
import { Users } from '../users';

@Component({
	selector: "session-display",
	templateUrl: "session-display.html"
})
export class SessionDisplay extends AuthenticatedView{

	session: Session = <any> {};
	loading = true;
	users: UserProfile[];
	sub: Subscription;
	tocs: TableOfContents;
	toc: TOCBlock;
	los: LearningObject[];
	state: SessionState;

	constructor(private navigator: QNavigator, 
				private store: ModelStore, 
				private client: RESTClient,
				private ws: SocketClient,
				public navCtrl: NavController, 
				private params: NavParams,
				private zone: NgZone) {
		super();
	}

	ionViewDidEnter(){	
		this.session = this.params.get("session");
		let sid = this.params.get("sid");
		this.tocs = this.store.tocs;
		if(!this.session && sid){
			
		}
		if(!this.tocs){
			this.client.getTableOfContentsForSession(this.session.id).subscribe(tocs => {
				this.tocs = tocs;
				if(tocs && tocs.blocks.length > 0){
					this.toc = tocs.blocks[0];
					this.client.getLOsInBlock(this.toc.blockId).subscribe((los) => {
						this.los = los;
						this.client.getLastVisitedContent(this.session, SessionMode.Class).subscribe(lo => {
							let st = new SessionState();
							if(lo){
								st.currentLO = this.los.find((item, index, arr) : boolean => {
									return item.id == lo.id;
								});
							}
							if(!st.currentLO){
								st.currentLO = this.los[0];
							}
							this.state = st;
						});
						this.loading = false;
					});
				}
			});
		}
	}

	exit(){
		this.params.get("emitter").emit(null);
	}
}
