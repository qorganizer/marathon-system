import { Component, ViewChild, NgZone } from "@angular/core";
import { NavController, Nav, App, NavParams, ActionSheetController } from "ionic-angular";
import { Subscription } from 'rxjs/Rx';
import { Dashboard } from "../dashboard";
import { Sessions } from "../sessions";
import { Users } from "../users";
import { Login} from "../login";
import { CoursesView} from "../courses";
import { QNavigator, ModelStore, RESTClient, SocketClient } from "../../services";
import { isStudent, isTeacher, isAdmin } from "../../utils";
import { AuthenticatedView } from "../auth-view";
import { SessionDisplay } from "../session-display";
import { Session, Attendance, UserProfile, EmitterMessage } from "../../model";

@Component({
	selector: "session-attendance",
	templateUrl: "session-attendance.html"
})
export class SessionAttendance extends AuthenticatedView{

	session: Session;
	loading = true;
	users: UserProfile[];
	sub: Subscription;

	constructor(private navigator: QNavigator, 
				private store: ModelStore, 
				private client: RESTClient,
				private ws: SocketClient,
				private sheetCtrl: ActionSheetController,
				public navCtrl: NavController, 
				private params: NavParams,
				private zone: NgZone) {
		super();
		this.session = this.params.get("session");
		let sid = this.params.get("sid");
		if(!this.session && sid){
			
		}
	}

	ionViewDidEnter(){		
		this.users = this.store.users;
		this.store.usersChanged.subscribe((data)=> {
			this.users = [].concat(this.store.users);
		});
	}

	presentation(){
		this.navCtrl.setRoot(SessionDisplay, this.params.data);
	}

	exit(){
		this.params.get("emitter").emit(EmitterMessage.exit());
	}

	userClicked(user: UserProfile){
		let sheet = this.sheetCtrl.create({
			title: user.lastName + " " + user.firstName,
			subTitle: user.email,
			buttons: [
				{
					text: 'Unregister',
					icon: "trash",
					role: "destructive",
					handler: () => {
						this.client.leaveSession(this.session, user).subscribe(data => {
							sheet.dismiss();
							this.users.splice(this.users.indexOf(user), 1);
						})
						return false;
					}
				}, {
					text: 'Cancel',
					role: 'cancel',
					handler: () => {
						//nothing
					}
				}
			]
		});
		sheet.present();
	}
}
