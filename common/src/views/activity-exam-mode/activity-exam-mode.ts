import { SessionMode } from '../../model/session';
import { QuestionSelector } from '../question';
import { Component, ViewChild } from "@angular/core";
import { NavController, NavParams, PopoverController, ActionSheetController, Nav } from "ionic-angular";
import { RESTClient, ModelStore, SocketClient } from "../../services";
import { Session, Activity, ActivityType, QuestionType, Answer, Question, ActivityChangeHistory} from "../../model";

@Component({
	selector: "activity-exam",
	templateUrl: "activity-exam-mode.html"
})
export class ActivityExamView {
	
	@ViewChild("questionsNav") private nav: Nav;

	loading: boolean = true;
	session: Session;
	activity: Activity;
	title: string = "";
	index = 1;
	total = null;

	currentQuestion: Question = <any>{};
	currentAnswer: Answer = <any>{};

	private start: Date;

	private answers: Answer[];

	constructor(public navCtrl: NavController, 
				private params: NavParams,  
				private client: RESTClient,
				private sock: SocketClient,
				private store: ModelStore,
				private popoverCtrl: PopoverController,
				private actionCtrl: ActionSheetController){
	}

	ionViewWillEnter(){
		this.session = this.params.get("session");
		this.activity = this.params.get("activity");
		this.index = 1;
		this.total = this.activity.questions.length;
		this.answers = [];
		this.activity.questions.forEach((q, i) => {
			let answer = new Answer();
			answer.activityId = this.activity.id;
			answer.sessionId = this.session.id;
			answer.questionIndex = i+1;
			answer.nativeQuiz = false;
			answer.answered = false;
			answer.answerKeys = [];
			answer.sessionMode = this.session.active ? SessionMode.Class.toString() : SessionMode.Individual.toString();
			this.answers.push(answer);
		});
		this.showQuestion(this.index, true);
	}

	goBack(){
		if(this.index > 1){
			this.index--;
			this.showQuestion(this.index, false);
		}
	}

	goForward(){
		if(this.index < this.total){
			this.index++;
			this.showQuestion(this.index, true);
		}
	}

	showQuestion(index: number, forward: boolean){
		let question = this.activity.questions[index-1];
		this.currentQuestion = question;
		this.currentAnswer = this.answers[index-1];
		if(this.start && !this.currentAnswer.answered){	
			let millis = new Date().getTime() - this.start.getTime();
			this.currentAnswer.timer += Math.ceil(millis/1000);
		}
		this.start = new Date();
		let view = new QuestionSelector().getExamViewFromType(question.questionType);
		if(!this.currentAnswer.answerKeys){
			this.currentAnswer.answerKeys = [];
		}
		if(forward){
			this.nav.push(view, {activity: this.activity, question: question, answer: this.answers[index-1]});
		}else{
			this.nav.pop();
		}
	}

	submitCurrentAnswer(){
		let millis = new Date().getTime() - this.start.getTime();
		this.currentAnswer.timer += Math.ceil(millis/1000);
		this.client.submitAnswer(this.currentAnswer).subscribe(d => {
			this.currentAnswer.answered = true;
		});
	}
}
