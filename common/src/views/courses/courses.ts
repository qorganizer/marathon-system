import { Component } from "@angular/core";
import { NavController } from "ionic-angular";
import { RESTClient, SocketClient} from "../../services";
import { Course } from "../../model";
import { CourseView } from "../course";

@Component({
  selector: "courses",
  templateUrl: "courses.html"
})
export class CoursesView {

  constructor(public navCtrl: NavController, private client: RESTClient, private sock: SocketClient) {
	console.log("Hellooo")
  }

  courses: Course[];

  loading = true;
  connected =false;
  msg = "Click realtime to connect";

  ionViewDidEnter() {
	  this.client.getAllCourses().subscribe( courses => {
		this.courses = courses;
		this.loading = false;
	  }, err => {
		  this.loading = false;
	  })
  }

  startRealtime() {
	this.msg = "Connecting...";
	this.sock.connect(null).subscribe(() => {
		this.connected = true;
		this.msg = "Connected";
		this.sock.attendance.subscribe((msg) => {
			if(msg) {
				console.log(msg);
			}
		});
		this.sock.attention.subscribe((msg) => {
			if(msg) {
				this.msg = msg;
			}
		});
	});

  }

  sendAttention() {
	  //this.sock.sendAttention("");
  }

  goToCourse(course: Course){
	  this.navCtrl.push(CourseView, {course: course, cid: course.id});
  }
}
