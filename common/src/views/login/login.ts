import { Component } from "@angular/core";
import { App, NavParams } from "ionic-angular";
import { BusinessService } from "../../services";
import { AuthenticationRequest } from "../../model/protocol";
import { Main } from "../main";
import { Register } from "../register";

@Component({
	selector: "login",
	templateUrl: "login.html"
})
export class Login {

	constructor(public navCtrl: App, private client: BusinessService, private params: NavParams) {

	}

	ionViewDidEnter(){

	}

	authenticate(creds: AuthenticationRequest) {
		this.client.signIn(creds).subscribe((data) => {
			this.navCtrl.getRootNav().setRoot(Main, {}, { animate: true, direction: "forward" });
		});
	}

	signup() {
		this.navCtrl.getRootNav().setRoot(Register, {}, { animate: true, direction: "forward" });
	}

	sso() {
		this.client.signInWithSSO().subscribe((data) => {
			this.navCtrl.getRootNav().setRoot(Main, {}, { animate: true, direction: "forward" });
		});
	}
}
