import { DashboardTeacher } from "./dashboard.teacher";
import { DashboardStudent} from "./dashboard.student";
import { DashboardAdmin} from "./dashboard.admin";
import { UserRole, Permissions } from "../../model";
import { ViewResolver } from "../../utils";

export class Dashboard extends ViewResolver{
	public get(role: UserRole): any{
		if(this.hasPermissions(role, Permissions.QPLAYER_STUDENT, Permissions.QPLAYER_TEACHER, Permissions.USER_MANAGEMENT)){
			return DashboardAdmin;
		}else if(this.hasPermission(role, Permissions.QPLAYER_TEACHER)){
			return DashboardTeacher;
		}else if(this.hasPermission(role, Permissions.QPLAYER_STUDENT)){
			return DashboardStudent;
		}
		return null;
	}
}