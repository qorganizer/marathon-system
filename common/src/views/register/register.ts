import { Component } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { RESTClient, ModelStore } from "../../services";
import { UserRole, UserProfile } from "../../model";
import { AuthenticationRequest } from "../../model/protocol";
import { Login } from "../login";

@Component({
	selector: "register",
	templateUrl: "register.html"
})
export class Register {

	roles: UserRole[];

	user: UserProfile;

	create: boolean;

	constructor(public navCtrl: NavController, private params: NavParams, private client: RESTClient) {

	}

	ionViewWillEnter() {
		this.user = this.params.get("user");
		this.create = !this.user;
		this.client.getSchoolRoles().subscribe(roles => {
			this.roles = roles;
		});
	}

	login() {
		this.navCtrl.setRoot(Login, {}, { animate: true, direction: "back" });
	}

	register(data){
		this.client.registerUser(data).subscribe(d => {
			this.navCtrl.setRoot(Login, {user: d.userName}, { animate: true, direction: "back" });
		});
	}
}
