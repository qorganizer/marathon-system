import { Component, ViewChild } from "@angular/core";
import { NavController, Nav, App, MenuController } from "ionic-angular";
import { Dashboard } from "../dashboard";
import { Sessions } from "../sessions";
import { Users } from "../users";
import { Login} from "../login";
import { CoursesView} from "../courses";
import { QNavigator, ModelStore, BusinessService } from "../../services";
import { isStudent, isTeacher, isAdmin } from "../../utils";
import { AuthenticatedView } from "../auth-view";

@Component({
	selector: "main",
	templateUrl: "main.html"
})
export class Main extends AuthenticatedView{

	@ViewChild("myNav") nav: Nav;

	pages: Page[] = null;

	constructor(private navigator: QNavigator, 
				private store: ModelStore, 
				private client: BusinessService,
				public navCtrl: App,
				private menuCtrl: MenuController) {
		super();
	}

	ngAfterContentInit(){		
		this.menuCtrl.enable(true, "main-menu");
		this.navigator.push(this.nav, Dashboard);
		if(isAdmin(this.store.profile)){
			this.pages = [
				{
					title: "Home",
					view: Dashboard,
					icon: "home"
				},
				{
					title: "Users",
					view: Users,
					icon: "people"
				},
				{
					title: "Sessions",
					view: Sessions,
					icon: "pulse"
				},
				{
					title: "Courses",
					view: CoursesView,
					icon: "book"
				}
			];
		}else if(isStudent(this.store.profile)){
			this.pages = [
				{
					title: "Home",
					view: Dashboard,
					icon: "home"
				},
				{
					title: "Sessions",
					view: Sessions,
					icon: "pulse"
				}
			];
		}else if(isTeacher(this.store.profile)){
			this.pages = [
				{
					title: "Home",
					view: Dashboard,
					icon: "home"
				},
				{
					title: "Sessions",
					view: Sessions,
					icon: "pulse"
				}
			];
		}
	}

	openPage(page: Page){
		this.navigator.setRoot(this.nav, page.view);
	}

	signOut(){
		this.client.signOut().subscribe(()=>{
			this.navCtrl.getRootNav().setRoot(Login);
		});
	}
}

class Page{
	public title: string;
	public view: any;
	public icon: string;
}
