import { EmitterMessage } from '../../model/ui';
import { ActivityDashboardQuestionView } from '../activity-dashboard-question/activity-dashboard-question';
import { DashNotifier, ActivitySummary, SessionMode } from '../../model';
import { Subscription } from 'rxjs/Rx';
import { Component, EventEmitter, NgZone } from '@angular/core';
import { NavController, NavParams, PopoverController, ActionSheetController } from "ionic-angular";
import { RESTClient, ModelStore, SocketClient } from "../../services";
import { Session, Activity, ActivityType, QuestionType, Question, ActivityChangeHistory} from "../../model";
import { debounce } from "../../utils";
@Component({
	selector: "activity-dashboard-quiz",
	templateUrl: "activity-dashboard-quiz.html"
})
export class ActivityDashboardQuizView {

	loading: boolean = true;
	session: Session;
	activity: Activity;
	subs: Subscription[] = [];
	emitter: EventEmitter<EmitterMessage>;
	timeInSeconds = 0;
	time: string = "0:00";
	activitySummary: ActivitySummary = new ActivitySummary();
	private debouncedCall: Function = debounce(this.getEvaluation, 1000, true);

	constructor(public navCtrl: NavController, 
				private params: NavParams,  
				private client: RESTClient,
				private store: ModelStore,
				private popoverCtrl: PopoverController,
				private actionCtrl: ActionSheetController,
				private zone: NgZone){
	}

	ionViewWillEnter(){
		this.session = this.params.get("session");
		this.activity = this.params.get("activity");
		this.emitter = this.params.get("emitter");
		this.subs.push(this.emitter.subscribe(data => {
			data && this.handleEmitter(data);
		}));
		this.getEvaluation();
	}

	ionViewWillLeave(){
		this.subs.forEach(sub => {
			sub.unsubscribe();
		});
		this.subs = [];
	}

	handleEmitter(data: EmitterMessage){
		if(data.type == EmitterMessage.DASH){
			let dash: DashNotifier = data.data;
			this.time = dash.timeDisplay;
			this.timeInSeconds = dash.time;
			if(dash.notificationReceived){
				this.debouncedCall();
			}
		}
	}

	showDetails(){
		this.navCtrl.push(ActivityDashboardQuestionView, {session: this.session, activity: this.activity, emitter: this.emitter});
	}

	private getEvaluation(){
		this.client.getActivityEvaluation(this.activity, this.session, SessionMode.Class).subscribe(data => {
			this.zone.run(() => {
				this.activitySummary = data;
			});
		});
	}
}
