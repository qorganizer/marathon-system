import { EmitterMessage } from '../../model/ui';
import { NotificationMessage, NotificationMessageType } from '../../model/protocol/socket/message';
import { SessionMode } from '../../model/session';
import { Component, EventEmitter } from '@angular/core';
import { Observable } from "rxjs/Rx";
import { NavController, NavParams, PopoverController, ActionSheetController } from "ionic-angular";
import { RESTClient, ModelStore, SocketClient } from "../../services";
import { Session, Activity, ActivityType, QuestionType, Question, ActivityChangeHistory} from "../../model";
import { QuestionChooser } from "../../components/question-chooser";
import { QuestionSelector } from "../question";
import { ActivityDashboardView } from "../activity-dashboard";

@Component({
	selector: "activity-editor",
	templateUrl: "activity.html"
})
export class ActivityEditView {

	loading: boolean = true;
	session: Session;
	activity: Activity = <any>{};
	emitter: EventEmitter<EmitterMessage>;
	create = false;

	title = "";

	private history: ActivityChangeHistory;

	canPresent;

	constructor(public navCtrl: NavController, 
				private params: NavParams,  
				private client: RESTClient,
				private store: ModelStore,
				private sock: SocketClient,
				private popoverCtrl: PopoverController,
				private actionCtrl: ActionSheetController){
	}

	ionViewWillEnter(){
		this.session = this.params.get("session");
		this.activity = this.params.get("activity");
		this.create = this.params.get("create");
		this.history = new ActivityChangeHistory();
		this.title = ActivityType.labelFromName(this.activity.activityType);
		this.emitter = this.params.get("emitter");
		this.client.getActivityEvaluation(this.activity, this.session, SessionMode.Class).subscribe(ev => {
			this.canPresent = ev.percentageOfResponders == 0;
		});
	}

	addQuestion(event){
		let questionTypes = QuestionType.asArray(this.activity.activityType);
		if(questionTypes.length == 1){
			this.showAddQuestionView(questionTypes[0]);
			return;
		}
		let popover = this.popoverCtrl.create(QuestionChooser, {type: this.activity.activityType});
		popover.onDidDismiss((type: QuestionType) => {
			if(type){
				this.showAddQuestionView(type);
			}
		});
		popover.present({
			ev: event
		});
	}

	private showAddQuestionView(type: QuestionType){
		let view = new QuestionSelector().getView(type);
		let question = new Question();
		this.navCtrl.push(view, {activity: this.activity, question: question, create: true});
	}

	onQuestionClick(question: Question){
		let sheet = this.actionCtrl.create({
			title: "Question",
			buttons: [
				{
					text: 'Edit',
					icon: "create",
					handler: () => {
						let view = new QuestionSelector().getViewFromType(question.questionType);
						this.navCtrl.push(view, {activity: this.activity, question: question, create: false});
					}
				}, {
					text: 'Remove',
					icon: "trash",
					role: "destructive",
					handler: () => {
						//hmmm
					}
				}, {
					text: 'Cancel',
					role: 'cancel',
					handler: () => {
						//nothing
					}
				}
			]
		});
		sheet.present();
	}

	save(){
		this.emitter.emit(EmitterMessage.save(this.activity));
	}

	saveAndPresent(){
		this.emitter.emit(EmitterMessage.present(this.activity));
	}

	results(){
		this.emitter.emit(EmitterMessage.results(this.activity));
	}
}
