import { EmitterMessage } from '../../model/ui';
import { ActivityDashboardView } from '../activity-dashboard';
import { ActivityChooser } from '../../components/activity-chooser';
import { Component, EventEmitter } from "@angular/core";
import { NavController, NavParams, ActionSheetController, PopoverController } from "ionic-angular";
import { RESTClient, ModelStore, SocketClient } from "../../services";
import { Session, Activity, ActivityType } from "../../model";
import { ActivityEditView } from "../activity";
import { Subscription } from "rxjs/Rx";
@Component({
	selector: "activities",
	templateUrl: "activities.html"
})
export class ActivitiesView {

	loading: boolean = true;
	session: Session;
	activities: Activity[];
	private sub: Subscription;
	private goToType: ActivityType;

	constructor(public navCtrl: NavController, 
				private params: NavParams,  
				private client: RESTClient,
				private sock: SocketClient,
				private store: ModelStore,
				private actionCtrl: ActionSheetController,
				private popoverCtrl:PopoverController) {
		
	}

	ionViewWillEnter()	{
		this.goToType = null;
		let emitter: EventEmitter<EmitterMessage> = this.params.get("emitter");
		this.sub = emitter.subscribe( msg => {
			if(msg && msg.type == EmitterMessage.GOTOTYPE){
				this.goToType = msg.data;
				if(this.session){
					this.showQuizByType(this.goToType);
				}
			}
		});
	}

	ionViewDidEnter(){
		this.session = this.params.get("session");
		let sid = this.params.get("sid");
		if(!this.session && sid){
			//do something
		}
		this.refresh();
	}

	ionViewWillLeave(){
		if(this.sub){
			this.sub.unsubscribe();
			this.sub = null;
		}
	}

	refresh(){
		this.loading = true;
		this.client.getActivitiesForLO(this.session.id, this.store.currentLO.id).subscribe(activities => {
			this.activities = activities;
			this.loading = false;
			if(this.goToType){
				this.showQuizByType(this.goToType);
			}
		});
	}

	addActivity(event){
		let popover = this.popoverCtrl.create(ActivityChooser, {activities: this.activities});
		popover.onDidDismiss((type: ActivityType) => {
			if(type){
				let activity = new Activity();
				activity.sessionId = this.session.id;
				activity.contextId = this.store.currentLO.id;
				activity.activityType = <any>type.toString();
				this.navCtrl.push(ActivityEditView, {activity: activity, session: this.session, create: true});
			}
		});
		popover.present({
			ev: event
		});
	}

	onQuizClick(activity: Activity){
		let sheet = this.actionCtrl.create({
			title: "activity",
			buttons: [
				{
					text: 'Edit',
					icon: "create",
					handler: () => {
						this.navCtrl.push(ActivityEditView, {activity:activity, create: false, session: this.session, lo: this.store.currentLO});
					}
				}, {
					text: 'Present',
					icon: "desktop",
					handler: () => {
						//soon to be implemented
						this.sock.sendActivityNotification(this.session, activity, true);
						this.navCtrl.push(ActivityDashboardView, {session: this.session, activity: activity})
					}
				}, {
					text: 'Remove',
					icon: "trash",
					role: "destructive",
					handler: () => {
						this.client.deleteActivity(activity).subscribe(data => {
							this.refresh();
						})
					}
				}, {
					text: 'Cancel',
					role: 'cancel',
					handler: () => {
						//nothing
					}
				}
			]
		});
		sheet.present();
	}

	private showQuizByType(type: ActivityType){

		let activity = this.activities.find(activity => {
			return activity.activityType == type.toString();
		});
		if(activity){
			this.navCtrl.push(ActivityEditView, {activity:activity, create: false, session: this.session, lo: this.store.currentLO});
		}else{
			activity = new Activity();
			activity.sessionId = this.session.id;
			activity.contextId = this.store.currentLO.id;
			activity.activityType = <any>type.toString();
			this.navCtrl.push(ActivityEditView, {activity:activity, create: true, session: this.session, lo: this.store.currentLO});
		}
		this.goToType = null;
	}
}
