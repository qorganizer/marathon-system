import { Component } from "@angular/core";
import { ViewController, NavParams } from "ionic-angular";
import { RESTClient, ModelStore } from "../../services";
import { Course, Block, UserRole, UserProfile, Session } from "../../model";

@Component({
	selector: "create-session",
	templateUrl: "create-session.html"
})
export class CreateSessionView {

	teacherRole: UserRole;

	teachers: UserProfile[];

	courses: Course[];

	session: Session;

	constructor(public navCtrl: ViewController, 
				private params: NavParams, 
				private client: RESTClient, 
				private store: ModelStore) {
		
	}

	ngAfterContentInit(){
		this.session = new Session();
		if(!this.store.roles){
			this.client.getAllRoles().subscribe( roles => {
				this.store.roles = roles;
				this.teacherRole = this.getTeacherRole(roles);
			});
		}else{
			this.teacherRole = this.getTeacherRole(this.store.roles);
		}
	}

	private getTeacherRole(roles: UserRole[]): UserRole{
		let role: UserRole = null;
		roles && roles.forEach(r => {
			if(r.roleName == "Teacher" && !role){
				role = r;
			}
		});
		return role;
	}

	ionViewDidEnter(){
		this.client.getAllUsers({roleId: this.teacherRole.id}).subscribe((resp) => {
			this.teachers = resp.content;
		});
		this.client.getAllCourses().subscribe(courses => {
			this.courses = courses;
		});
	}

	cancel(){
		this.navCtrl.dismiss(null);
	}

	register(){
		this.client.registerSession(this.session).subscribe(() => {			
			this.navCtrl.dismiss((this.session));
		}, err => {

		});
	}
}
