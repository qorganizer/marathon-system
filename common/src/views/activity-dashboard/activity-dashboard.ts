import { EmitterMessage } from '../../model/ui';
import { ActivitySummary } from '../../model/evaluation';
import { Subscription } from 'rxjs/Rx';
import { Component, EventEmitter, NgZone, ViewChild } from '@angular/core';
import { NavController, NavParams, PopoverController, ActionSheetController, Nav, Tabs } from "ionic-angular";
import { RESTClient, ModelStore, SocketClient } from "../../services";
import { Session, SessionMode, Activity, ActivityType, QuestionType, Question, DashNotifier, ActivityChangeHistory} from "../../model";
import { ActivityDashboardQuizView, ActivityDashboardQuestionView} from "../";

@Component({
	selector: "activity-dashboard",
	templateUrl: "activity-dashboard.html"
})
export class ActivityDashboardView {

	@ViewChild("summaryNav") nav: Nav;

	loading: boolean = true;
	session: Session;
	activity: Activity;
	subs: Subscription[] = [];

	timeInSeconds = 0;
	time: string = "0:00";

	title = "Activity Evaluation";

	private emitter: EventEmitter<EmitterMessage>;
	private 
	private timeout: any;

	constructor(public navCtrl: NavController, 
				private params: NavParams,  
				private client: RESTClient,
				private sock: SocketClient,
				private store: ModelStore,
				private popoverCtrl: PopoverController,
				private actionCtrl: ActionSheetController,
				private zone: NgZone){
	}

	ionViewWillEnter(){
		this.session = this.params.get("session");
		this.activity = this.params.get("activity");
		this.emitter = this.params.get("emitter");
		if(this.subs.length == 0){
			this.subs.push(this.sock.subscribeToAnswers(this.session.id, this.activity.id));
			this.subs.push(this.sock.answer.subscribe(message => {
				message && this.handleAnswerReceived(message.value);
			}));
			this.nav.push(ActivityDashboardQuizView, {session: this.session, activity: this.activity, emitter: this.emitter})
		}
		(<Tabs>this.navCtrl.parent).setTabbarHidden(true);
	}

	ionViewDidEnter(){
		let t = this;
		if(this.timeout){
			return;
		}
		this.timeout = setInterval(() => {
			t.zone.run(()=> {
				t.timeInSeconds++;
				let mins = Math.floor(t.timeInSeconds / 60);
				let secs = t.timeInSeconds % 60;
				t.time = mins + " : " + (secs < 10 ? "0" : "") + secs;
				t.emitter.emit(EmitterMessage.dash(new DashNotifier(t.timeInSeconds, t.time, false)));
			});
		}, 1000);
	}

	ionViewWillLeave(){		
		clearInterval(this.timeout);
		(<Tabs>this.navCtrl.parent).setTabbarHidden(false);
	}

	endActivity(){
		clearInterval(this.timeout);
		this.time = "0:00";
		this.timeInSeconds = 0;
		this.subs.forEach(sub => {
			sub.unsubscribe();
		});
		this.subs = [];
		this.sock.sendActivityNotification(this.session, this.activity, false);
		this.emitter.emit(EmitterMessage.out());
	}

	handleAnswerReceived(data){
		if(this.isMainDash()){
			this.emitter.emit(EmitterMessage.dash(new DashNotifier(this.timeInSeconds, this.time, false)));
		}else{
			this.emitter.emit(EmitterMessage.dash(new DashNotifier(this.timeInSeconds, this.time, true)));
		}
	}

	isMainDash(){
		return this.navCtrl.length() == 1;
	}
}
