import { ModelStore } from "../services";

export class AuthenticatedView{
	
	ionViewCanEnter(){
		let model: ModelStore = this["model"] || this["store"];
		return model && model.profile != null;
	}
}