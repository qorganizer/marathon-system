import { CoursesSelectView } from '../courses-select';
import { Component } from "@angular/core";
import { ViewController, NavParams, ModalController } from "ionic-angular";
import { RESTClient, ModelStore } from "../../services";
import { Course, Block, UserRole, UserProfile } from "../../model";
import { UserRequest } from "../../model/protocol";

@Component({
	selector: "user",
	templateUrl: "user.html"
})
export class UserView {

	user: UserProfile = new UserProfile();

	roles: UserRole[];

	create: boolean;

	roleChanged: boolean = false;
	courseSelectionChanged: boolean = false;

	constructor(public navCtrl: ViewController, 
				private modalCtrl: ModalController,
				private params: NavParams, 
				private client: RESTClient,
				private store: ModelStore) {
		
	}

	ionViewWillEnter(){
		this.user = this.params.get("user");
		this.create = this.user == null;
		if(this.create){
			this.user = new UserProfile();
			this.user.password = "changeit";
		}
		//this.user.password = "changeit";
		if(!this.store.roles){
			this.client.getAllRoles().subscribe( roles => {
				this.store.roles = roles;
				this.roles = roles;
			});
		}else{
			this.roles = this.store.roles;
		}
	}

	ionViewDidEnter(){
		
	}

	cancel(){
		this.navCtrl.dismiss(null);
	}

	register(){
		if(this.create){
			let req: UserRequest = new UserRequest();
			req.firstName = this.user.firstName;
			req.lastName = this.user.lastName;
			req.password = this.user.password;
			req.roleId = this.user.role.id;
			req.userName = this.user.userName;
			this.client.registerUser(req).subscribe((user) => {	
				this.user.id = user.id;
				if(this.courseSelectionChanged){
					this.client.assignCoursesToUser(this.user.accesibleLevelIds, this.user).subscribe( d => {
						this.navCtrl.dismiss(user);
					});
				}else{
					this.navCtrl.dismiss(user);
				}
			}, err => {

			});
		}else{
			this.client.updateUser(this.user).subscribe(d => {
				if(this.roleChanged){
					this.client.reassignRoleToUser(this.user).subscribe( d => {
						this.client.assignCoursesToUser(this.user.accesibleLevelIds, this.user).subscribe( d => {
							this.navCtrl.dismiss(this.user);
						});
					});
				}else{
					if(this.courseSelectionChanged){
						this.client.assignCoursesToUser(this.user.accesibleLevelIds, this.user).subscribe( d => {
							this.navCtrl.dismiss(this.user);
						});
					}else{
						this.navCtrl.dismiss(this.user);
					}
				}
			});
		}
	}

	chooseCourses(){
		let modal = this.modalCtrl.create(CoursesSelectView, {user: this.user});
		modal.onDidDismiss(data => {
			if(data){
				this.user.accesibleLevelIds = data.selectedIds;
				this.courseSelectionChanged = true;
			}
		});
		modal.present();
	}

	removeCourse(id){
		this.user.accesibleLevelIds.splice(this.user.accesibleLevelIds.indexOf(id), 1);
	}

	compareFn(e1: UserRole, e2: UserRole): boolean{
		return e1 && e2 ? e1.id === e2.id : e1 === e2;
	}
}
