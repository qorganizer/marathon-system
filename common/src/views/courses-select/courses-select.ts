import { UserProfile } from '../../model/user-profile';
import { Component } from "@angular/core";
import { ViewController, NavParams } from "ionic-angular";
import { RESTClient, SocketClient } from "../../services";
import { Course } from "../../model";

@Component({
	selector: "courses-select",
	templateUrl: "courses-select.html"
})
export class CoursesSelectView {

	subject: string;
	stream: string;
	level: string;

	subjects: string[];
	streams: string[];
	levels: string[];

	selectedCourses: Course[];

	user: UserProfile;

	constructor(public viewCtrl: ViewController, 
				private params: NavParams,
				private client: RESTClient, 
				private sock: SocketClient) {
		this.client.getAllSubjects().subscribe(subjects => {
			this.subjects = subjects;
		});
	}

	courses: Course[];
	filteredCourses: Course[];

	ionViewWillEnter() {
		this.user = this.params.get("user");
		this.client.getAllCourses().subscribe( courses => {
			this.courses = courses;
			this.doFilter();
		})
	}

	subjectSelected(){
		if(this.subject){
			this.client.getAllStreamsForSubject(this.subject).subscribe(streams => {
				this.streams = streams;
				this.doFilter();
			});
		}
	}

	streamSelected(){
		if(this.subject && this.stream){
			this.client.getLevelsForSubjectAndStream(this.subject, this.stream).subscribe(levels => {
				this.levels = levels;
				this.doFilter();
			});
		}
	}

	doFilter(){
		this.selectedCourses = [];
		this.filteredCourses = this.courses.filter(course => {
			let valid = true;
			if(this.subject){
				valid = course.subject == this.subject;
			}
			if(this.stream){
				valid = course.stream == this.stream;
			}
			if(this.level){
				valid = course.level == this.level;
			}
			if(this.user && this.user.accesibleLevelIds && this.user.accesibleLevelIds.length > 0){
				valid = this.user.accesibleLevelIds.indexOf(course.levelId) == -1;
			}
			return valid;
		});
	}

	cancel(){
		this.viewCtrl.dismiss(null);
	}

	save(){
		let data = this.selectedCourses.map(c => {
			return c.levelId;
		});
		this.viewCtrl.dismiss({selectedIds: data});
	}

	toggleCourseSelected(course: Course){
		let i = this.selectedCourses.indexOf(course);
		if(i == -1){
			this.selectedCourses.push(course);
		}else{
			this.selectedCourses.splice(i, 1);
		}
	}
}
