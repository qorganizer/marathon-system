import { Component } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { RESTClient } from "../../../services";
import { Question, QuestionType, Distracter, Activity } from "../../../model";

@Component({
	selector: "single-choice",
	templateUrl: "single-choice.html"
})
export class SingleChoiceView {

	loading: boolean = true;

	question: Question;

	activity: Activity;
	
	create = false;

	constructor(public navCtrl: NavController, 
				private params: NavParams, 
				private client: RESTClient) {
		
	}

	ngAfterContentInit(){
		this.question = this.params.get("question");
		this.activity = this.params.get("activity");
		this.create = this.params.get("create");
		if(this.create){
			this.question.questionType = <any>QuestionType.SINGLE_CHOICE.toString();
			this.question.distracters = [];
		}
		while(this.question.distracters.length < 5){
			this.question.distracters.push({
				key: null,
				distracter: "",
				correct: false,
				feedback: ""
			});
		}
	}

	onChange(index: number){
		this.question.distracters.forEach((d, i) => {
			d.correct = index == i;
		});
	}

	save(){
		this.navCtrl.pop();
		if(this.create){
			this.activity.questions.push(this.question);
		}
	}	

	isChecked(i){
		return this.question.distracters.find((distracter, index, list) => {
			return index == i && distracter.correct == true;
		}) != null;
	}
}
