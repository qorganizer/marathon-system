import { QuestionType } from "../../model";
import { MultipleChoiceView, MultipleChoiceExamView} from "./multiple-choice";
import { SingleChoiceView, SingleChoiceExamView} from "./single-choice";
import { TrueFalseView, TrueFalseExamView} from "./true-false";
import { RubricView, RubricExamView} from "./rubric";
import { SliderView, SliderExamView} from "./slider";
import { PollView, PollExamView} from "./poll";
import { ChecklistView, ChecklistExamView} from "./checklist";
import { LikeDislikeView, LikeDislikeExamView} from "./likedislike";

export class QuestionSelector{
	private map: any = {};

	constructor(){
		this.map[QuestionType.MULTIPLE_CHOICE.toString()] = {
			view: MultipleChoiceView,
			exam: MultipleChoiceExamView
		};
		this.map[QuestionType.SINGLE_CHOICE.toString()] = {
			view: SingleChoiceView,
			exam: SingleChoiceExamView
		};
		this.map[QuestionType.TRUE_FALSE.toString()] = {
			view: TrueFalseView,
			exam: TrueFalseExamView
		};
		this.map[QuestionType.SLIDER.toString()] = {
			view: SliderView,
			exam: SliderExamView
		};
		this.map[QuestionType.RUBRIC.toString()] = {
			view: RubricView,
			exam: RubricExamView
		};
		this.map[QuestionType.POLL.toString()] = {
			view: PollView,
			exam: PollExamView
		};
		this.map[QuestionType.CHECKLIST.toString()] = {
			view: ChecklistView,
			exam: ChecklistExamView
		};
		this.map[QuestionType.LIKEDISLIKE.toString()] = {
			view: LikeDislikeView,
			exam: LikeDislikeExamView
		};
	}

	getView(type: QuestionType): any{
		let entry = this.map[type.toString()];
		return entry ? entry.view : null;
	}

	getExamView(type: QuestionType): any{
		let entry = this.map[type.toString()];
		return entry ? entry.exam : null;
	}

	getViewFromType(type: string): any{
		let entry = this.map[type];
		return entry ? entry.view : null;
	}

	getExamViewFromType(type: string): any{
		let entry = this.map[type];
		return entry ? entry.exam : null;
	}
}