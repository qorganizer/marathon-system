import { Component } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { RESTClient } from "../../../services";
import { Question, QuestionType, Distracter, Activity } from "../../../model";

@Component({
	selector: "true-false",
	templateUrl: "true-false.html"
})
export class TrueFalseView {

	loading: boolean = true;

	question: Question;

	activity: Activity;
	
	create = false;

	constructor(public navCtrl: NavController, 
				private params: NavParams, 
				private client: RESTClient) {
		
	}

	ionViewWillEnter(){
		this.question = this.params.get("question");
		this.activity = this.params.get("activity");
		this.create = this.params.get("create");
		if(this.create){
			this.question.questionType = <any>QuestionType.TRUE_FALSE.toString();
			this.question.distracters = [new Distracter(), new Distracter()];
			this.question.distracters[0].distracter = "true";
			this.question.distracters[0].correct = false;
			this.question.distracters[1].distracter = "false";
			this.question.distracters[1].correct = false;
		}
	}

	save(){
		if(this.create){
			this.activity.questions.push(this.question);
		}
		this.navCtrl.pop();
	}

	onChange(data){
		this.question.distracters.forEach(distracter => {
			distracter.correct = distracter.distracter == data;
		});
	}

	isChecked(val){
		return this.question.distracters.find((distracter, index, list) => {
			return distracter.distracter == val && distracter.correct == true;
		}) != null;
	}
}
