import { Component } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { RESTClient } from "../../../services";
import { Question, QuestionType, Distracter, Activity, Answer } from "../../../model";

@Component({
	selector: "true-false-exam",
	templateUrl: "true-false-exam.html"
})
export class TrueFalseExamView {

	question: Question = <any>{};

	activity: Activity;

	answer: Answer = <any>{};

	constructor(public navCtrl: NavController, 
				private params: NavParams) {
		
	}

	ionViewWillEnter(){
		this.question = this.params.get("question");
		this.activity = this.params.get("activity");
		this.answer = this.params.get("answer");
	}

	isChecked(d: number){
		return this.answer.answerKeys && this.answer.answerKeys.length > 0 && this.answer.answerKeys[0] == d;
	}

	onChange(d: number){
		this.answer.answerKeys = [d];
	}
}
