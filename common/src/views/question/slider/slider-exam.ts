import { Component } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { RESTClient } from "../../../services";
import { Question, QuestionType, Distracter, Activity, Answer } from "../../../model";

@Component({
	selector: "slider-exam",
	templateUrl: "slider-exam.html"
})
export class SliderExamView {

	question: Question = <any>{};

	activity: Activity;

	answer: Answer = <any>{};

	constructor(public navCtrl: NavController, 
				private params: NavParams) {
		
	}

	ionViewWillEnter(){
		this.question = this.params.get("question");
		this.activity = this.params.get("activity");
		this.answer = this.params.get("answer");
		if(this.answer.answerKeys){
			this.answer.answerKeys = [];
		}
		this.question.distracters.forEach(d => {
			this.answer.answerKeys.push(null);
		})
	}

	getLabel(question: Question, start: boolean){
		if(!question.distracters){
			return "";
		}
		let distracter = question.distracters[0];
		let label = distracter.sliderRubricOptions[start ? 0 : distracter.sliderRubricOptions.length - 1].label;
		return label;
	}

	onRangeSelected(event, distracter: Distracter){
		var index = this.question.distracters.indexOf(distracter);
		this.answer.answerKeys[index] = event.value;
	}
}
