import { Component } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { RESTClient } from "../../../services";
import { Question, QuestionType, Distracter, Activity, Answer } from "../../../model";

@Component({
	selector: "multiple-choice-exam",
	templateUrl: "multiple-choice-exam.html"
})
export class MultipleChoiceExamView {

	question: Question = <any>{};

	activity: Activity;

	answer: Answer = <any>{};

	constructor(public navCtrl: NavController, 
				private params: NavParams) {
		
	}

	ionViewWillEnter(){
		this.question = this.params.get("question");
		this.activity = this.params.get("activity");
		this.answer = this.params.get("answer");
	}

	isChecked(d: number){
		return this.answer.answerKeys && this.answer.answerKeys.length > 0 && this.answer.answerKeys[0] == d;
	}

	onChange(d: number){
		if(this.answer.answerKeys.indexOf(d) > -1){
			this.answer.answerKeys.splice(this.answer.answerKeys.indexOf(d), 1)
		}else{
			this.answer.answerKeys.push(d);
		}
	}
}
