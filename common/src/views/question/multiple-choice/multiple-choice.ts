import { Component } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { RESTClient } from "../../../services";
import { Question, QuestionType, Distracter, Activity } from "../../../model";

@Component({
	selector: "multiple-choice",
	templateUrl: "multiple-choice.html"
})
export class MultipleChoiceView {

	loading: boolean = true;

	question: Question;

	activity: Activity;
	
	create = false;

	constructor(public navCtrl: NavController, 
				private params: NavParams, 
				private client: RESTClient) {
		
	}

	ionViewWillEnter(){
		this.question = this.params.get("question");
		this.activity = this.params.get("activity");
		this.create = this.params.get("create");
		if(this.create){
			this.question.questionType = <any>QuestionType.MULTIPLE_CHOICE.toString();
			this.question.distracters = [];
		}
		while(this.question.distracters.length < 5){
			this.question.distracters.push({
				key: null,
				distracter: "",
				correct: false
			});
		}
	}	

	onChange(index: number){
		this.question.distracters.forEach((d, i) => {
			d.correct = i == index ? !d.correct : d.correct;
		});
	}

	save(){
		this.navCtrl.pop();
		if(this.create){
			this.activity.questions.push(this.question);
		}
	}

	isChecked(i){
		return this.question.distracters.find((distracter, index, list) => {
			return index == i && distracter.correct == true;
		}) != null;
	}
}
