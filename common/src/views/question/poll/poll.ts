import { Component, EventEmitter } from '@angular/core';
import { NavController, NavParams } from "ionic-angular";
import { RESTClient } from "../../../services";
import { Question, QuestionType, Distracter, Activity, EmitterMessage } from "../../../model";

@Component({
	selector: "poll",
	templateUrl: "poll.html"
})
export class PollView {

	loading: boolean = true;

	question: Question;

	activity: Activity;
	
	emitter: EventEmitter<EmitterMessage>;
	
	create = false;

	canPresent = true;

	constructor(public navCtrl: NavController, 
				private params: NavParams, 
				private client: RESTClient) {
		
	}

	ngAfterContentInit(){
		this.question = this.params.get("question");
		this.activity = this.params.get("activity");
		this.create = this.params.get("create");
		this.emitter = this.params.get("emitter");
		
		if(this.create){
			this.question.questionType = <any>QuestionType.POLL.toString();
			this.question.distracters = [];
		}
		while(this.question.distracters.length < 4){
			this.question.distracters.push({
				key: null,
				distracter: "",
				correct: false
			});
		}
		
	}

	save(){
		if(this.create){
			this.activity.questions.push(this.question);
		}
		this.emitter.emit(EmitterMessage.save(this.activity));
	}

	saveAndPresent(){
		this.emitter.emit(EmitterMessage.present(this.activity));
	}

	results(){
		this.emitter.emit(EmitterMessage.results(this.activity));
	}
}
