import { Component } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { RESTClient } from "../../../services";
import { Question, QuestionType, Distracter, Activity, Answer } from "../../../model";

@Component({
	selector: "likedislike-exam",
	templateUrl: "likedislike-exam.html"
})
export class LikeDislikeExamView {

	
	question: Question = <any>{};

	activity: Activity;

	answer: Answer = <any>{};

	first: boolean = false;
	second: boolean = false;

	constructor(public navCtrl: NavController, 
				private params: NavParams) {
		
	}

	ionViewWillEnter(){
		this.question = this.params.get("question");
		this.activity = this.params.get("activity");		
		this.answer = this.params.get("answer");
		this.first = this.isChecked(1);
		this.second = this.isChecked(2);
	}

	isChecked(d: number){
		return this.answer.answerKeys && this.answer.answerKeys.length > 0 && this.answer.answerKeys[0] == d;
	}

	onChange(d: number){
		this.answer.answerKeys = [d];		
		this.first = this.isChecked(1);
		this.second = this.isChecked(2);
	}
}
