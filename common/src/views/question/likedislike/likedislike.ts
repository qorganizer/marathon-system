import { Component, EventEmitter } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { RESTClient } from "../../../services";
import { Question, QuestionType, Distracter, Activity, EmitterMessage } from "../../../model";

@Component({
	selector: "likedislike",
	templateUrl: "likedislike.html"
})
export class LikeDislikeView {

	loading: boolean = true;

	question: Question;

	activity: Activity;
	
	emitter: EventEmitter<EmitterMessage>;
	
	create = false;

	constructor(public navCtrl: NavController, 
				private params: NavParams, 
				private client: RESTClient) {
		
	}

	ionViewWillEnter(){
		this.question = this.params.get("question");
		this.activity = this.params.get("activity");
		this.create = this.params.get("create");
		this.emitter = this.params.get("emitter");
		if(this.create){
			this.question.questionType = <any>QuestionType.LIKEDISLIKE.toString();
			this.question.distracters = [new Distracter(), new Distracter()];
			this.question.distracters[0].distracter = "true";
			this.question.distracters[0].correct = false;
			this.question.distracters[1].distracter = "false";
			this.question.distracters[1].correct = false;
		}
	}

	save(){
		if(this.create){
			this.activity.questions.push(this.question);
		}
		this.emitter.emit(EmitterMessage.save(this.activity));
	}

	saveAndPresent(){
		this.emitter.emit(EmitterMessage.present(this.activity));
	}

	results(){
		this.emitter.emit(EmitterMessage.results(this.activity));
	}
}
