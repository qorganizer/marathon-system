import { Component, EventEmitter } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { RESTClient } from "../../../services";
import { Question, QuestionType, Distracter, Activity, SliderRubric, EmitterMessage } from "../../../model";

@Component({
	selector: "rubric",
	templateUrl: "rubric.html"
})
export class RubricView {

	loading: boolean = true;

	question: Question;

	activity: Activity;
	
	emitter: EventEmitter<EmitterMessage>;
	
	create = false;

	constructor(public navCtrl: NavController, 
				private params: NavParams, 
				private client: RESTClient) {
		
	}

	ionViewWillEnter(){
		this.question = this.params.get("question");
		this.activity = this.params.get("activity");
		this.create = this.params.get("create");
		this.emitter = this.params.get("emitter");
		if(this.create){
			this.question.questionType = <any>QuestionType.RUBRIC.toString();
			this.question.distracters = [];
		}
		while(this.question.distracters.length < 5){
			this.question.distracters.push({
				key: null,
				distracter: "",
				correct: false,
				feedback: "",
				sliderRubricOptions: []
			});
		}
		this.question.distracters.forEach(distracter => {
			while(distracter.sliderRubricOptions.length < 4){
				distracter.sliderRubricOptions.push(new SliderRubric());
			}
		});
	}	

	save(){
		if(this.create){
			this.activity.questions.push(this.question);
		}
		this.emitter.emit(EmitterMessage.save(this.activity));
	}

	saveAndPresent(){
		this.emitter.emit(EmitterMessage.present(this.activity));
	}

	results(){
		this.emitter.emit(EmitterMessage.results(this.activity));
	}	
}
