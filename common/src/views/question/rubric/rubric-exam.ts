import { Component } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { RESTClient } from "../../../services";
import { Question, QuestionType, Distracter, Activity, SliderRubric, Answer } from "../../../model";

@Component({
	selector: "rubric-exam",
	templateUrl: "rubric-exam.html"
})
export class RubricExamView {

	question: Question = <any>{};

	activity: Activity;

	answer: Answer = <any>{};

	constructor(public navCtrl: NavController, 
				private params: NavParams) {
		
	}

	ionViewWillEnter(){
		this.question = this.params.get("question");
		this.activity = this.params.get("activity");
		this.answer = this.params.get("answer");
		if(this.answer.answerKeys){
			this.answer.answerKeys = [];
		}
		this.question.distracters.forEach(d => {
			this.answer.answerKeys.push(null);
		})
	}

	onChange(distracter: Distracter, option: SliderRubric){
		var index = this.question.distracters.indexOf(distracter);
		var value = distracter.sliderRubricOptions.indexOf(option)+1;
		this.answer.answerKeys[index] = value;
	}

	isChosen(distracter: Distracter, option: SliderRubric){
		var index = this.question.distracters.indexOf(distracter);
		var value = distracter.sliderRubricOptions.indexOf(option)+1;
		return this.answer.answerKeys[index] == value;
	}
}
