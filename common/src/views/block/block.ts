import { Component } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { RESTClient } from "../../services";
import { Course, Block, TOCBlock, LearningObject, QuizProgress, UserAction, SessionState } from "../../model";

@Component({
	selector: "block",
	templateUrl: "block.html"
})
export class BlockView {

	course: Course;

	block: Block;

	loading: boolean = true;

	toc: TOCBlock;

	los: LearningObject[];

	state: SessionState;

	constructor(public navCtrl: NavController, private params: NavParams,  private client: RESTClient) {
		this.course = this.params.get("course");
		this.block = this.params.get("block");
		let cid = this.params.get("cid");
		let bid = this.params.get("bid");
		if(!this.course && cid){
			this.course = <any> {};
			this.client.getCourseById(cid).subscribe((c) => {
				this.course = c;
			})
		}
		if(!this.block && bid){
			this.block = <any> {};
			this.client.getBlockById(bid).subscribe((b) => {
				this.block = b;
			})
		}
	}

	ionViewDidEnter(){
		this.client.getTableOfContents(this.block.id).subscribe((toc) => {
			this.toc = toc;
			this.client.getLOsInBlock(this.block.id).subscribe((los) => {
				this.los = los;
				this.loading = false;
				let state = new SessionState();
				state.currentLO = los[0];
				this.state = state;
			});
		});
	}

	onUserActed(ev: UserAction){

	}

	onQuizProgress(ev: QuizProgress){

	}
}
