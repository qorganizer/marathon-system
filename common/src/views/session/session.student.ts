import { NotificationMessageType } from '../../model/protocol';
import { NotificationMessage } from '../../model/protocol';
import { Component, ViewChild } from "@angular/core";
import { Subscription } from "rxjs"
import { Session, SessionMode, TableOfContents, TOCBlock, SessionState, LearningObject, UserAction, QuizProgress } from "../../model";
import { RESTClient, ModelStore, SocketClient } from "../../services";
import { AlertController, ModalController, Modal, Nav, ToastController, MenuController, NavParams, NavController } from "ionic-angular";
import { ActivityExamView } from "../activity-exam-mode";
import { SessionDisplay } from "../session-display";

@Component({
	selector: "session",
	templateUrl: "session.student.html"
})
export class SessionStudent {

	
	@ViewChild("studentNav") nav: Nav;

	loading = false;

	session: Session;

	sub: Subscription;
	activityModal: Modal;

	constructor(private client: RESTClient,
				private ws: SocketClient,
				private model: ModelStore,
				private alertCtrl: AlertController,
				private modalCtrl: ModalController,
				private toast: ToastController,
				private params: NavParams,
				private menu: MenuController) {
	}

	ionViewWillEnter(){
		this.session = this.params.get("session");
		let sid = this.params.get("sid");
		if(!this.session && sid){
			
		}
		this.nav.setRoot(SessionDisplay, {session: this.session, sid: sid});
		this.refresh();
		this.menu.enable(true, "session-menu");
		
	}

	ionViewWillLeave(){		
		this.ws.updateAttendance(this.session.id, this.model.profile, false);
		this.client.endSession(this.session, SessionMode.Class).subscribe(()=> {
			this.sub && this.sub.unsubscribe();
			this.ws.disconnect();
		});
		this.menu.enable(true, "main-menu");
	}

	refresh(){
		this.ws.connect(this.session).subscribe((state)=>{
			this.ws.updateAttendance(this.session.id, this.model.profile, true);
			this.sub = this.ws.notification.subscribe(this.handleIncomingNotifications.bind(this));
		});
		this.client.getActiveSessionState(this.session).subscribe( state => {
			state.state.forEach(entry => {
				let note = NotificationMessage.fromString(JSON.stringify(entry));
				this.handleIncomingNotifications(note);
			});
		});
	}

	onUserActed(ev: UserAction){

	}

	onQuizProgress(ev: QuizProgress){

	}

	handleIncomingNotifications(notification: NotificationMessage){
		if(!notification){
			return;
		}
		switch(notification.type){
			case NotificationMessageType.ATTENTION:
				this.toast.create({
					showCloseButton: true,
					closeButtonText: "OK",
					message: "Pay Attention!",
					cssClass: "warning",
					position: "top"
				})
				break;
			case NotificationMessageType.ACTIVITY:
				let id = notification.value;
				if(!notification.active){
					if(this.activityModal){
						this.activityModal.dismiss(null);
						return;
					}
				}
				this.client.getActivity(id).subscribe(activity => {
					this.activityModal = this.modalCtrl.create(ActivityExamView, {session: this.session, activity: activity}, {enableBackdropDismiss: false, cssClass: "activity-modal"});
					this.activityModal.onDidDismiss((data) => {

					});
					this.activityModal.present();
				});
				break;
			default:
				console.log("Not managing " + notification.type);
		}
	}
}