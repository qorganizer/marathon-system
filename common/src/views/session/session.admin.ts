import { Component } from "@angular/core";
import { ModalController } from "ionic-angular";
import { CreateSessionView } from "../";
import { Session } from "../../model";
import { RESTClient, ModelStore} from "../../services";

@Component({
  selector: "sessions",
  templateUrl: "session.admin.html"
})
export class SessionAdmin{

	loading = false;

	session: Session;

	constructor(private modalCtrl: ModalController, 
				private client: RESTClient, 
				private model: ModelStore){

	}	

	ionViewDidEnter(){
		this.refresh();
	}

	refresh(){
		this.loading = true;
		//this.client.getAllSessions().subscribe(sessions => {
		//	this.loading = false;
		//	this.sessions = sessions;
		//}, (err) => {
		//	this.loading = false;
		//})
	}
}