import { SessionTeacher } from "./session.teacher";
import { SessionStudent} from "./session.student";
import { SessionAdmin} from "./session.admin";
import { UserRole, Permissions } from "../../model";
import { ViewResolver } from "../../utils";

export class SessionView extends ViewResolver{
	public get(role: UserRole): any{
		if(this.hasPermissions(role, Permissions.QPLAYER_STUDENT, Permissions.QPLAYER_TEACHER, Permissions.USER_MANAGEMENT)){
			return SessionAdmin;
		}else if(this.hasPermission(role, Permissions.QPLAYER_TEACHER)){
			return SessionTeacher;
		}else if(this.hasPermission(role, Permissions.QPLAYER_STUDENT)){
			return SessionStudent;
		}
		return null;
	}
}