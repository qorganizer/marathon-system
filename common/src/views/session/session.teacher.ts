import { Question, QuestionType } from '../../model/quiz';
import { QuestionSelector } from '../question';
import { ActivityEditView } from '../activity';
import { EmitterMessage } from '../../model/ui';
import { MenuState, SessionMenuStructure, ActivityType } from '../../model';
import { Component, ViewChild, NgZone, EventEmitter } from "@angular/core";
import { Observable, Subscription } from 'rxjs/Rx';
import { ToastController, NavController, Nav, NavParams, MenuController, Tabs } from "ionic-angular";
import { Session, SessionMode, UserProfile, NotificationMessage, NotificationMessageType, Activity } from "../../model";
import { QNavigator, ModelStore, RESTClient, SocketClient } from "../../services";
import { SessionAttendance, SessionDisplay, ActivitiesView } from "../";
import { ActivityDashboardView } from "../activity-dashboard";

@Component({
	selector: "session",
	templateUrl: "session.teacher.html"
})
export class SessionTeacher {
	loading = false;

	session: Session;

	display = SessionDisplay;
	quiz = ActivitiesView;

	rootParams = null;
	private emitter: EventEmitter<any> = new EventEmitter();

	sub: Subscription;

	@ViewChild(Tabs) tabs: Tabs;

	private activities: Activity[];

	second: any = SessionAttendance;
	secondRootParams: any = {};

	attention: boolean;
	share: boolean;

	menuData: SessionMenuStructure = {
		ledOn: true,
		menus: [
			{
				id: "",
				icon:"q-home",
				cssClass: "home",
				state: MenuState.SELECTED,
				handler: null,
				subMenus: [
					{
						id: "",
						handler: () => {
							
						},
						icon: "q-performance",
						state: MenuState.ENABLED,
					},
					{
						id: "",
						handler: () => {
							
						},
						icon: "q-navigator",
						state: MenuState.ENABLED,
					},
					{
						id: "",
						handler: () => {
							
						},
						icon: "q-todo",
						state: MenuState.ENABLED,
					},
					{
						id: "",
						handler: () => {
							
						},
						icon: "q-my-profile",
						state: MenuState.ENABLED,
					}
				]
			},
			{
				id: "",
				icon:"q-current-lo",
				cssClass: "current-lo",
				state: MenuState.ENABLED,
				handler: () => {
				},
				subMenus: null
			},
			{
				id: "",
				icon:"q-session",
				cssClass: "session",
				state: MenuState.ENABLED,
				handler: null,
				subMenus: [
					{
						id: "share-screen",
						handler: () => {
							this.share = !this.share;
							this.setMenuState("share-screen", this.share ? MenuState.SELECTED : MenuState.ENABLED);
							this.ws.sendShareScreen(this.session.id, this.store.currentLO, this.share);
						},
						icon: "q-share-screen",
						state: MenuState.ENABLED,
					},
					{
						id: "attendance",
						handler: () => {
							this.showTab(1, SessionAttendance, this.rootParams);
						},
						icon: "q-attendance",
						state: MenuState.ENABLED,
					},
					{
						id: "attention",
						handler: () => {
							this.attention = !this.attention;
							this.setMenuState("attention", this.attention ? MenuState.SELECTED : MenuState.ENABLED);
							this.ws.sendAttention(this.session.id, this.attention);
						},
						icon: "q-attention",
						state: MenuState.ENABLED,
					}
				]
			},
			{
				id: "",
				icon:"q-interaction",
				cssClass: "interaction",
				state: MenuState.ENABLED,
				handler: null,
				subMenus: [
					{
						id: "quiz",
						handler: () => {
							this.goToQuizByType(ActivityType.QUIZ);
						},
						icon: "q-tag-cloud",
						state: MenuState.ENABLED,
					},
					{
						id: "poll",
						handler: () => {
							this.goToQuizByType(ActivityType.POLL);
						},
						icon: "q-enquette",
						state: MenuState.ENABLED,
					},
					{
						id: "likedislike",
						handler: () => {
							this.goToQuizByType(ActivityType.LIKEDISLIKE);
						},
						icon: "q-like-dislike",
						state: MenuState.ENABLED,
					},
					{
						id: "checklist",
						handler: () => {
							this.goToQuizByType(ActivityType.CHECKLIST);
						},
						icon: "q-check",
						state: MenuState.ENABLED,
					},
					{
						id: "rubric",
						handler: () => {
							this.goToQuizByType(ActivityType.RUBRIC);							
						},
						icon: "q-rubric",
						state: MenuState.ENABLED,
					},
					{
						id: "slider",
						handler: () => {							
							this.goToQuizByType(ActivityType.SLIDER);
						},
						icon: "q-checklist",
						state: MenuState.ENABLED,
					}
				]
			},
			{
				id: "",
				icon:"q-resources",
				cssClass: "resources",
				state: MenuState.ENABLED,
				handler: () => {
				},
				subMenus: null
			},
			{
				id: "",
				icon:"q-tools",
				cssClass: "tools",
				state: MenuState.ENABLED,
				handler: null,
				subMenus: [
					{
						id: "",
						handler: () => {
							
						},
						icon: "q-onenote",
						state: MenuState.ENABLED,
					},
					{
						id: "",
						handler: () => {
							
						},
						icon: "q-excel",
						state: MenuState.ENABLED,
					},
					{
						id: "",
						handler: () => {
							
						},
						icon: "q-powerpoint",
						state: MenuState.ENABLED,
					},
					{
						id: "",
						handler: () => {
							
						},
						icon: "q-edit",
						state: MenuState.ENABLED,
					}
				]
			},
			{
				id: "",
				icon:"q-collaborate",
				cssClass: "collaborate",
				state: MenuState.ENABLED,
				handler: null,
				subMenus: [
					{
						id: "",
						handler: () => {
							
						},
						icon: "q-edit",
						state: MenuState.ENABLED,
					},
					{
						id: "",
						handler: () => {
							
						},
						icon: "q-edit",
						state: MenuState.ENABLED,
					},
					{
						id: "",
						handler: () => {
							
						},
						icon: "q-edit",
						state: MenuState.ENABLED,
					}
				]
			}
		]
	};

	constructor(private client: RESTClient,
				private store: ModelStore,
				private navigator: QNavigator,
				private navCtrl: NavController,
				private ws: SocketClient,
				private params: NavParams,
				private zone: NgZone,
				private toastCtrl: ToastController,
				private menu: MenuController) {
		this.session = this.params.get("session");
		let sid = this.params.get("sid");
		this.rootParams = {
			session: this.session,
			sid: sid,
			emitter: this.emitter
		};
		this.store.currentSession = this.session;
		let t = this;
		if (!this.session && sid) {

		}
	}

	ngAfterContentInit(){
		let t = this;
		this.emitter.subscribe((msg: EmitterMessage) => {
			if(!msg){
				return;
			}
			switch(msg.type){
				case EmitterMessage.EXIT:
					t.navCtrl.pop();
					break;
				case EmitterMessage.OUT:
					t.tabs.select(0);
					break;
				case EmitterMessage.SAVE:
					this.save(msg.data);
					break;
				case EmitterMessage.PRESENT:
					this.presentActivity(msg.data);
					break;
				case EmitterMessage.RESULTS:
					this.showTab(1, ActivityDashboardView, {session: this.session, activity: msg.data, emitter: this.emitter});
					break;
			}
		});
	}

	ionViewWillEnter() {
		this.menu.enable(false);
		this.tabs.getByIndex(1).setRoot(SessionAttendance, {});
		this.refresh();
	}

	ionViewWillLeave() {
		this.menu.enable(true);
		if (this.sub) {
			this.sub.unsubscribe();
		}
		this.ws.disconnect();
	}

	refresh() {
		this.client.getAttendance(this.params.get("sid")).subscribe(attendance => {
			let u = [];
			if (attendance) {
				attendance.activeUsers.forEach((user: UserProfile) => {
					user.inSession = true;
					u.push(user);
				});
				attendance.inactiveUsers.forEach((user: UserProfile) => {
					user.inSession = false;
					u.push(user);
				});
			}
			this.store.users = u;
			this.store.usersChanged.emit(null);
			this.ws.connect(this.session).subscribe((state) => {
				this.sub = this.ws.attendance.subscribe(attendance => {
					if (!attendance) {
						return;
					}
					this.zone.run(() => {
						let user = this.store.users.find(u => {
							return u.id == attendance.profile.id;
						});
						if (!user) {
							user = attendance.profile;
							user.inSession = attendance.online;
							this.store.users.push(user);
						} else {
							user.inSession = attendance.online;
						}
						this.store.usersChanged.emit(null);
					});
					this.toastCtrl.create({
						duration: 3000,
						cssClass: attendance.online ? "online" : "offline",
						position: "bottom",
						message: attendance.profile.firstName + " " + attendance.profile.lastName + (attendance.online ? " came online" : " went offline")
					}).present();
				});
			});
		}, (err) => {
			this.loading = false;
		});

		this.client.getTableOfContentsForCourse(this.session.course.id).subscribe(tocs => {
			this.store.tocs = tocs;
			if (tocs.blocks.length > 0) {
				this.client.getLOsInBlock(this.store.tocs.blocks[0].blockId).subscribe((los) => {
					this.client.getLastVisitedContent(this.session, SessionMode.Class).subscribe(lo => {
						if (lo) {
							this.store.currentLO = los.find((item, index, arr): boolean => {
								return item.id == lo.id;
							});
						}
						if (!this.store.currentLO) {
							this.store.currentLO = los[0];
							this.loChanged();
						}
					});					
					this.client.getActiveSessionState(this.session).subscribe( state => {
						this.loading = false;
						state.state.forEach(entry => {
							let note = NotificationMessage.fromString(JSON.stringify(entry));
							this.handleIncomingNotifications(note);
						});
					});
				});
			}
		});
	}

	private loChanged(){
		this.client.getActivitiesForLO(this.params.get("sid"), this.store.currentLO.id).subscribe( activities => {
			this.activities = activities;
			this.refreshActivityMenuItems();
		});
	}

	private refreshActivityMenuItems(){
		ActivityType.asArray().forEach(type => {
			this.setMenuState(type.toString().toLowerCase(), this.getActivityByType(type, this.activities) ?  MenuState.SELECTED : MenuState.ENABLED);
		});
	}

	handleIncomingNotifications(notification: NotificationMessage){
		if(!notification){
			return;
		}
		switch(notification.type){
			case NotificationMessageType.ATTENTION:
				this.attention = notification.active;
				this.setMenuState("attention", notification.active ? MenuState.SELECTED : MenuState.ENABLED);
				break;
			case NotificationMessageType.SHARE_SCREEN:
				this.share = notification.active;
				this.setMenuState("share-screen", notification.active ? MenuState.SELECTED : MenuState.ENABLED);
				break;
			case NotificationMessageType.ACTIVITY:
				let id = notification.value;
				if(!notification.active){
					return;
				}
				this.client.getActivity(id).subscribe(activity => {
					this.showTab(1, ActivityDashboardView, {session: this.session, activity: activity, emitter: this.emitter});
				});
				break;
			default:
				console.log("Not managing " + notification.type);
		}
	}

	private goToQuizByType(type: ActivityType){
		let activity = this.getActivityByType(type, this.activities);
		let types = QuestionType.asArray(type.toString());
		if(!activity){
			activity = new Activity();
			activity.activityType = <any>type.toString();
			activity.sessionId = this.params.get("sid");
			activity.contextId = this.store.currentLO.id;
			if(types.length == 1){
				let question = new Question();
				question.questionType = <any>types[0].toString();
				question.distracters = [];
				activity.questions.push(question);
			}
		}
		let view = ActivityEditView;
		let params: any = {
			activity:activity, 
			create: !activity.id, 
			session: this.session, 
			lo: this.store.currentLO,
			emitter: this.emitter
		};
		if(types.length == 1){
			view = new QuestionSelector().getView(types[0]);
			params.question = activity.questions[0];		
		}
		this.showTab(1, view, params);
	}

	private setMenuState(id: string, state: MenuState){
		let menu = null, submenu = null;
		for(let i = 0; i < this.menuData.menus.length; i++ ){
			menu = this.menuData.menus[i];
			if(menu.id == id){
				menu.state = state;
				return;
			}
			if(menu.subMenus){
				for(let j = 0; j < menu.subMenus.length; j++){
					submenu = menu.subMenus[j];
					if(submenu.id == id){
						submenu.state = state;
						return;
					}
				}
			}
		}
	}	

	private showTab(tabIndex: number, view: any, params: any){
		this.zone.run(() => {	
			let tab = this.tabs.getByIndex(tabIndex);
			if(!tab){
				return;
			}
			this.secondRootParams = params;
			this.second = view;
			tab.setRoot(view, params).then((d)=> {
				if(!this.tabs.getSelected() || this.tabs.getSelected().index != tabIndex){
					this.tabs.select(tabIndex);
				}
			}).catch( e => {
				console.error(e);
			});
		});
			
	}

	private getActivityByType(type: ActivityType, activities: Activity[]): Activity{
		for(let i = 0; i < activities.length; i++){
			if(activities[i].activityType == type.toString()){
				return activities[i];
			}
		}
		return null;
	}

	private save(activity: Activity){
		this.doSave(activity).subscribe(a => {
			this.tabs.select(0);
		});
	}

	private presentActivity(activity: Activity){
		this.doSave(activity).subscribe(a => {
			this.ws.sendActivityNotification(this.session, activity, true);
			this.showTab(1, ActivityDashboardView, {session: this.session, activity: activity, emitter: this.emitter});
		});
	}

	private doSave(activity: Activity): Observable<Activity>{
		let copy = JSON.parse(JSON.stringify(activity));
		copy.questions.forEach(question => {			
			question.distracters = question.distracters.filter((d) => {
				return d.distracter && d.distracter.trim().length > 0;
			});
			question.questionType == QuestionType.RUBRIC.toString() && question.distracters.forEach(d => {
				d.sliderRubricOptions && (d.sliderRubricOptions = d.sliderRubricOptions.filter( o => {
					return o.label && o.label.trim().length > 0;
				}));
			});
		})
		if(!activity.id){
			return this.client.createActivity(copy).map(a=> {
				activity.id= a.id;
				return activity;
			});
		}else{
			return Observable.of(activity);
		}
	}
}