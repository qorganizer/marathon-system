
import { UserRole, UserProfile, Permissions } from "../model";

export const Role = (requiredRole: string) => {

    return (target: any) => {
        target.prototype.ionViewCanEnter = function() {
          if(this.navCtrl === undefined) {
            throw new Error("@Role requires the target component to have a property called 'navCtrl' that is the injected NavController");
          }else {
             //do the role check stuff here to see if the desired role is good
            //just navigating to default and returning false to test
            //this.navCtrl.setRoot(redirectRoute);
            return false;
          }
        }
    }
};

export class ViewResolver{
	public resolver: boolean = true;

	public get(role: UserRole): any{
		return null;
	};

	protected hasPermission(role: UserRole, permission: string): boolean{
		return role && role.permissions && role.permissions.indexOf(permission) > -1;
	}

	protected hasPermissions(role: UserRole, ...permissions): boolean{
		return role && role.permissions && [role.permissions, permissions].reduce((p,c) => p.filter(e => c.indexOf(e) > -1)).length == permissions.length;
	}
}

export function hasPermissions(role: UserRole, ...permissions): boolean{
	return role && role.permissions && [role.permissions, permissions].reduce((p,c) => p.filter(e => c.indexOf(e) > -1)).length == permissions.length;
}

export function isAdmin(user: UserProfile){
	return hasPermissions(user.role, Permissions.QPLAYER_STUDENT, Permissions.QPLAYER_TEACHER, Permissions.USER_MANAGEMENT);
}

export function isStudent(user: UserProfile){
	return hasPermissions(user.role, Permissions.QPLAYER_STUDENT);
}

export function isTeacher(user: UserProfile){
	return hasPermissions(user.role, Permissions.QPLAYER_TEACHER);
}