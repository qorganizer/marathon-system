import { SSOServiceManager } from './sso-manager';
import { Injectable, Inject } from "@angular/core";
import { RESTClient, SocketClient, ModelStore } from "../services";
import { Observable, Observer} from "rxjs/Rx";
import {AuthenticationRequest, UserProfile, ActivityChangeHistory} from "../model";
import { Storage } from '@ionic/storage';
import {OAuthService} from "./oauth-service";

@Injectable()
export class BusinessService{
	
	constructor(private rest: RESTClient,
				private sock: SocketClient,
				private store: ModelStore,
				private storage: Storage,
				private oauth: OAuthService,
				private sso: SSOServiceManager){
		this.rest.tokenUpdated.subscribe(token => {
			if(!token){
				return;
			}
			this.storage.ready().then(()=>{
				this.storage.set("auth-token", token);
			})
			this.sock.setToken(token);
		});
	}

	public isAuthenticated(): Observable<boolean>{
		return new Observable<boolean>((obs: Observer<boolean>) => {
			this.oauth.isAuthenticated().subscribe(isAuthenticated => {
				this.storage.ready().then(()=> {
					this.storage.get("auth-token").then((token) => {
						this.onTokenObtained(token, obs, true);
					});
				});
			});			
		});
	}	

	public isOauthCallback(): boolean{
		return this.oauth.isCallback();
	}

	private onTokenObtained(token: string, obs: Observer<boolean>, suppressUnauthorizedException?: boolean){
		if(token){
			this.rest.setToken(token);
			this.rest.getLoggedInProfile(suppressUnauthorizedException).subscribe((profile)=> {
				this.store.profile = profile;
				obs && obs.next(true);
				obs && obs.complete();
			}, (err) => {
				obs && obs.next(false);
				obs && obs.complete();
			});
		}else{
			obs && obs.next(false);
			obs && obs.complete();
		}
	}

	public signInWithSSO(): Observable<boolean>{
		return new Observable<boolean>((obs: Observer<boolean>) => {
			this.oauth.signIn().subscribe((token) => {
				this.onTokenObtained(token, obs, true);
			});
		});		
	}

	public signIn(req: AuthenticationRequest): Observable<UserProfile>{
		return this.rest.login(req).map((data) => {
			this.sso.handleLogin(data);
			this.store.profile = data;
			return data;
		})
	}

	public signOut(): Observable<boolean>{
		return new Observable<boolean>((obs: Observer<boolean>) => {
			this.rest.logout().subscribe(()=>{
				this.storage.remove("auth-token");
				this.sock.setToken(null);
				this.rest.setToken(null);
				obs.next(true);
				obs.complete();
			}, (err) => {
				this.storage.remove("auth-token");
				this.sock.setToken(null);
				this.rest.setToken(null);
				obs.next(true);
				obs.complete();
			});
		});
	}

	public updateActivity(info: ActivityChangeHistory): Observable<boolean>{
		return new Observable<boolean>( obs => {
			
		});
	}
}