import { Injectable } from "@angular/core";
import { Headers, Response, Http, URLSearchParams, QueryEncoder } from "@angular/http";
import { AuthenticationRequest, UserRequest, PagedResponse, SessionRequest } from "../model/protocol";
import { Course, Attendance, LearningObject, Block, UserRole, Task, Answer, SessionActiveState, ActivitySummary, AnswerEvaluation, QuestionSummary, UserEvaluation, TableOfContents, TOCBlock, UserProfile, Session, SessionMode, Activity, Question } from "../model";
import { Observable, Observer, BehaviorSubject } from "rxjs/Rx";
import {OAuthService} from "./oauth-service";

@Injectable()
export class RESTClient {

	protected errorSource: BehaviorSubject<any>;
    /**
     * Error event
     */
	error: Observable<any>;

	protected unauthorizedSource: BehaviorSubject<any>;
    /**
     * Unauthorized event
     */
	unauthorized: Observable<any>;

	protected tokenUpdatedSource: BehaviorSubject<string>;
    /**
     * Unauthorized event
     */
	tokenUpdated: Observable<string>;

	private token: string;

	private online = true;

	private restUrl: string;

	private responseHeaders: Headers;

	constructor(private http: Http) {
		this.errorSource = new BehaviorSubject<any>(null);
		this.error = this.errorSource.asObservable();
		this.unauthorizedSource = new BehaviorSubject<any>(null);
		this.unauthorized = this.unauthorizedSource.asObservable();
		this.tokenUpdatedSource = new BehaviorSubject<any>(null);
		this.tokenUpdated = this.tokenUpdatedSource.asObservable();
	}

	public setRestRoot(restUrl: string) {
		this.restUrl = restUrl;
	}

	public setOnline(val: boolean) {
		this.online = val;
	}

	public setToken(token) {
		this.token = token;
		this.tokenUpdatedSource.next(token);
		if(this.emitter){
			this.emitter.next(token);
		}
	}

	/**
	 * 
	 * @returns Observable
	 */
	public login(req: AuthenticationRequest): Observable<UserProfile> {
		return new Observable<UserProfile>((obs: Observer<UserProfile>) => {
			this.call<any>("POST", "/login", req, false)
				.map(resp => {
					var token = this.responseHeaders.get("authorization");
					if (!token) {
						this.errorSource.next("Login response was OK but token is missing!!!!");
						obs.error("Login response was OK but token is missing!!!!");
					} else {
						this.token = token;
						this.tokenUpdatedSource.next(token);
					}
					return {};
				}).subscribe((d) => {
					this.getLoggedInProfile().subscribe((profile) => {
						obs.next(profile);
						obs.complete();
					}, (err) => {
						obs.error(err);
					})
				}, (err) => {
					obs.error(err);
				});
		});

	}

	public getLoggedInProfile(supressUnauthorizedException: boolean = false): Observable<UserProfile> {
		return this.call<UserProfile>("GET", "/user", null, true, supressUnauthorizedException);
	}

	public updateLoggedInProfile(profile: UserProfile): Observable<any>{
		return this.call<any>("PUT", "/user", profile);
	}

	public resetPassword(user:UserProfile, password: string){
		return this.call<any>("POST", `/user/${user.id}/resetPassword`, password);
	}

	public assignCoursesToUser(courses: number[], user: UserProfile){
		let q = courses && courses.length > 0 ? "?levelIds=" + courses.join("&levelIds=") : "";
		return this.call<any>("PUT", `/user/${user.id}/courses${q}`, {});
	}

	public changePassword(oldP: string, newP: string): Observable<any>{
		return this.call<any>("POST", `/user/changePassword`, {
			passwords: {
				newPassword: newP,
				oldPassword: oldP
			}
		});
	}

	public registerUser(user: UserRequest): Observable<UserProfile> {
		return this.call<any>("POST", "/user", user)
	}

	public getAllCourses(): Observable<Course[]> {
		return this.call<Course[]>("GET", "/course", null)
			.map(resp => {
				return resp;
			});
	}

	public getCourseById(id: string | number): Observable<Course> {
		return this.call<Course>("GET", `/course/${id}`, null)
			.map(resp => {
				return resp;
			});
	}

	public getAttendance(id: string | number): Observable<Attendance> {
		return this.call<Attendance>("GET", `/session/${id}/attendance`, null)
			.map(resp => {
				return resp;
			});
	}

	public getLevelsForSubjectAndStream(subject: string, stream: string): Observable<string[]> {
		return this.call<string[]>("GET", "/course/levels", { subject: subject, stream: stream })
			.map(resp => {
				return resp;
			});
	}

	public getAllSubjects(): Observable<string[]> {
		return this.call<string[]>("GET", "/course/subjects", null)
			.map(resp => {
				return resp;
			});
	}

	public getAllStreamsForSubject(subject: string): Observable<string[]> {
		return this.call<string[]>("GET", "/course/streams", { subject: subject })
			.map(resp => {
				return resp;
			});
	}

	public getLearningObject(id: string | number): Observable<LearningObject> {
		return this.call<LearningObject>("GET", `/learning-object/${id}`, null)
			.map(resp => {
				return resp;
			});
	}

	public getLOsInBlock(id: string | number): Observable<LearningObject[]> {
		return this.call<PagedResponse<LearningObject>>("GET", `/content/block/${id}/los`, null)
			.map(resp => {
				return resp.content;
			});
	}


	public getBlocksInCourse(id: string | number): Observable<Block[]> {
		return this.call<Block[]>("GET", `/content/course/${id}/blocks`, null)
			.map(resp => {
				return resp;
			});
	}

	public getTasksInBlock(blockId: string | number): Observable<LearningObject> {
		return this.call<LearningObject>("GET", `/content/block/${blockId}/tasks`, null)
			.map(resp => {
				return resp;
			});
	}

	public getBlockById(id: string | number): Observable<Block> {
		return this.call<Block>("GET", `/content/block/${id}`, null)
			.map(resp => {
				return resp;
			});
	}

	public getLOsInTask(id: string | number): Observable<LearningObject[]> {
		return this.call<LearningObject[]>("GET", `/content/tasks/${id}/los`, null)
			.map(resp => {
				return resp;
			});
	}

	public getTableOfContents(blockId: string | number): Observable<TOCBlock> {
		return this.call<TOCBlock>("GET", `/content/toc/block/${blockId}`, null)
			.map(resp => {
				return resp;
			});
	}

	public getTableOfContentsForCourse(courseId: string | number): Observable<TableOfContents> {
		return this.call<TableOfContents>("GET", `/content/toc/course/${courseId}`, null)
			.map(resp => {
				return resp;
			});
	}

	public getTableOfContentsForSession(sessionId: string | number): Observable<TableOfContents> {
		return this.call<TableOfContents>("GET", `/content/toc/session/${sessionId}`, null)
			.map(resp => {
				return resp;
			});
	}

	public registerSession(session: Session): Observable<any> {
		let data: SessionRequest = new SessionRequest();
		data.course.level = session.course.level;
		data.course.stream = session.course.stream;
		data.course.subject = session.course.subject;
		data.session = session.sessionName;
		data.network = session.network || "network";
		data.school = session.school || "school";
		data.teachers = session.teachers;
		return this.call<any>("POST", `/session`, data);
	}

	public getStudentSessions(): Observable<Session[]> {
		return this.call<Session[]>("GET", `/session/all`, null);
	}

	public getAllSessions(): Observable<Session[]> {
		return this.call<Session[]>("GET", `/session/all`, null);
	}

	public getTeacherSessions(): Observable<Session[]> {
		return this.call<Session[]>("GET", `/session/teacher`, null);
	}
	
	public joinSession(sessionCode: string): Observable<any> {
		return this.call<any>("PUT", `/session/register/${sessionCode}`, null);
	}

	public leaveSession(session: Session, user?: UserProfile): Observable<any> {
		let url = `/session/unregister/${session.sessionCode}`
		if(user){
			url += `?userId=${user.id}`;
		}
		return this.call<any>("PUT", url, null);
	}

	public updateLastVisitedContent(session: Session, sessionMode: SessionMode, lo: LearningObject): Observable<any> {
		return this.call<any>("PUT", `/session/state/${session.id}?sessionMode=${sessionMode.toString()}&loId=${lo.id}`, null);
	}

	public getLastVisitedContent(session: Session, sessionMode: SessionMode): Observable<LearningObject> {
		return this.call<LearningObject>("GET", `/session/${session.id}/state`, null);
	}

	public getActiveSessionState(session: Session): Observable<SessionActiveState> {
		return this.call<SessionActiveState>("GET", `/session/${session.id}/activeState`, null)
		.map(resp => {
			resp && resp.state && resp.state.forEach(r => {
				r.value = JSON.parse(r.value);
			});
			return resp;
		});
	}

	public submitAnswer(answer: Answer): Observable<any>{
		return this.call<any>("POST", `/activity/answer`, answer);
	}

	public endSession(session: Session, mode: SessionMode): Observable<any> {
		return this.call<any>("PUT", `/session/end/${session.id}?sessionMode=${mode}`, {});
	}

	public startSession(session: Session, mode: SessionMode): Observable<any> {
		return this.call<any>("PUT", `/session/start/${session.id}?sessionMode=${mode}`, {});
	}

	public updateVisitedSession(session: Session): Observable<any> {
		return this.call<any>("PUT", `/session/state/${session.id}`, null);
	}

	public deleteUser(user: UserProfile): Observable<any> {
		return this.call<any>("DELETE", `/user/${user.id}`, null);
	}

	public reassignRoleToUser(user: UserProfile): Observable<any> {
		return this.call<any>("PUT", `/user/${user.id}/role?roleId=${user.role.id}`, null);
	}

	public getUser(id: string): Observable<UserProfile> {
		return this.call<UserProfile>("GET", `/user/${id}`, null);
	}

	public updateUser(user: UserProfile): Observable<any>{
		return this.call("PUT", `/user/${user.id}`, user);
	}

	public getAllUsers(filter: any, page?: number, size?: number ): Observable<PagedResponse<UserProfile>> {
		page = page >= 0 ? page : 0;
		size = size > 0 ? size : 50;
		return this.call<PagedResponse<UserProfile>>("POST", `/user/all?page=${page}&size=${size}`, filter ? filter : null);
	}

	public getAllRoles(): Observable<UserRole[]> {
		return this.call<UserRole[]>("GET", `/role`, null);
	}

	public getPublisherRoles(): Observable<UserRole[]> {
		return this.call<UserRole[]>("GET", `/role/publisher`, null);
	}

	public getSchoolRoles(): Observable<UserRole[]> {
		return this.call<UserRole[]>("GET", `/role/school`, null);
	}

	public getActivity(id: string): Observable<Activity> {
		return this.call<Activity>("GET", `/activity/${id}`, null);
	}

	public createActivity(activity: Activity): Observable<Activity>{
		return this.call<Activity>("POST", `/activity`, activity);
	}

	public getActivityTypes(): Observable<string[]>{
		return this.call<string[]>("GET", `/activity/activityType`, null);
	}

	public getQuestionTypes(): Observable<string[]>{
		return this.call<string[]>("GET", `/activity/questionType`, null);
	}

	public deleteActivity(activity: Activity): Observable<any>{
		return this.call<any>("DELETE", `/activity/${activity.id}`, null);
	}

	public addQuestion(activityId: string, question: Question, index?: number): Observable<Question>{
		return this.call<Question>("POST", `/activity/${activityId}/add` + (index > 0 ? "?index=" + index : ""), question);
	}

	public editQuestion(activityId: string, question: Question, index: number): Observable<Question>{
		return this.call<Question>("POST", `/activity/${activityId}/edit?index=${index}`, question);
	}

	public getActivityEvaluation(activity: Activity, session: Session, mode: SessionMode): Observable<ActivitySummary>{
		return this.call<ActivitySummary>("GET", `/evaluation/summary/${session.id}/${activity.id}?sessionMode=${mode}`, null);
	}

	public getQuestionEvaluation(activity: Activity, session: Session, questionIndex: number, mode: SessionMode): Observable<QuestionSummary>{
		return this.call<QuestionSummary>("GET", `/evaluation/summary/${session.id}/${activity.id}/${questionIndex}?sessionMode=${mode}`, null);
	}

	public getUserEvaluationForActivity(activity: Activity, session: Session, mode: SessionMode, user?: UserProfile): Observable<UserEvaluation>{
		return this.call<UserEvaluation>("GET", `/evaluation/${session.id}/${activity.id}?sessionMode=${mode}` + (user ? "&userId=" + user.id : ""), null);
	}

	public removeQuestion(activity: Activity, questionIndex: number): Observable<any>{
		return this.call<any>("POST", `/activity/${activity.id}/remove?index=${questionIndex}`, null);
	}

	public getActivitiesForLO(sessionId: string, loId: string): Observable<Activity[]>{
		return this.call<Activity[]>("GET", `/activity/${sessionId}/${loId}`, null);
	}

	/**
	 * 
	 * @returns Observable
	 */
	public logout(): Observable<any> {
		return this.call<any>("POST", "/logout", null)
			.map(resp => {
				return {};
			});
	}

	private call<T>(method: String, relativeUrl: string, content: any, withAuth: boolean = true, supressUnauthorizedException: boolean = false): Observable<T> {
		let response = new Observable<T>((responseObserver: Observer<T>) => {
			this.getTheToken(withAuth).subscribe((t) => {
				var sub: Observable<Response> = null;
				let url = this.restUrl + relativeUrl;
				let params = new URLSearchParams();
				var opts = {
					headers: this.getHeaders(withAuth),
				};
				switch (method) {
					case "GET":
						var hasParams = false;
						for(let key in content){
							params.append(key, content[key]);
							hasParams = true;
						}
						if(hasParams){
							url += "?" + params.toString();
						}
						sub = this.http.get(url, opts);
						break;
					case "POST":
						sub = this.http.post(url, JSON.stringify(content), opts);
						break;
					case "PUT":
						sub = this.http.put(url, JSON.stringify(content), opts);
						break;
					case "DELETE":
						sub = this.http.delete(url, opts);
						break;
					case "PATCH":
						sub = this.http.patch(url, content, opts);
						break;
				}

				sub
					.map((response: any) => {
						this.responseHeaders = response.headers;
						if (response._body) {
							try {
								return response.json();
							} catch (e) { }
						}
						return null;
					})
					.subscribe(
					response => {
						responseObserver.next(response as T);
						responseObserver.complete();
					},
					(err) => {
						if (err.status == 401) {
							!supressUnauthorizedException && this.unauthorizedSource.next(err);
						} else {
							this.errorSource.next(err);
						}
						responseObserver.error(err);
					}
					);
			});
		});
		return response;
	}

	private tokenObs: Observable<string>;
	private emitter: Observer<string>;

	private getTheToken(withAuth: boolean) : Observable<string> {
		if(this.token || !withAuth){
			this.tokenObs = Observable.of(this.token);
		}else if(!this.tokenObs){
			this.tokenObs = new Observable<string>((obs: Observer<string>)=> {
				this.emitter = obs;
			});
		}
		return this.tokenObs;
	}

	private getHeaders(withAuth?: boolean) {
		var headers = new Headers();
		headers.append("Content-Type", "application/json");
		withAuth && headers.append("Authorization", this.token);
		return headers;
	}
}