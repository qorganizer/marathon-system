import { Injectable } from "@angular/core";
import { Observable, Observer, BehaviorSubject } from "rxjs/Rx";

@Injectable()
export class OAuthService{
	constructor(){

	}
	
	initialize(config){

	}

	signIn(): Observable<any>{
		return Observable.of(null);
	}

	signOut(){
		
	}

	isAuthenticated(): Observable<boolean>{
		return Observable.of(null);
	}

	isCallback(): boolean{
		return false;
	}

	isOauthEnabled(): Observable<boolean>{
		return Observable.of(null);
	}
}