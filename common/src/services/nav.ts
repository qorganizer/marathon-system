import { NavController, NavOptions } from "ionic-angular";
import { Injectable } from "@angular/core";
import { ModelStore } from "./";
import { ViewResolver } from "../utils/rbac";

@Injectable()
export class QNavigator{

	constructor(private store: ModelStore){}

	public push(nav: NavController, page: any, params?: any, opts?: NavOptions, done?: Function): Promise<any>{
		return new Promise((resolve, reject) => {
			this.getPage(page).then((p) => {
				nav.push(p, params, opts, done)
					.then((data)=>{
						resolve(data);
					}).catch((err)=>{
						console.log(err);
						resolve(null);
					});
			}).catch((err)=>{
				console.log(err);
				resolve(null);
			});
		});
	}

	public setRoot(nav: NavController, pageOrViewCtrl: any, params?: any, opts?: NavOptions, done?: Function): Promise<any>{
		return new Promise((resolve, reject) => {
			this.getPage(pageOrViewCtrl).then((p) => {
				nav.setRoot(p, params, opts, done)
					.then((data)=>{
						resolve(data);
					}).catch((err)=>{
						console.log(err);
						resolve(null);
					});
			}).catch((err)=>{
				console.log(err);
				resolve(null);
			});
		});
	};

	public getPage(page: any): Promise<any>{
		return new Promise((resolve, reject) => {
			if(page.__proto__.name == "ViewResolver"){
				resolve((<ViewResolver> new page()).get(this.store.profile.role));
			}else{
				resolve(page);
			}
		});
	}
}