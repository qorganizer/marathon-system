import { LearningObject } from '../../model/learning-object';
import { Injectable } from '@angular/core';
import { StompService, StompState } from './stomp';
import { BehaviorSubject, Observable, Observer, Subscription } from 'rxjs/Rx';

import { NotificationMessage, Session, UserProfile, Activity } from '../../model';
import { AttendanceMessage } from '../../model/protocol';
import { SockJStompService } from './service';

@Injectable()
export class SocketClient {

	protected attendanceSource: BehaviorSubject<AttendanceMessage>;
    /**
     * Attendance event
     */
	attendance: Observable<AttendanceMessage>;

	protected attentionSource: BehaviorSubject<any>;
    /**
     * Attendance event
     */
	attention: Observable<any>;

	protected notificationSource: BehaviorSubject<NotificationMessage>;
    /**
     * Attendance event
     */
	notification: Observable<NotificationMessage>;

	protected answerSource: BehaviorSubject<NotificationMessage>;
    /**
     * Attendance event
     */
	answer: Observable<NotificationMessage>;

	private socketUrl: string;
	private _stompService: StompService;
	private token;

	private subs: Subscription[] = [];

	constructor() {
		this.attendanceSource = new BehaviorSubject<AttendanceMessage>(null);
		this.attendance = this.attendanceSource.asObservable();
		this.attentionSource = new BehaviorSubject<any>(null);
		this.attention = this.attentionSource.asObservable();
		this.notificationSource = new BehaviorSubject<any>(null);
		this.notification = this.notificationSource.asObservable();		
		this.answerSource = new BehaviorSubject<any>(null);
		this.answer = this.answerSource.asObservable();
	}

	public setSocketUrl(url) {
		this.socketUrl = url;
	}

	public setToken(token) {
		this.token = token;
	}

	public sendAttention(id, active: boolean) {
		this._stompService.publish("/app/notifications/"+id, NotificationMessage.attention(active).stringify());
	}

	public sendShareScreen(id, lo: LearningObject, active: boolean) {
		this._stompService.publish("/app/notifications/"+id, NotificationMessage.shareScreen(lo, active).stringify());
	}

	public updateAttendance(id, profile: UserProfile, online: boolean = true) {
		this._stompService.publish("/app/attendance/"+id, NotificationMessage.attendance(online, profile).stringify());
	}

	public sendActivityNotification(session: Session, activity: Activity, isActive: boolean){
		this._stompService.publish("/app/notifications/"+session.id, NotificationMessage.activity(isActive, activity).stringify());
	}

	public connect(session: Session): Observable<any>{
		if(!this._stompService) {
			return new Observable<any>( (obs : Observer<any>) => {
				this.createService(session.id);
				this.subs.push(this._stompService.state.subscribe((state) => {
					console.log(state);
					switch(state) {
						case StompState.CONNECTED:
							obs.next("connected");
							break;
						case StompState.CLOSED:
							obs.error("connection closed");
							break;
						case StompState.DISCONNECTING:
							obs.error("Oups, disconnecting");
							break;
						case StompState.TRYING:
							break;
					}
				}));
			});
		}else {
			return Observable.of(null);
		}
	}

	public disconnect(){		
		if(this.subs){
			this.subs.forEach(sub => {
				sub.unsubscribe();
			});			
		}
		if(this._stompService){
			this._stompService.disconnect();
			this._stompService = null;
		}
	}

	public subscribeToAnswers(sessionId: string, activityId: string): Subscription{
		return this._stompService.subscribe(`/topic/${sessionId}/${activityId}`).subscribe((msg)=>{
			this.answerSource.next(NotificationMessage.fromString(msg.body));
		});
	}

	private subscribe(id) {
		this.subs.push(this._stompService.subscribe("/queue/attendance/"+id).subscribe((msg)=>{
			this.attendanceSource.next(NotificationMessage.fromString(msg.body).value);
		}));

		this.subs.push(this._stompService.subscribe("/topic/notifications/"+id).subscribe((msg)=>{
			this.notificationSource.next(NotificationMessage.fromString(msg.body));
		}));
	}

	protected createService(id) {
		let conf = {
			url: this.socketUrl,

			// Headers
			// Typical keys: login, passcode, host
			headers: {
				passcode: this.token
			},

			// How often to heartbeat?
			// Interval in milliseconds, set to 0 to disable
			heartbeat_in: 0, // Typical value 0 - disabled
			heartbeat_out: 20000, // Typical value 20000 - every 20 seconds

			// Wait in milliseconds before attempting auto reconnect
			// Set to 0 to disable
			// Typical value 5000 (5 seconds)
			reconnect_delay: 5000,

			// Will log diagnostics on console
			debug: true
		};

		this._stompService = new SockJStompService(conf);
		this.subscribe(id);
	}
}


