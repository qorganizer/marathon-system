import { StompService } from "./stomp";
import * as Stomp from "@stomp/stompjs";
import * as SockJS from "sockjs-client";

export class SockJStompService extends StompService {
	protected initStompClient(): void {

		// Attempt connection, passing in a callback
		let ws:any = new SockJS(this.config.url);
		this.client = Stomp.over(ws);

		// Configure client heart-beating
		this.client.heartbeat.incoming = this.config.heartbeat_in;
		this.client.heartbeat.outgoing = this.config.heartbeat_out;

		// Auto reconnect
		this.client.reconnect_delay = this.config.reconnect_delay;

		// Set function to debug print messages
		this.client.debug = this.config.debug || this.config.debug == null ? this.debug : () => { };
	}

}