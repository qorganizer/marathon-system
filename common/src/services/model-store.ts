import { Injectable, EventEmitter } from "@angular/core";
import { UserProfile, UserRole, Session, LearningObject, TableOfContents, ActivityType } from "../model";

@Injectable()
export class ModelStore{
	
	public profile: UserProfile = null;

	public roles: UserRole[];

	public users: UserProfile[] = [];

	public usersChanged: EventEmitter<any> = new EventEmitter<any>();

	public currentSession: Session;
	
	public currentLO: LearningObject;

	public tocs: TableOfContents;

	public activities: ActivityType[];

	public ssoLogin: boolean;
	public ssoEndpoint: string;
	public hash: string;
}