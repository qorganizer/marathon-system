import { Component } from "@angular/core";
import { Session, Activity, ActivityType, SessionMode, TableOfContents, SessionState, LearningObject, UserAction, QuizProgress } from "../../model";
import { RESTClient, ModelStore, SocketClient } from "../../services";
import { AlertController, ActionSheetController, NavParams, ViewController } from "ionic-angular";

@Component({
	selector: "activity-chooser",
	templateUrl: "activity-chooser.html"
})
export class ActivityChooser {

	activityTypes: ActivityType[];

	activities: Activity[];
	
	constructor(private client: RESTClient,
				private ws: SocketClient,
				private model: ModelStore,
				private alertCtrl: AlertController,
				private sheetCtrl: ActionSheetController,
				private params: NavParams,
				private viewCtrl: ViewController) {
		this.activityTypes = ActivityType.asArray();
	}

	ngAfterContentInit(){
		this.activities = this.params.get("activities");
	}

	close(type: ActivityType) {
		this.viewCtrl.dismiss(type);
	}

	shouldDisable(type: ActivityType){
		return this.activities && this.activities.filter((val) => {return val.activityType == type.toString();}).length > 0;
	}
}