import { Component, Input, NgZone } from "@angular/core";
import { Observable } from "rxjs/Rx";
import { NavController, NavParams, PopoverController, ActionSheetController } from "ionic-angular";
import { Menu, SessionMenuStructure, SubMenu, MenuState } from '../../model/ui';
@Component({
	selector: "session-menu",
	templateUrl: "session-menu.html"
})
export class SessionMenu {

	loading: boolean = true;

	@Input() structure: SessionMenuStructure = <any>{};

	constructor(public navCtrl: NavController,
				private zone: NgZone){
	}

	ionViewWillEnter(){
		
	}

	handleMenuClick(menu: Menu){
		if(menu.subMenus && menu.subMenus.length > 0){
			menu.state = menu.state == MenuState.IN ? MenuState.ENABLED : MenuState.IN;
		}else if(menu.handler){
			menu.handler();
		}
	}

	handleSubMenuClick(submenu: SubMenu){
		if(submenu && submenu.handler){
			submenu.handler();
		}
	}

	outside(menu: Menu){
		if(menu.subMenus && menu.subMenus.length > 0){
			menu.state = MenuState.ENABLED;
		}
	}
}
