import { Component, OnInit, ContentChild } from '@angular/core';

import { LoStructure }      from '../los/los';
import { LoService }        from '../los/los.service';
import { LoLayoutService }  from '../los/los-layout.service';
import { LoStimService }   from '../los/lo-stim.service';

import { HiddenDirective } from '../directives/hidden.directive';

@Component({
	templateUrl: './S03.component.html',
	//styleUrls: ['./S03.component.css'],
	providers: [
		LoService,
		LoLayoutService,
		LoStimService,
		HiddenDirective
	]
})

export class S03Component implements OnInit {

	loContent: LoStructure;
	columnLayout: object = {};
	wallpaperFile: string = '';
	hyperlinkImage: string = '';
	videoFileName;
	videoPosterFileName;
	showHyperlinkImage: boolean = false;
	showMagnifiedVideo: boolean = false;
	clickCounter: number = 0;

	constructor (
		private loService: LoService,
		private loLayoutService: LoLayoutService,
		private loStimService: LoStimService
	) {}

	ngOnInit() {
		this.getLoContent();
	}

	getLoContent(): void {
		this.loService.getLoContent().then(loContent => {

			this.loContent = loContent;
			console.log('S03::loContent:', loContent);

			this.columnLayout = this.loLayoutService.columnWidth(loContent);
			console.log('S03::columnLayout:', this.columnLayout);

			this.wallpaperFile = 'url(' + this.loStimService.getRichMedia(loContent)[0].wallpaperFileName + ')';
			console.log('S03::wallpaperFile:', this.wallpaperFile);

			this.hyperlinkImage = this.loStimService.getRichMedia(loContent)[3].fileName;
			console.log('S03::hyperlinkImage:', this.hyperlinkImage);

			this.videoFileName = this.loStimService.getRichMedia(loContent)[2].fileName;
			console.log('S03::videoFileName:', this.videoFileName);

			this.videoPosterFileName = this.loStimService.getRichMedia(loContent)[4].fileName;
			console.log('S03::videoPosterFileName:', this.videoPosterFileName);
		})
	}

	ShowLinkedImage(imgNum){ }

	closeImage(){}

	onDoubleClick(){
		this.clickCounter ++;
		let timer = setTimeout(() => {if(this.clickCounter == 1){this.clickCounter = 0;}}, 300);
		if(this.clickCounter == 2){
			this.showMagnifiedVideo = true;
			this.clickCounter = 0;
			clearTimeout(timer);
		}
		console.log('S03::onDoubleClick::clickCounter:', this.clickCounter);
	}

}

