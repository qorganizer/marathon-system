import { Component, OnInit, ViewChild } from '@angular/core';

import { LoStructure }   from '../los/los';
import { LoService }     from '../los/los.service';
import { LoLayoutService }   from '../los/los-layout.service';
import { StimComponent }   from '../los/lo-stim.component';

@Component({
	templateUrl: './S01.component.html',
	//styleUrls: ['./S01.component.css'],
	providers: [
		LoService,
		LoLayoutService
	]
})

export class S01Component implements OnInit {

	loContent: LoStructure;
	columnLayout: object = {};

	@ViewChild(StimComponent) stimulus: StimComponent;

	constructor (
		private loService: LoService,
		private loLayoutService: LoLayoutService
	) {  }

	ngOnInit() {
		this.getLoContent();
	}

	getLoContent(): void {
		this.loService.getLoContent().then(loContent => {
			this.loContent = loContent;
			console.log('S01::loContent:', loContent);
			this.columnLayout = this.loLayoutService.columnWidth(loContent);
		})
	}

}

