import { Component, OnInit } from '@angular/core';

import { LoStructure }   from '../los/los';
import { LoService }     from '../los/los.service';
import { LoLayoutService }   from '../los/los-layout.service';


@Component({
	templateUrl: './S08.component.html',
	//styleUrls: ['./S08.component.css'],
	providers: [
		LoService,
		LoLayoutService
	]
})

export class S08Component implements OnInit {

	loContent: LoStructure;
	columnLayout: object = {};
	videoFile: string = '';

	constructor (
		private loService: LoService,
		private loLayoutService: LoLayoutService
	) {}

	ngOnInit() {
		this.getLoContent();
	}

	getLoContent(): void {
		this.loService.getLoContent().then(loContent => {
			this.loContent = loContent;
			console.log('S08::loContent:', loContent);
			this.videoFile = '../../../src/assets/video/280_1';
		})
	}

	onClickFlipBtn(){
		document.querySelector("#myCard").classList.toggle("flip")
	}

}

