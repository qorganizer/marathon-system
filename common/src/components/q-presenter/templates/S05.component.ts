import { Component, OnInit, ContentChild, AfterContentInit, AfterContentChecked } from '@angular/core';

import { LoStructure }      from '../los/los';
import { StimComponent }      from '../los/lo-stim.component'
import { LoService }        from '../los/los.service';
import { LoLayoutService }  from '../los/los-layout.service';
import { LoStimService }    from '../los/lo-stim.service';


@Component({
	templateUrl: './S05.component.html',
	//styleUrls: ['./S05.component.css'],
	providers: [
		LoService,
		LoLayoutService,
		LoStimService,
		StimComponent
	]
})

export class S05Component implements OnInit {

	loContent: LoStructure;
	columnLayout: object = {};
	videoFile: string = '';
	imageFile: string = '';
	videoShow:boolean = false;
	imageShow:boolean = false;
	triggerRichMedia:object = {};

	constructor (
		private loService: LoService,
		private loLayoutService: LoLayoutService,
		private loStimService: LoStimService,
		private stimComponent: StimComponent
	) {}

	ngOnInit() {
		this.getLoContent();
	}

	// Query for a CONTENT child of type `ChildComponent`
	//@ContentChild(StimComponent) contentChild: StimComponent;

	getLoContent(): void {
		this.loService.getLoContent().then(loContent => {

			this.loContent = loContent;
			console.log('S05::loContent:', loContent);

			this.columnLayout = this.loLayoutService.columnWidth(loContent);
			//console.log('S05::columnLayout:', this.columnLayout);

			//this.videoFile = 'http://vjs.zencdn.net/v/oceans.mp4';
			/*this.videoFile = '../../../src/assets/video/280_1';
			this.imageFile = '../../../src/assets/images/279_1';
			if(this.loContent.learningObjectId == 280){this.videoShow = true; this.imageShow = false;}
			if(this.loContent.learningObjectId == 279){this.imageShow = true; this.videoShow = false;}*/
		});
		//
	}


}

