import { Component, OnInit } from '@angular/core';

import { LoStructure }   from '../los/los';
import { LoService }     from '../los/los.service';


@Component({
	selector: 'lo-NotFound',
	templateUrl: './loNotFound.component.html',
	//styleUrls: ['./loNotFound.component.css'],
	providers: [LoService]
})

export class loNotFoundComponent implements OnInit {

	loContent: LoStructure;

	constructor (private loService: LoService) {}

	ngOnInit() {
		this.getLoContent();
	}

	getLoContent(): void {
		this.loService.getLoContent().then(loContent => {
			this.loContent = loContent;
			console.log('loNotFound::loContent:', loContent);


		})
	}

}

