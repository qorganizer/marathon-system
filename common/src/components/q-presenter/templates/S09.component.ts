import { Component, OnInit } from '@angular/core';

import { LoStructure }   from '../los/los';
import { LoService }     from '../los/los.service';
import { LoLayoutService }   from '../los/los-layout.service';


@Component({
	templateUrl: './S09.component.html',
	//styleUrls: ['./S09.component.css'],
	providers: [
		LoService,
		LoLayoutService
	]
})

export class S09Component implements OnInit {

	loContent: LoStructure;
	columnLayout: object = {};

	constructor (
		private loService: LoService,
		private loLayoutService: LoLayoutService
	) {}

	ngOnInit() {
		this.getLoContent();
	}

	getLoContent(): void {
		this.loService.getLoContent().then(loContent => {
			this.loContent = loContent;
			console.log('S09::loContent:', loContent);
		})
	}

}

