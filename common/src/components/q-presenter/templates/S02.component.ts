import { Component, OnInit } from '@angular/core';

import { LoStructure }   from '../los/los';
import { LoService }     from '../los/los.service';
import { LoLayoutService }   from '../los/los-layout.service';


@Component({
	templateUrl: './S02.component.html',
	//styleUrls: ['./S02.component.css'],
	providers: [
		LoService,
		LoLayoutService
	]
})

export class S02Component implements OnInit {

	loContent: LoStructure;
	columnLayout: object = {};

	constructor (
		private loService: LoService,
		private loLayoutService: LoLayoutService
	) {}

	ngOnInit() {
		this.getLoContent();
	}

	getLoContent(): void {
		this.loService.getLoContent().then(loContent => {
			this.loContent = loContent;
			console.log('S02::loContent:', loContent);
			this.columnLayout = this.loLayoutService.columnWidth(loContent);
		})
	}

}

