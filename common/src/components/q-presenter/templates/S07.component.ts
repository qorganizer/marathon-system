import { Component, OnInit } from '@angular/core';

import { LoStructure }   from '../los/los';
import { LoService }     from '../los/los.service';
import { LoStimService }   from '../los/lo-stim.service';


@Component({
	templateUrl: './S07.component.html',
	//styleUrls: ['./S07.component.css'],
	providers: [
		LoService,
		LoStimService
	]
})

export class S07Component implements OnInit {

	loContent: LoStructure;
	triggerRichMedia: object = {};
	wallpaperFile: string = '';

	constructor (
		private loService: LoService,
		private loStimService: LoStimService
	) {}

	ngOnInit() {
		this.getLoContent();
	}

	getLoContent(): void {
		this.loService.getLoContent().then(loContent => {

			this.loContent = loContent;
			console.log('S07::loContent:', loContent);

			this.wallpaperFile = 'url(' + this.loStimService.getRichMedia(loContent)[0].wallpaperFileName + ')';
			console.log('S07::wallpaperFile:', this.wallpaperFile);

		})
	}

}

