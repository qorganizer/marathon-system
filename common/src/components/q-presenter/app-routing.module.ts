
import { NgModule }              from '@angular/core';
import { RouterModule, Routes }  from '@angular/router';

import { loNotFoundComponent }   from './templates/loNotFound.component';


const appRoutes: Routes = [
  { path: '',   redirectTo: 'SR-01-00/280', pathMatch: 'full' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
