import { TableOfContent } from './navbar';

export const TOC: TableOfContent =
	{
		"blockId": "MSNC04_TOC01",
		"subject":"Biology",
		"stream":"HAVO",
		"level":1,
		"title": "block 1",
		"displayName": "<b>You'rE Up<br><br>Studielast 16 uur</b>",
		"slu": "Blok SLU",
		"concept": "Begrippen",
		"description": "Beschrijving",
		"goals": "Doelen",
		"requiredKnowldge": "Benodigde kennis",
		"commands": "Opdrachten in blok",
		"blockMaterials": "Materialen",
		"blockStudentInfo": "Info voor leerling",
		"blockTeacherInfo": "Info voor leraar",
		"blockSloskills": "SLO Skills Aims",
		"blockSloknowhow": "SLO Know-How Aims",
		"blockSlo21stcentury": "SLO 21st Century Aims",
		"tasks":
			[
				{
					"taskId": "MSNC01_TOC0400",
					"title": "Task 0 - Reisvoorbereiding",
					"displayName": "Task 0 - Reisvoorbereiding",
					"resume": "Samenvatting",
					"SLU": "SLU",
					"learningObjective": "Leerdoelen",
					"task": "Taakopdrachten",
					"taskMaterials": "Materialen",
					"taskStudentInfo": "Info voor leraar",
					"taskTeacherInfo": "Info voor leerling",
					"los": [
						{
							"index": 1,
							"loId": "1241",
							"color":"red",
							"title":"Fancy a surprise dinner this weekend?",
							"loTemp":"SR-03-02",
							"target":"target info"
						},
			{
							"index": 2,
							"loId": "126",
							"color":"red",
							"title":"Top of flop?",
							"loTemp":"SR-04-01",
							"target":"target info"
						},
			{
							"index": 3,
							"loId": "280",
							"color":"red",
							"title":"Waarom eet je chocolade als je baalt?",
							"loTemp":"SR-05-04",
							"target":"target info"
						},
						{
							"index": 4,
							"loId": "279",
							"color":"red",
							"title":"Griekse mythes",
							"loTemp":"SR-05-05",
							"target":"target info"
						},
			{
							"index": 5,
							"loId": "130",
							"color":"red",
							"title":"Waarom eet je chocolade als je baalt?",
							"loTemp":"SR-08-00",
							"target":"target info"
						}
					]
				},
				{
					"taskId": "MSNC01_TOC0401",
					"title": "Task A - Analyseren Klokhuisvideo",
					"displayName": "Task A - Analyseren Klokhuisvideo",
					"resume": "Samenvatting",
					"SLU": "SLU",
					"learningObjective": "Leerdoelen",
					"task": "Taakopdrachten",
					"taskMaterials": "Materialen",
					"taskStudentInfo": "Info voor leraar",
					"taskTeacherInfo": "Info voor leerling",
					"los": [
						{
							"index": 1,
							"loId": "Wat_ga_ik_doen_1",
							"color":"yellow",
							"title":"Wat ga ik doen?",
							"loTemp":"SY-03-02",
							"target":"target info"
						},
						{
							"index": 2,
							"loId": "Klokhuis_analyse",
							"color":"green",
							"title":"Klokhuis analyse",
							"loTemp":"SG-05-06",
							"target":"target info"
						},
						{
							"index": 3,
							"loId": "Check_het_zelf!",
							"color":"yellow",
							"title":"Check het zelf!",
							"loTemp":"TY-10-03",
							"target":"target info"
						},
						{
							"index": 4,
							"loId": "Onze_reisbestemming",
							"color":"green",
							"title":"Onze reisbestemming",
							"loTemp":"SG-05-04",
							"target":"target info"
						},
						{
							"index": 5,
							"loId": "Build_my_Challenge",
							"color":"green",
							"title":"Build my Challenge",
							"loTemp":"SG-05-03",
							"target":"target info"
						},
						{
							"index": 6,
							"loId": "Top_of_flop",
							"color":"green",
							"title":"Top of flop?",
							"loTemp":"SG-05-04",
							"target":"target info"
						},
						{
							"index": 7,
							"loId": "Ik_ga_op_reis_en_neem_mee_1",
							"color":"yellow",
							"title":"Ik ga op reis en neem mee ...",
							"loTemp":"RY-02-04",
							"target":"target info"
						}
					]
				},
				{
					"taskId": "MSNC01_TOC0402",
					"title": "Task B - Storyboards maken en filmtips",
					"displayName": "Task B - Storyboards maken en filmtips",
					"resume": "Samenvatting",
					"SLU": "SLU",
					"learningObjective": "Leerdoelen",
					"task": "Taakopdrachten",
					"taskMaterials": "Materialen",
					"taskStudentInfo": "Info voor leraar",
					"taskTeacherInfo": "Info voor leerling",
					"los": [
						{
							"index": 1,
							"loId": "Wat_weet_ik_al",
							"color":"green",
							"title":"Wat weet ik al?",
							"loTemp":"SG-05-04",
							"target":"target info"
						},
						{
							"index": 2,
							"loId": "Een_snelle_blik_op_storyboard",
							"color":"red",
							"title":"Een snelle blik op storyboard",
							"loTemp":"SR-05-04",
							"target":"target info"
						},
						{
							"index": 3,
							"loId": "Wat_ga_ik_doen_2",
							"color":"yellow",
							"title":"Wat ga ik doen?",
							"loTemp":"SY-03-02",
							"target":"target info"
						},
						{
							"index": 4,
							"loId": "Camerashots",
							"color":"red",
							"title":"Camerashots",
							"loTemp":"SR-05-06",
							"target":"target info"
						},
						{
							"index": 5,
							"loId": "Storyboard",
							"color":"red",
							"title":"Storyboard",
							"loTemp":"SR-05-04",
							"target":"target info"
						},
						{
							"index": 6,
							"loId": "Video_over_storyboard",
							"color":"red",
							"title":"Video over storyboard",
							"loTemp":"SR-05-04",
							"target":"target info"
						},
						{
							"index": 7,
							"loId": "Dirk_is_scheel",
							"color":"red",
							"title":"Dirk is scheel.",
							"loTemp":"SR-05-06",
							"target":"target info"
						},
						{
							"index": 8,
							"loId": "Start!",
							"color":"yellow",
							"title":"Start!",
							"loTemp":"SY-06-00",
							"target":"target info"
						},
						{
							"index": 9,
							"loId": "Vraag_1",
							"color":"yellow",
							"title":"Vraag 1",
							"loTemp":"SR-05-04",
							"target":"target info"
						},
						{
							"index": 10,
							"loId": "Vraag_2",
							"color":"yellow",
							"title":"Vraag 2",
							"loTemp":"SY-05-03",
							"target":"target info"
						},
						{
							"index": 11,
							"loId": "Vraag_3",
							"color":"yellow",
							"title":"Vraag 3",
							"loTemp":"SY-05-03",
							"target":"target info"
						},
						{
							"index": 12,
							"loId": "Vraag_4",
							"color":"yellow",
							"title":"Vraag 4",
							"loTemp":"SY-05-03",
							"target":"target info"
						},
						{
							"index": 13,
							"loId": "Vraag_5",
							"color":"yellow",
							"title":"Vraag 5",
							"loTemp":"SY-05-03",
							"target":"target info"
						},
						{
							"index": 14,
							"loId": "Vraag_6",
							"color":"yellow",
							"title":"Vraag 6",
							"loTemp":"SY-05-03",
							"target":"target info"
						},
						{
							"index": 15,
							"loId": "Vraag_7",
							"color":"yellow",
							"title":"Vraag 7",
							"loTemp":"SY-05-03",
							"target":"target info"
						},
						{
							"index": 16,
							"loId": "Vraag_8",
							"color":"yellow",
							"title":"Vraag 8",
							"loTemp":"SY-05-03",
							"target":"target info"
						},
						{
							"index": 17,
							"loId": "Vraag_9",
							"color":"yellow",
							"title":"Vraag 9",
							"loTemp":"SY-05-03",
							"target":"target info"
						},
						{
							"index": 18,
							"loId": "Vraag_10",
							"color":"yellow",
							"title":"Vraag 10",
							"loTemp":"SY-05-03",
							"target":"target info"
						},
						{
							"index": 19,
							"loId": "Love_story",
							"color":"red",
							"title":"Love story",
							"loTemp":"SR-05-04",
							"target":"target info"
						},
						{
							"index": 20,
							"loId": "Waar_ga_ik_op_letten",
							"color":"green",
							"title":"Waar ga ik op letten?",
							"loTemp":"SG-05-04",
							"target":"target info"
						},
						{
							"index": 21,
							"loId": "Zelf_storyboard_maken",
							"color":"green",
							"title":"Zelf storyboard maken",
							"loTemp":"SG-05-04",
							"target":"target info"
						},
						{
							"index": 22,
							"loId": "Filmtips",
							"color":"red",
							"title":"Filmtips",
							"loTemp":"SR-05-04",
							"target":"target info"
						},
						{
							"index": 23,
							"loId": "Ik_ga_op_reis_en_neem_mee_2",
							"color":"yellow",
							"title":"Ik ga op reis en neem mee...",
							"loTemp":"RY-02-04",
							"target":"target info"
						}
					]
				},
				{
					"taskId": "MSNC01_TOC0403",
					"title": "Task C - Film Bewerken",
					"displayName": "Task C - Film Bewerken",
					"resume": "Samenvatting",
					"SLU": "SLU",
					"learningObjective": "Leerdoelen",
					"task": "Taakopdrachten",
					"taskMaterials": "Materialen",
					"taskStudentInfo": "Info voor leraar",
					"taskTeacherInfo": "Info voor leerling",
					"los": [
						{
							"index": 1,
							"loId": "Film_bewerken",
							"color":"yellow",
							"title":"Film bewerken",
							"loTemp":"SY-03-02",
							"target":"target info"
						}
					]
				},
				{
					"taskId": "MSNC01_TOC0404",
					"title": "Task D - Sketches opnemen",
					"displayName": "Task D - Sketches opnemen",
					"resume": "Samenvatting",
					"SLU": "SLU",
					"learningObjective": "Leerdoelen",
					"task": "Taakopdrachten",
					"taskMaterials": "Materialen",
					"taskStudentInfo": "Info voor leraar",
					"taskTeacherInfo": "Info voor leerling",
					"los": [
						{
							"index": 1,
							"loId": "Wat_ga_ik_doen_4",
							"color":"yellow",
							"title":"Wat ga ik doen?",
							"loTemp":"SY-03-02",
							"target":"target info"
						},
						{
							"index": 2,
							"loId": "Ontdekken_4",
							"color":"green",
							"title":"Ontdekken",
							"loTemp":"SG-05-03",
							"target":"target info"
						},
						{
							"index": 3,
							"loId": "Aan_de_slag!_4",
							"color":"green",
							"title":"Aan de slag!",
							"loTemp":"SG-05-04",
							"target":"target info"
						},
						{
							"index": 4,
							"loId": "Ik_ga_op_reis_en_neem_mee_4",
							"color":"yellow",
							"title":"Ik ga op reis en neem mee...",
							"loTemp":"SY-03-02",
							"target":"target info"
						}
					]
				},
				{
					"taskId": "MSNC01_TOC0405",
					"title": "Task E - De EU",
					"displayName": "Task E - De EU",
					"resume": "Samenvatting",
					"SLU": "SLU",
					"learningObjective": "Leerdoelen",
					"task": "Taakopdrachten",
					"taskMaterials": "Materialen",
					"taskStudentInfo": "Info voor leraar",
					"taskTeacherInfo": "Info voor leerling",
					"los": [
						{
							"index": 1,
							"loId": "Gekke_Engelsen!",
							"color":"yellow",
							"title":"Gekke Engelsen?!",
							"loTemp":"SY-06-00",
							"target":"target info"
						},
						{
							"index": 2,
							"loId": "Wat_weet_ik_er_al_van",
							"color":"yellow",
							"title":"Wat weet ik er al van?",
							"loTemp":"SY-05-03",
							"target":"target info"
						},
						{
							"index": 3,
							"loId": "Wat_ga_ik_doen_5",
							"color":"green",
							"title":"Wat ga ik doen?",
							"loTemp":"SY-03-02",
							"target":"target info"
						},
						{
							"index": 4,
							"loId": "De_EU_in_het_kort",
							"color":"red",
							"title":"De EU in het kort",
							"loTemp":"SR-05-03",
							"target":"target info"
						},
						{
							"index": 5,
							"loId": "Meer_EU",
							"color":"red",
							"title":"Meer EU",
							"loTemp":"SR-05-04",
							"target":"target info"
						},
						{
							"index": 6,
							"loId": "Ready",
							"color":"yellow",
							"title":"Ready?",
							"loTemp":"SY-05-01",
							"target":"target info"
						},
						{
							"index": 7,
							"loId": "Europa_Quiz",
							"color":"yellow",
							"title":"Europa Quiz",
							"loTemp":"SY-05-03",
							"target":"target info"
						},
						{
							"index": 8,
							"loId": "Onderhandelingsspel",
							"color":"green",
							"title":"Onderhandelspel",
							"loTemp":"SR-05-04",
							"target":"target info"
						},
						{
							"index": 9,
							"loId": "Ik_ga_op_reis_en_neem_mee_5",
							"color":"yellow",
							"title":"Ik ga op reis en neem mee...",
							"loTemp":"SY-05-03",
							"target":"target info"
						},
						{
							"index": 10,
							"loId": "Final_check",
							"color":"yellow",
							"title":"Final check",
							"loTemp":"RY-02-04",
							"target":"target info"
						}
					]
				},
				{
					"taskId": "MSNC01_TOC0406",
					"title": "Task F - Leren discussieren",
					"displayName": "Task F - Leren discussieren",
					"resume": "Samenvatting",
					"SLU": "SLU",
					"learningObjective": "Leerdoelen",
					"task": "Taakopdrachten",
					"taskMaterials": "Materialen",
					"taskStudentInfo": "Info voor leraar",
					"taskTeacherInfo": "Info voor leerling",
					"los": [
						{
							"index": 1,
							"loId": "Wat_ga_ik_doen_6",
							"color":"yellow",
							"title":"Wat ga ik doen?",
							"loTemp":"SY-03-02",
							"target":"target info"
						},
						{
							"index": 2,
							"loId": "Aan_de_slag!_6",
							"color":"green",
							"title":"Aan de slag!",
							"loTemp":"SG-05-03",
							"target":"target info"
						},
						{
							"index": 3,
							"loId": "Stemmen_1",
							"color":"green",
							"title":"Stemmen 1",
							"loTemp":"SG-07-01",
							"target":"target info"
						},
						{
							"index": 4,
							"loId": "Stemmen_2",
							"color":"green",
							"title":"Stemmen 2",
							"loTemp":"SG-07-01",
							"target":"target info"
						},
						{
							"index": 5,
							"loId": "Stemmen_3",
							"color":"green",
							"title":"Stemmen 3",
							"loTemp":"SG-07-01",
							"target":"target info"
						},
						{
							"index": 6,
							"loId": "Ik_ga_op_reis_en_neem_mee_6",
							"color":"yellow",
							"title":"Ik ga op reis en neem mee...",
							"loTemp":"RY-02-04",
							"target":"target info"
						}
					]
				},
				{
					"taskId": "MSNC01_TOC0407",
					"title": "Task 7 SLH Student",
					"displayName": "Task 7 SLH Student",
					"resume": "Samenvatting",
					"SLU": "SLU",
					"learningObjective": "Leerdoelen",
					"task": "Taakopdrachten",
					"taskMaterials": "Materialen",
					"taskStudentInfo": "Info voor leraar",
					"taskTeacherInfo": "Info voor leerling",
					"los": [
						{
							"index": 1,
							"loId": "Wat_ga_ik_doen_7",
							"color":"yellow",
							"title":"Wat ga ik doen?",
							"loTemp":"SY-03-02",
							"target":"target info"
						},
						{
							"index": 2,
							"loId": "Ontdekken_7",
							"color":"green",
							"title":"Ontdekken",
							"loTemp":"SG-05-03",
							"target":"target info"
						},
						{
							"index": 3,
							"loId": "Aan_de_slag!_7",
							"color":"green",
							"title":"Aan de slag!",
							"loTemp":"SG-05-04",
							"target":"target info"
						},
						{
							"index": 4,
							"loId": "Ik_ga_op_reis_en_neem_mee_7",
							"color":"yellow",
							"title":"Ik ga op reis en neem mee...",
							"loTemp":"RY-02-04",
							"target":"target info"
						}
					]
				},
				{
					"taskId": "MSNC01_TOC0408",
					"title": "Task 8 SLH Student",
					"displayName": "Task 8 SLH Student",
					"resume": "Samenvatting",
					"SLU": "SLU",
					"learningObjective": "Leerdoelen",
					"task": "Taakopdrachten",
					"taskMaterials": "Materialen",
					"taskStudentInfo": "Info voor leraar",
					"taskTeacherInfo": "Info voor leerling",
					"los": [
						{
							"index": 1,
							"loId": "Finale",
							"color":"yellow",
							"title":"Finale",
							"loTemp":"SY-07-01",
							"target":"target info"
						},
						{
							"index": 2,
							"loId": "The_End",
							"color":"yellow",
							"title":"The End",
							"loTemp":"SY-07-01",
							"target":"target info"
						}
					]
				}
			]
	}
;
