import { Component, OnInit } from '@angular/core';

import { TableOfContent }    from './navbar';
import { NavbarService }     from './navbar.service';


@Component({
	selector: 'app-navbar',
	templateUrl: './navbar.component.html',
	//styleUrls: ['./navbar.component.css'],
	providers: [NavbarService]
})

export class NavbarComponent implements OnInit {

	tableOfContent: TableOfContent;
	displayNavbar: boolean = false;
	taskSelected:number = 0;
	loSelected:number = 0;
	prevSel:number = null;
	showLOs:boolean = false;

	constructor (private navbarService: NavbarService) {}

	ngOnInit() {
		this.getNavbarContent();
	}

	getNavbarContent(): void {
		this.navbarService.getNavbarContent().then(tableOfContent => {
			this.tableOfContent = tableOfContent;
			//console.log(tableOfContent.tasks.length);
		});
	}

	navbarDisplay(display): void {
		this.displayNavbar = display;
		console.log(display);
	}

	selectedTask(selected): void{
		this.taskSelected = selected;
		console.log('selectedTask::taskSelected:', selected);

		/* set first/second click actions */
		if(this.prevSel == selected) {
			this.showLOs = true;
			this.prevSel = null;
		}
		else {
			this.showLOs = false;
			this.prevSel = selected;
		}

	}

	selectedLo(selected): void {
		this.loSelected = selected;
		console.log('selectedTask::loSelected:', selected);
	}
}
