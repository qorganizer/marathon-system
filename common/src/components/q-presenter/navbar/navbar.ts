
export class TableOfContent{
	blockId: string;
	title: string;
	displayName:string;
	subject: string;
	stream: string;
	level: number;
	slu: string;
	concept: string;
	description: string;
	goals: string;
	requiredKnowldge: string;
	commands: string;
	blockMaterials: string;
	blockStudentInfo: string;
	blockTeacherInfo: string;
	blockSloskills: string;
	blockSloknowhow: string;
	blockSlo21stcentury: string;
	tasks: TOCTask[] = [];
}

export class TOCTask{
	taskId: string;
	title: string;
	displayName: string;
	resume: string;
	SLU: string;
	learningObjective: string;
	task: string;
	taskMaterials: string;
	taskStudentInfo: string;
	taskTeacherInfo: string;
	los: TOCLearningObject[] = [];
}

export class TOCLearningObject{
	loId: string;
	title: string;
	index: number;
	color: string;
	loTemp: string;
	target: string;
}
