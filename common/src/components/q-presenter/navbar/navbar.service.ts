import { Injectable } from '@angular/core';

import { TableOfContent } from './navbar';
import { TOC }            from './mock-toc';

@Injectable()
export class NavbarService {
	getNavbarContent(): Promise<TableOfContent> {
		return Promise.resolve(TOC);
	}
}
