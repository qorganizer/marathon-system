import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';

import { LoStructure }   from './los';
import { LoService }     from './los.service';


@Component({
	selector: 'header-child',
	templateUrl: './los.header.html',
	//styleUrls: ['./los.header.css'],
	providers: [LoService]
})

export class LoHeader implements OnInit {

	loContent: LoStructure;
	btnColor: string = '';

	@ViewChild('header') header;

	constructor (
		private loService: LoService,
		private viewContainerRef: ViewContainerRef
	) {}

	ngOnInit() {
		this.getLoContent();
	}

	getLoContent(): void {
		console.log('LoHeader::getLoContent');
		this.loService.getLoContent().then(loContent => {
			this.loContent = loContent;

			this.viewContainerRef.createEmbeddedView(this.header);

			/* used when LO button is displayed by the presenter */
			/*switch (loContent.metadata.LOColor) {
				case 'blue':
					this.btnColor = 'btn-primary';
					break;
				case 'green':
					this.btnColor = 'btn-success';
					break;
				case 'red':
					this.btnColor = 'btn-danger';
					break;
				case 'yellow':
					this.btnColor = 'btn-warning';
					break;
				default:
					this.btnColor = 'btn-normal';
			}*/

		});
	}

	showLoInfo(){
		console.log('showLoInfo');
	}

}

