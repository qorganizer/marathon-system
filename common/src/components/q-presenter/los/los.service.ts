import { Injectable }     from '@angular/core';
import { Location }       from '@angular/common';
import { Router }         from '@angular/router';
import { Http, Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { LoStructure } from './los';
import { LOD }         from './mock-lo';

@Injectable()
export class LoService {

	constructor (
		private http: Http,
		private router: Router,
		private location: Location )  {
		this.router = router;
		this.location = location;
	}

	getLoContent(): Promise<any> {
		let currentUrl = this.location.path(); //this.router.url;
		let loURL = 'assets' + currentUrl.substr(currentUrl.lastIndexOf('/')) + '.json';
		console.log('getLoContent::currentUrl:', currentUrl);
		console.log('getLoContent::loURL:', loURL);

		return this.http.get(loURL)
			.toPromise()
			.then(this.extractData)
			.catch(this.handleError);
	}

	private extractData(res: Response) {
		let loContent = res.json();
		console.log('extractData::loContent:', loContent);
		return loContent;
	}

	private handleError (error: Response | any) {
		// In a real world app, you might use a remote logging infrastructure
		let errMsg: string;
		if (error instanceof Response) {
			const loCont = error.json() || '';
			const err = loCont.error || JSON.stringify(loCont);
			errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
		} else {
			errMsg = error.message ? error.message : error.toString();
		}
		console.error(errMsg);
		return Observable.throw(errMsg);
	}

}

