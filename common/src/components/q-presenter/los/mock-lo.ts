import { LoStructure }   from './los';

export const LOD: LoStructure =
{
	"title": "Aan de slag!",
	"time": "60",
	"learningObjectId": 242,
	"sortOrder": 3,
	"metadata": {
		"adaptiveLevel": "3",
		"adaptiveUse": "key_basic",
		"compulsory": "key_yes",
		"linkType": "key_noLink",
		"liveActv": "none",
		"liveActvType": "",
		"marker": "key_yes",
		"organization": "",
		"objective": "",
		"pen": "key_yes",
		"sticky": "key_yes",
		"tts": "key_yes",
		"helpingHands": "",
		"activity": "In de workshop.",
		"target": "Ik heb de workshop gedaan.",
		"output": "",
		"teacherInfo": "",
		"liveActvContent": "",
		"LOColor": "green",
		"LOTemplate": "SG-05-04",
		"instructions": "Start je excursie met je excursieleider"
	},
	"content": {
		"key_triggerText": {
			"value": ""
		},
		"key_contentText": {
			"value": "<p>In de workhop ga je met je excursieleider aan de slag.</p><br/><p>Zij leert je hoe je <strong>sketches</strong>* voorbereidt en opneemt als een echte pro!</p><br/><p>Check vooraf aan je excursie in Outlook Groups of er nog belangrijke dingen zijn die je moet weten over deze workshop. Denk aan lokaal, tijden, dingen die je mee moet nemen.</p><br/><p>Take 1 in 3...2...1!</p><br/><p>*Een&nbsp;<strong>sketch</strong>&nbsp;is een kort komisch optreden met één of meer acteurs.&nbsp;</p>"
		}
	},
	"richMedia": [
		{
			"type": "image",
			"item": "Trigger",
			"description_link": "<p>https://drive.google.com/open?id=0B3NZmIF41_c9Q2I3X1VOLUp4UDg</p><br/>",
			"text_on_image": "",
			"effects": ""
		}
	]
};
