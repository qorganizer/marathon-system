import { NgModule }       from '@angular/core';
import { CommonModule }   from '@angular/common';
import { FormsModule }    from '@angular/forms';

import { S01Component }   from '../templates/S01.component';
import { S02Component }   from '../templates/S02.component';
import { S03Component }   from '../templates/S03.component';
import { S04Component }   from '../templates/S04.component';
import { S05Component }   from '../templates/S05.component';
import { S06Component }   from '../templates/S06.component';
import { S07Component }   from '../templates/S07.component';
import { S08Component }   from '../templates/S08.component';
import { S09Component }   from '../templates/S09.component';

import { loNotFoundComponent } from '../templates/loNotFound.component';

import { LoRoutingModule } from './lo-routing.module';

import { LoService } from './los.service';

import { StimComponent }    from './lo-stim.component';
//import { VideoJSComponent } from '../videojs/videojs.component'
import { SanitizeHtml }     from "../pipes/sanitizeHtml.pipe"
import { SharedModule } from "../directives/shared.module";

import { LoHeader } from '../los/los.header'

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		LoRoutingModule,
		SharedModule
	],
	declarations: [
		S01Component,
		S02Component,
		S03Component,
		S04Component,
		S05Component,
		S06Component,
		S07Component,
		S08Component,
		S09Component,
		loNotFoundComponent,
		StimComponent,
		//VideoJSComponent,
		LoHeader,
		SanitizeHtml
	],
	providers: [ LoService ]
})
export class LosModule {}
