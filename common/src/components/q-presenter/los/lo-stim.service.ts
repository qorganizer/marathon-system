import { Injectable } from '@angular/core';

import * as AppConst from '../app.constants';

@Injectable()
export class LoStimService {

	constructor() { }

	imageNum: number = 0;
	videoNum: number = 0;
	audioNum: number = 0;

	getCarouselState(loTemp): Boolean {
		let carousel:Boolean = false;
		switch(loTemp){
			case 'SR-05-06':
			case 'SY-05-06':
			case 'SG-05-06':
			case 'SR-05-07':
			case 'SY-05-07':
			case 'SG-05-07':
			case 'SR-05-08':
			case 'SY-05-08':
			case 'SG-05-08':
			case 'SR-07-02':
			case 'SY-07-02':
			case 'SG-07-02':
				carousel = true;
				break;
			default:
				carousel = false;
				break;
		}
		return carousel;
	}

	/* carousel image */
	getCarouselImages(btn): Array<boolean> {
		let showImg = [];
		let imgNum = 0;

		if(btn === 'back' && imgNum > 0){
			imgNum--;
		}
		if(btn === 'next' && imgNum < ((this.imageNum + this.videoNum) - 1)){
			imgNum++;
		}
		showImg[imgNum] = true;
		console.log('showImg:', showImg);
		return showImg;
	}

	getRichMedia(LO): Array<any>{

		let stimMediaExist: boolean = false;
		let MEDIA_PATH: string = AppConst.MEDIA_PATH; //'https://www.marathon-group.net/QNL/'; //
		let mediaTypeNum: string = '';
		let indx: number = 0;
		var triggerRichMedia = new Array();
		var wallpaperFileName = '';

		var imageNum = 0;
		var videoNum = 0;
		var audioNum = 0;

		LO.richMedia.forEach(function(richMediaItem) {
			// if (richMediaItem.item === 'Trigger' || richMediaItem.item === 'Content' || richMediaItem.item === 'Wallpapper' || richMediaItem.item === 'Hyperlink') {
				stimMediaExist = true;
				triggerRichMedia.push(richMediaItem);
				switch (richMediaItem.type) {
					case 'image':
					case 'presentation':
						imageNum++;
						mediaTypeNum = '_' + imageNum + '.png';
						break;
					case 'video':
						videoNum++;
						mediaTypeNum = '_' + videoNum;
						break;
					case 'audio':
						audioNum++;
						mediaTypeNum = '_' + audioNum;
						break;
				}

				/* full path of media files */
				triggerRichMedia[indx].fileName = MEDIA_PATH + richMediaItem.type + '/' + LO.learningObjectId + mediaTypeNum;
				console.log('getRichMedia::triggerRichMedia:', triggerRichMedia[indx].fileName);
				triggerRichMedia[indx].mediaTypeNum = mediaTypeNum;

				/* full path of wallpaper file */
				if(richMediaItem.type == 'image' && richMediaItem.item == 'Wallpapper'){
					wallpaperFileName = MEDIA_PATH + richMediaItem.type + '/' + LO.learningObjectId + mediaTypeNum;
				}
				else if (richMediaItem.type == 'image' && imageNum == 1 && wallpaperFileName == '') {
					wallpaperFileName = MEDIA_PATH + richMediaItem.type + '/' + LO.learningObjectId + mediaTypeNum;
				}
				else if (richMediaItem.type == 'video' && videoNum == 1 && wallpaperFileName == '') {
					wallpaperFileName = MEDIA_PATH + richMediaItem.type + '/' + LO.learningObjectId + mediaTypeNum;
				}
				else if(wallpaperFileName == ''){
					wallpaperFileName = MEDIA_PATH + 'wallpaper/wallpaper_1.png'
				}
				triggerRichMedia[0].wallpaperFileName = wallpaperFileName;

				indx++;
		 // }
		});

		this.imageNum = imageNum;
		this.videoNum = videoNum;
		this.audioNum = audioNum;

		return triggerRichMedia;
	}

}

