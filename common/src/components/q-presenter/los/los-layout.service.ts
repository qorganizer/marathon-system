import { Injectable } from '@angular/core';

import * as AppConst from '../app.constants';

@Injectable()
export class LoLayoutService {

	columLayout = {
		"leftColWidth": 'col-xs-' + Math.round(AppConst.COLUMNS * 0.67),
		"rightColWidth": 'col-xs-' + Math.round(AppConst.COLUMNS * 0.33)
	};
	columnWidth(loTemp): Object {
		switch(loTemp.metadata.LOTemplate.substr(-5,2)) {
			case '03':
				switch (loTemp.metadata.LOTemplate.substr(-2)) {
					case '00':
						this.columLayout.leftColWidth = 'col-xs-' + Math.round(AppConst.COLUMNS * 1.05);
						this.columLayout.rightColWidth = 'col-xs-0';
						break;
					case '02':
						this.columLayout.leftColWidth = 'col-xs-23'; //'col-xs-' + Math.round(AppConst.COLUMNS * 0.625);
						this.columLayout.rightColWidth = 'col-xs-11'; //'col-xs-' + Math.round(AppConst.COLUMNS * 0.375);
						break;
					case '01':
						this.columLayout.leftColWidth = 'col-xs-' + Math.round(AppConst.COLUMNS * 0.525);
						this.columLayout.rightColWidth = 'col-xs-' + Math.round(AppConst.COLUMNS * 0.525);
						break;
					case '03':
						this.columLayout.leftColWidth = 'col-xs-' + Math.round(AppConst.COLUMNS * 0.33);
						this.columLayout.rightColWidth = 'col-xs-' + Math.round(AppConst.COLUMNS * 0.67);
						break;
				}
				break;
			case '04':
				switch (loTemp.metadata.LOTemplate.substr(-2)) {
					case '00':
						this.columLayout.leftColWidth = 'col-xs-' + Math.round(AppConst.COLUMNS * 1.05);
						this.columLayout.rightColWidth = 'col-xs-0';
						break;
					case '01':
						this.columLayout.leftColWidth = 'col-xs-' + Math.round(AppConst.COLUMNS * 0.67);
						this.columLayout.rightColWidth = 'col-xs-' + Math.round(AppConst.COLUMNS * 0.33);
						break;
					case '02':
						this.columLayout.leftColWidth = 'col-xs-' + Math.round(AppConst.COLUMNS * 0.525);
						this.columLayout.rightColWidth = 'col-xs-' + Math.round(AppConst.COLUMNS * 0.525);
						break;
					case '03':
						this.columLayout.leftColWidth = 'col-xs-' + Math.round(AppConst.COLUMNS * 0.33);
						this.columLayout.rightColWidth = 'col-xs-' + Math.round(AppConst.COLUMNS * 0.67);
						break;
				}
				break;
			case '05':
				switch (loTemp.metadata.LOTemplate.substr(-2)) {
					case '00':
						this.columLayout.leftColWidth = 'col-xs-' + Math.round(AppConst.COLUMNS * 1.05);
						this.columLayout.rightColWidth = 'col-xs-0';
						break;
					case '01':
					case '05':
						this.columLayout.leftColWidth = 'col-xs-' + Math.round(AppConst.COLUMNS * 0.67);
						this.columLayout.rightColWidth = 'col-xs-' + Math.round(AppConst.COLUMNS * 0.33);
						break;
					case '02':
						this.columLayout.leftColWidth = 'col-xs-' + Math.round(AppConst.COLUMNS * 0.525);
						this.columLayout.rightColWidth = 'col-xs-' + Math.round(AppConst.COLUMNS * 0.525);
						break;
					case '03':
					case '04':
						this.columLayout.leftColWidth = 'col-xs-' + Math.round(AppConst.COLUMNS * 0.33);
						this.columLayout.rightColWidth = 'col-xs-' + Math.round(AppConst.COLUMNS * 0.67);
						break;
				}
				break;
		}
		return this.columLayout;
	}

	stimLayout = {
		"pos": "left",
		"width": Math.round(AppConst.COLUMNS * 0.475)
	};
	stimSubTemp(loTemp): Object {
		switch(loTemp.metadata.LOTemplate){
			case 'SR-05-01':
			case 'SG-05-01':
			case 'SY-05-01':
				this.stimLayout.pos = 'left';
				this.stimLayout.width = Math.round(AppConst.COLUMNS * 0.33);
				break;
			case 'SR-05-02':
			case 'SG-05-02':
			case 'SY-05-02':
			case 'SR-05-07':
			case 'SG-05-07':
			case 'SY-05-07':
				this.stimLayout.pos = 'left';
				this.stimLayout.width = Math.round(AppConst.COLUMNS * 0.475);
				break;
			case 'SR-05-03':
			case 'SG-05-03':
			case 'SY-05-03':
			case 'SR-05-06':
			case 'SG-05-06':
			case 'SY-05-06':
				this.stimLayout.pos = 'left';
				this.stimLayout.width = Math.round(AppConst.COLUMNS * 0.67);
				break;
			case 'SR-05-05':
			case 'SG-05-05':
			case 'SY-05-05':
				this.stimLayout.pos = 'right';
				this.stimLayout.width = Math.round(AppConst.COLUMNS * 0.33);
				break;
			case 'NOT_USED':
				this.stimLayout.pos = 'right';
				this.stimLayout.width = Math.round(AppConst.COLUMNS * 0.475);
				break;
			case 'SR-05-04':
			case 'SG-05-04':
			case 'SY-05-04':
			case 'SR-05-08':
			case 'SG-05-08':
			case 'SY-05-08':
				this.stimLayout.pos = 'right';
				this.stimLayout.width = Math.round(AppConst.COLUMNS * 0.67);
				break;
	}
	return this.stimLayout;
};

	/*this.textsTemp = function(LO, scope) {
	switch(LO.metadata.LOTemplate) {
		case 'SR-03-01':
		case 'SG-03-01':
		case 'SY-03-01':
			scope.leftContWidth = 'col-xs-7';
			scope.rightContWidth = 'col-xs-14';
			break;
		case 'SR-03-03':
		case 'SG-03-03':
		case 'SY-03-03':
			scope.leftContWidth = 'col-xs-14';
			scope.rightContWidth = 'col-xs-7';
			break;
	}
};*/


	/*stimLayout = {
		"leftStim" : false,
		"rightStim" : false,
		"leftStimWidth" : "col-xs-21",
		"rightStimWidth" : "col-xs-21",
		"contentWinWidth" : "col-xs-21"
	};
	stimulusPos(pos, stimWidth): Object {
		switch(pos){
			case 'left':
				this.stimLayout.leftStim = true;
				this.stimLayout.rightStim = false;
				this.stimLayout.leftStimWidth = 'col-xs-' + stimWidth;
				this.stimLayout.contentWinWidth = 'col-xs-' + (20 - Number(stimWidth));
				break;
			case 'right':
				this.stimLayout.leftStim = false;
				this.stimLayout.rightStim = true;
				this.stimLayout.rightStimWidth = 'col-xs-' + stimWidth;
				this.stimLayout.contentWinWidth = 'col-xs-' + (20 - Number(stimWidth));
				break;
			default:
				this.stimLayout.leftStim = false;
				this.stimLayout.rightStim = false;
				this.stimLayout.contentWinWidth = 'col-xs-21';
		}
		return this.stimLayout;
	}*/

}
