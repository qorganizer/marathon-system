
export class LoStructure{
	learningObjectId: number;
	title: string;
	time:string;
	sortOrder: number;
	metadata: LoMeta;
	content: LoContent;
	richMedia: LoMedia[] = [];
}

export class LoMeta{
	adaptiveLevel: string;
	adaptiveUse: string;
	compulsory: string;
	linkType: string;
	liveActv: string;
	liveActvType: string;
	marker: string;
	organization: string;
	objective: string;
	pen: string;
	sticky: string;
	tts: string;
	helpingHands: string;
	activity: string;
	target: string;
	output: string;
	teacherInfo: string;
	liveActvContent: string;
	LOColor: string;
	LOTemplate: string;
	instructions: string;
}

export class LoMedia{
	type: string;
	item: string;
	description_link: string;
	text_on_image: string;
	effects: string;
}

export class LoContent {
	key_triggerText: LoContentText;
	key_contentText: LoContentText;
}

export class LoContentText {
	value: string;
}
