import { Component, OnInit }  from '@angular/core';
import { Location }           from '@angular/common';
import { Router }             from '@angular/router';

import { AdjacentLOService }  from './adjacent-lo.service';
import { NavbarService }  from '../navbar/navbar.service';
import { TableOfContent }    from '../navbar/navbar';

@Component({
	selector: 'app-toolbar',
	templateUrl: './toolbar.component.html',
	//styleUrls: ['./toolbar.component.css']
})

export class ToolbarComponent implements OnInit {

	tableOfContent: TableOfContent;

	currLoId = '';
	adjacentLo;

	BackBtnState = true;
	NextBtnState = true;

	HintBtnState = true;
	ExampleBtnState = true;
	HelpBtnState = true;
	StarBtnState = true;
	HintBtnShow = true;
	ExampleBtnShow = true;
	HelpBtnShow = true;
	StarBtnShow = true;

	AnswerBtnState = false;
	ResetBtnState = false;
	CheckBtnState = false;
	AnswerBtnShow = true;
	ResetBtnShow = true;
	CheckBtnShow = true;

	CheckSubmit  = false;
	//NavBtnState = true;

	constructor (
		private adjacentLOService: AdjacentLOService,
		private navbarService: NavbarService,
		private router: Router,
		private location: Location
	) {
		this.router = router;
		this.location = location;
	}

	ngOnInit() {
		this.getNavbarContent();
	}

	getNavbarContent(): void {
		this.navbarService.getNavbarContent().then(tableOfContent => {
			this.tableOfContent = tableOfContent;
			console.log('getNavbarContent::tableOfContent:', this.tableOfContent);
		});
	}

	onClickBackBtn() {
		console.log('BACK button clicked');

		this.currLoId = this.currentLO();
		this.adjacentLo = this.adjacentLOService.getAdjacentLO(this.currLoId, this.tableOfContent);
		console.log('this.adjacentLo.prevLoId:', this.adjacentLo.prevLoId);
		this.currLoId = this.adjacentLo.prevLoId;

		this.BackBtnState = this.adjacentLo.currLoPos > 1;
		this.NextBtnState = true;

		let prevLoRoute = '/' + this.adjacentLo.prevLoTemp + '/' + this.adjacentLo.prevLoId;
		console.log('prevLoRoute:', prevLoRoute);
		this.router.navigate([prevLoRoute]);
	}

	onClickHintBtn() {
		this.HelpBtnState = false;
		console.log('HELP button clicked');
	}

	onClickHelpBtn() {
		this.HelpBtnState = false;
		console.log('HELP button clicked');
	}

	onClickExampleBtn() {
		this.ExampleBtnState = false;
		console.log('HELP button clicked');
	}

	onClickResetBtn() {
		this.ResetBtnState = false;
		console.log('RESET button clicked');
	}

	onClickAnswerBtn() {
		this.AnswerBtnState = false;
		console.log('ANSWER button clicked');
	}

	onClickCheckBtn() {
		this.CheckBtnState = false;
		console.log('CHECK button clicked');
	}

	onClickStarBtn() {
		this.StarBtnState = false;
		console.log('STAR button clicked');
	}

	/*onClickNavBtn() {
		this.NavBtnState = false;
		console.log('NAV button clicked');
	}*/

	onClickNextBtn() {
		console.log('onClickNextBtn::tableOfContent:', this.tableOfContent);

		this.currLoId = this.currentLO();
		this.adjacentLo = this.adjacentLOService.getAdjacentLO(this.currLoId, this.tableOfContent);
		console.log('this.adjacentLo.nextLoId:', this.adjacentLo.nextLoId);
		this.currLoId = this.adjacentLo.nextLoId;

		this.NextBtnState = !this.adjacentLo.beforeLast;
		this.BackBtnState = true;

		let nextLoRoute = '/' + this.adjacentLo.nextLoTemp + '/' + this.adjacentLo.nextLoId;
		console.log('nextLoRoute:', nextLoRoute);
		this.router.navigate([nextLoRoute]);

	}

	currentLO(): string {
		let currentUrl = this.location.path();
		return currentUrl.substr(currentUrl.lastIndexOf('/') + 1);
	}
}
