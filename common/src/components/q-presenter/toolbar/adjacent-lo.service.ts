
import { Injectable } from '@angular/core';


@Injectable()
export class AdjacentLOService {

	getAdjacentLO(currLoId, tableOfContent): object {
		console.log('getAdjacentLO::tableOfContent:', tableOfContent);
		let adjacentLo = {"currTask": "", "prevLoId": "", "nextLoId": "", "prevLoTemp": "", "nextLoTemp": "", "currLoPos": 0, "beforeLast": false};
		let currTask;
		console.log('tableOfContent.tasks.length:', tableOfContent.tasks.length);
		for(let i = 0; i < tableOfContent.tasks.length; i++){
			currTask = tableOfContent.tasks[i];
			//if(currTask.taskId == currLo.currTaskId){
				console.log('tableOfContent.tasks[i].los.length:', tableOfContent.tasks[i].los.length);
				for(let j = 0; j < currTask.los.length; j++){
					if(currTask.los[j].loId == currLoId){
						adjacentLo.currTask = currTask.taskId;
						adjacentLo.currLoPos = j;
						adjacentLo.beforeLast = j == currTask.los.length - 2;
						adjacentLo.nextLoId = j == currTask.los.length - 1 ? "undefined" : currTask.los[j+1].loId;
						adjacentLo.prevLoId = j > 0 ? currTask.los[j-1].loId : "undefined";
						adjacentLo.nextLoTemp = j == currTask.los.length - 1 ? "undefined" : currTask.los[j+1].loTemp;
						adjacentLo.prevLoTemp = j > 0 ? currTask.los[j-1].loTemp : "undefined";
						break;
					}
				}
			//}
		}
		console.log('AdjacentLOService::adjacentLo:', adjacentLo);
		return adjacentLo;
	}

}

