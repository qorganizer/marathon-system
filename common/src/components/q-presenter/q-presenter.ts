import { Component, Input, Output, EventEmitter, AfterViewInit, OnChanges, SimpleChanges } from "@angular/core";
import { LearningObject, UserAction, QuizProgress, TableOfContents, SessionState } from "../../model";
/**
 * <p>This is the placeholder for Q-Presenter. The Q-Presenter component received needed data as input and outputs event to trigger action to be taken like saving a response, updating user activity.</p>
 * <p>Dev guidelines:
 * <pre>
 * 	- Input required by the component to be designed as fields annotated with the @Input decorator
 *  - Output to be provided by the component is to be designed as EventEmitter fields annotated with the @Output decorator.
 *  - The component does not need to handle calling services to get/persist content. All the interaction needs to be required via input/output fields
 * </pre></p>
 * <p>Some inputs and outputs have already been specified for convenince a code sample. Feel free to add as neeeded.</p>
 * @class QPresenter
 * 
 */
@Component({
	selector: "q-presenter",
	templateUrl: "q-presenter.html"
})
export class QPresenter implements AfterViewInit, OnChanges {
	/**
	 * The learning objects to be used for display. All LOs for this toc will come as an array. TOC will come first as input allowing for display of the table for contents if needed and the LOs will come in after that.
	 */
	@Input() los: LearningObject[];

	/**
	 * The current state containing the LO to be displayed amongst posbly other things
	 */
	@Input() currentState: SessionState;
	/**
	 * The table of contents input
	 */
	@Input() toc: TableOfContents;
	/**
	 * Event triggered when the user performs various actions in the compoonent like move to a new page, start a quiz, end a quiz, etc
	 * Trigerring an event is done like:
	 * <code>this.userActionPerformed.emit(new UserAction())</code>
	 * @event QPresenter#userActionPerformed
	 * @type {UserAction}
	 */
	@Output() userActionPerformed: EventEmitter<UserAction> = new EventEmitter<UserAction>();
	/**
	 * Event triggered when the user progresses through a quiz and the give response must be saved
	 * Trigerring an event is done like:
	 * <code>this.quizProgress.emit(new QuizProgress())</code>
	 * @event QPresenter#quizProgress
	 * @type {QuizProgress}
	 */
	@Output() quizProgress: EventEmitter<QuizProgress> = new EventEmitter<QuizProgress>();

	constructor() {
		
	}

	ngAfterViewInit(){
		//called after the component and all it's children have inited. Input decorated fields are also ready
		//Add you initialization code here.
	}

	ngOnChanges(changes: SimpleChanges){
		console.log(changes);
		//If you need to track changes to the Input fields and react to them you need to do it here
		// For example we detect changes to the list of LOs
		if(changes.los && changes.los.currentValue){
			//Do something with changes.los.currentValue about which we know is not null and different from the previous value
		}
		if(changes.toc && changes.toc.currentValue){
			//Do something with changes.toc.currentValue about which we know is not null and different from the previous value
		}
	}

	

}
