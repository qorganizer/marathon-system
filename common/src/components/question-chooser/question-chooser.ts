import { Component } from "@angular/core";
import { Session, QuestionType, SessionMode, TableOfContents, SessionState, LearningObject, UserAction, QuizProgress } from "../../model";
import { RESTClient, ModelStore, SocketClient } from "../../services";
import { AlertController, ActionSheetController, NavParams, ViewController } from "ionic-angular";

@Component({
	selector: "question-chooser",
	templateUrl: "question-chooser.html"
})
export class QuestionChooser {

	questionTypes: QuestionType[];
	
	constructor(private client: RESTClient,
				private ws: SocketClient,
				private model: ModelStore,
				private alertCtrl: AlertController,
				private sheetCtrl: ActionSheetController,
				private params: NavParams,
				private viewCtrl: ViewController) {
	}

	ngAfterContentInit(){
		this.questionTypes = QuestionType.asArray(this.params.get("type"));
	}

	close(type: QuestionType) {
		this.viewCtrl.dismiss(type);
	}
}