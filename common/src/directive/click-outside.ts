import {Directive, OnInit, OnDestroy, Output, EventEmitter, ElementRef, HostListener} from '@angular/core';
import {Observable, Subscription} from 'rxjs/Rx';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/do';

@Directive({
  selector: '[click-outside]'
})

export class ClickOutside implements OnDestroy {
  private listening:boolean;
  private globalClick:Subscription;

  @Output('clickOutside') clickOutside:EventEmitter<Object>; 

  @HostListener('click', ['$event']) onClick($event){
	  if(!this.globalClick){
		this.init();
	  }
  }

  constructor(private _elRef:ElementRef) {
    this.listening = false;
    this.clickOutside = new EventEmitter();
  }

  init() {
    this.globalClick = Observable
      .fromEvent(document, 'click')
      .delay(1)
      .do(() => {
        this.listening = true;
      }).subscribe((event:MouseEvent) => {
        this.onGlobalClick(event);
      });
  }
  
  ngOnDestroy() {
	  if(this.globalClick){
		this.globalClick.unsubscribe();
	  }
  }

  onGlobalClick(event:MouseEvent) {
    if (event instanceof MouseEvent && this.listening === true) {
      if(this.isDescendant(this._elRef.nativeElement, event.target) === true) {
		//do nothing
      } else {
        this.clickOutside.emit({
          target: (event.target || null),
          value: true
		});
		this.globalClick.unsubscribe();
		this.globalClick = null;
      }
    }
  }

  isDescendant(parent, child) {
    let node = child;
    while (node !== null) {
      if (node === parent) {
        return true;
      } else {
        node = node.parentNode;
      }
    }
    return false;
  }
}