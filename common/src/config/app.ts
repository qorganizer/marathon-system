import { SSOServiceManager } from '../services/sso-manager';
import { Storage } from '@ionic/storage';
import { ModelStore } from '../services/model-store';
import { Injectable, Inject } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { ToastController } from "ionic-angular";
import { RESTClient, SocketClient, BusinessService, OAuthService } from "../services";
import { Observable, Observer} from "rxjs/Rx";

@Injectable()
export class AppConfigurer {
	private config;

	private lang: string = "en";

	constructor(private translate: TranslateService,
				private rest: RESTClient,
				private sock: SocketClient,
				private client: BusinessService,
				private toastCtrl: ToastController,
				private oauth: OAuthService,
				private store: ModelStore, 
				private storage: Storage,
				private sso: SSOServiceManager) {

	}

	public initializeWithConfig(config) {
		this.config = config;
		this.store.hash = window.location.hash;
		this.setupServices();
	}

	public isAuthenticated(): Observable<boolean>{
		return this.client.isAuthenticated();
	}

	public isOauthCallback(): boolean{
		return this.client.isOauthCallback();
	}

	public shouldAuthenticate(): Observable<boolean>{
		return new Observable<boolean>(obs => {
			this.client.isAuthenticated().subscribe(isAuthenticated => {
				let shouldLogin = this.sso.manageSSO(isAuthenticated, this.oauth)
				if(shouldLogin != null){
					obs.next(shouldLogin);
					obs.complete();
				}
			})	
		});
	}

	private setupServices() {
		this.translate.setDefaultLang(this.lang);
		this.translate.use(this.lang);
		this.rest.setRestRoot(this.config.restUrl);
		this.sock.setSocketUrl(this.config.socketUrl);
		this.rest.error.subscribe((err) => {
			if(!err){
				return;
			}
			let obj = err.json && err._body && err.json(); 
			this.makeMessage(obj).subscribe(message => {
				let toast = this.toastCtrl.create({
					message: message,
					duration: 4000,
					cssClass: "error",
					position: 'top'
				});
				toast.present();
			});			
		});
		this.oauth.initialize(this.config.oauth);
	}

	public reInitOauth(){
		this.oauth.initialize(this.config.oauth);
	}

	private makeMessage(obj): Observable<string>{
		return new Observable<string>(obs => {
			let message = "An error occured";
			if(obj){
				if(Array.isArray(obj)){
					let index = 0;
					message = "";
					let handle = (value) => {
						if(value){
							message += value;
						}
						if(index < obj.length - 1){
							message += "\n";
						}
						if(index == obj.length-1){							
							obs.next(message);
							obs.complete();
							return;
						}
						index++;
						this.translate.get("errors." + obj[index]).subscribe(handle);
					}
					this.translate.get("errors." + obj[index]).subscribe(handle);
				}else if(obj && obj.message){
					obs.next(message);
					obs.complete();
					return;
				}
			}
			obs.next(message);
			obs.complete();
		});		
	}
}