import { ModelStore } from '../../../common/src/services/model-store';
import { Injectable } from "@angular/core";
import { Storage } from '@ionic/storage';
import { Observable, Observer, BehaviorSubject } from "rxjs/Rx";
import { ApplicationConfig } from "../app/app.config";
import 'expose-loader?AuthenticationContext!adal-angular/lib/adal.js';
let createAuthContextFn: adal.AuthenticationContextStatic = AuthenticationContext;

window["Logging"] = {
	level: 3,
	log: function (message) {
		console.log(message);
	}
};

@Injectable()
export class ADALService{
    
	private context: adal.AuthenticationContext;

	constructor(private storage: Storage, private store: ModelStore){
		
	}
	
	initialize(config: adal.Config){
		AuthenticationContext.prototype._singletonInstance = null;
		console.log("Initializing ADAL");
		let s = this.storage;
		config["callback"] = this.handleCallback.bind(this);
		config.redirectUri = config.redirectUri || window.location.origin;
		config.postLogoutRedirectUri = config.postLogoutRedirectUri || window.location.origin;
		this.context = new createAuthContextFn(config);
		this.context.handleWindowCallback();
	}

	private handleCallback(errorDesc: string, token: string, error: string) {
		this.storage.ready().then(()=>{
			this.storage.set("auth-token", "Azure_ID " + token);
			this.storage.set("auth-error-desc", errorDesc);
			this.storage.set("auth-error", error);
		});
	}

	signIn(): Observable<any>{
		return new Observable<any>( obs => {
			this.context.login();
			//no need for a complete call as this will redirect
		});
	}

	signOut(){
		this.context.logOut();
	}

	isOauthEnabled(): Observable<boolean>{
		return Observable.of(this.context.getCachedUser() != null);
	}

	isAuthenticated(): Observable<boolean>{
		return Observable.of(this.context.getCachedToken(ApplicationConfig.oauth.loginResource)!= null);
	}

	isCallback(): boolean{
		return this.context.isCallback(this.store.hash);
	}
}