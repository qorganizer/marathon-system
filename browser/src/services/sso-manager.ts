import { Storage } from '@ionic/storage';
import { UserProfile } from '../../../common/src/model/user-profile';
import { OAuthService } from '../../../common/src/services/oauth-service';
import { ModelStore } from '../../../common/src/services/model-store';
import { Component, Inject, Injectable, ViewChild } from "@angular/core";
import { Platform, Nav } from "ionic-angular";
import { Observable, Observer } from "rxjs/Rx";

import { APP_CONFIG, AppConfig, configHasRedirect } from "../app/app.config";

@Injectable()
export class SSOManager {
	private redirectKey: string = "";
	private redirectParam: string = "";

	constructor( @Inject(APP_CONFIG) private config: AppConfig,
				private model: ModelStore,
				private platform: Platform,
				private storage: Storage) {
	}

	public manageSSO(isAuthenticated: boolean, oauth: OAuthService): boolean {
		let isADALRedirect = false;
		this.redirectKey = this.getRedirectDestination();
		isADALRedirect = this.redirectKey && this.model.hash.indexOf("id_token") > -1;
		if (isADALRedirect) {
			this.redirectToSSOEndpoint(true);
			return null;
		}
		if(isAuthenticated){
			return false;
		}
		if (this.redirectKey) {
			this.model.ssoLogin = true;
			this.model.ssoEndpoint = this.redirectKey;
			return true;
		}
		this.redirectParam = this.platform.getQueryParam("redirect");
		
		if (configHasRedirect(this.redirectParam)) {
			this.config.oauth.redirectUri = window.location.origin + "/" + this.redirectParam;
			oauth.initialize(this.config.oauth);
		}
		
		return true;
	}

	public handleLogin(profile: UserProfile): boolean{
		if(this.redirectParam && configHasRedirect(this.redirectParam)){
			this.redirectToSSOEndpoint(false);
		}else{
			return true;
		}
	}

	private getRedirectDestination(): string{
		let k = "";
		Object.keys(this.config.redirects).forEach(key => {
			window.location.pathname.indexOf(key) > -1 ? k = key : key;
		});
		return k;
	}

	private redirectToSSOEndpoint(isOauth: boolean){
		let token = "";
		if(isOauth){
			let entries = this.model.hash.substr(1).split("&");
			entries.forEach(entry => {
				let map = entry.split("=");
				if(map[0] == "id_token"){
					token = map[1];
				}
			});
			window.location.href = window.location.origin + this.config.redirects[this.redirectKey] + "?q-token=Azure_ID " + token;
		}else{
			this.storage.get("auth-token").then(token => {
				window.location.href = window.location.origin + this.config.redirects[this.redirectParam] + "?q-token=" + token;
			});
		}
	}
}