import { Component, Inject, ViewChild } from "@angular/core";
import { App, Nav, Platform } from 'ionic-angular';

import {Login, Main} from "../../../common/src/views";
import {AppConfigurer} from "../../../common/src/config/app";
import { APP_CONFIG, AppConfig, configHasRedirect } from "./app.config";
import {QNavigator} from "../../../common/src/services";
@Component({
	templateUrl: "app.html"
})
export class MyApp {

	constructor(private platform: Platform, 
				private navigator: QNavigator,
				@Inject(APP_CONFIG) private config: AppConfig,
				private setupper: AppConfigurer,
				private navCtrl: App) {
		this.setupper.initializeWithConfig(this.config);
		platform.ready().then(() => {
			 //do something else
			 //
		});
	}

	ngAfterViewInit(){ 
		this.setupper.shouldAuthenticate().subscribe((shouldLogin) => {
			let promise: Promise<any> = null;
			if (shouldLogin) {
				promise = this.navigator.setRoot(this.navCtrl.getRootNav(), Login);
			} else {
				promise = this.navigator.setRoot(this.navCtrl.getRootNav(), Main);
			}
			promise.then(data => {
				//all good
			}, (error) => {
				console.log(error);
			})
		});
	}
}
