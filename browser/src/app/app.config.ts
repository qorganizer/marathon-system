import { InjectionToken } from "@angular/core";
export interface AppConfig{
	restUrl: any;
	socketUrl: any;
	i18n_prefix: any;
	i18n_suffix: any;
	redirects: any;
	oauth: any;

}
export let APP_CONFIG = new InjectionToken<AppConfig>("app.config");
export const ApplicationConfig:AppConfig = {"restUrl":"https://52.174.178.62/api","socketUrl":"https://52.174.178.62/qplayer","i18n_prefix":"https://52.174.178.62/assets/i18n/","i18n_suffix":".json","redirects":{"q-organizer":"/api/sso/q-organizer"},"oauth":{"tenant":"common","clientId":"114746ca-c10d-49d2-aa33-ed916cec854c","redirectUri":"","navigateToLoginRequestUrl":false,"postLogoutRedirectUri":"","cacheLocation":"localStorage","loginResource":"https://graph.windows.net/"}};
export function configHasRedirect(name: string){
	return ApplicationConfig.redirects && ApplicationConfig.redirects[name];
}
