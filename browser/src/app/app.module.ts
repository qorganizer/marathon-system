import { NgModule, ErrorHandler, isDevMode } from "@angular/core";
import { HttpModule, Http } from "@angular/http";
import { BrowserModule } from "@angular/platform-browser";
import { IonicApp, IonicModule, IonicErrorHandler, DeepLinkConfig } from "ionic-angular";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { MyApp } from "./app.component";

import { SocketStompConfig, SockJStompService } from "../../../common/src/services/socket";
import { AppConfigurer } from "../../../common/src/config/app";
import { RESTClient, SocketClient, ModelStore, QNavigator, BusinessService, OAuthService, SSOServiceManager } from "../../../common/src/services";
import { IonicStorageModule } from '@ionic/storage';
import { APP_CONFIG, ApplicationConfig } from "./app.config";
import { providers, components } from "../../../common/src/common.module";

import { SessionsTeacher } from "../../../common/src/views/sessions/sessions.teacher";
import { SessionsStudent } from "../../../common/src/views/sessions/sessions.student";
import { SessionsAdmin } from "../../../common/src/views/sessions/sessions.admin";
import { SessionTeacher } from "../../../common/src/views/session/session.teacher";
import { SessionStudent } from "../../../common/src/views/session/session.student";
import { SessionAdmin } from "../../../common/src/views/session/session.admin";
import { SessionAttendance } from "../../../common/src/views/session-attendance";
import { DashboardTeacher } from "../../../common/src/views/dashboard/dashboard.teacher";
import { DashboardStudent } from "../../../common/src/views/dashboard/dashboard.student";
import { DashboardAdmin } from "../../../common/src/views/dashboard/dashboard.admin";
import { Login, Main, CoursesView, CoursesSelectView, CourseView, BlockView, Register, Users, CreateSessionView, UserView, SessionDisplay, ActivitiesView, ActivityEditView, TrueFalseView, SingleChoiceView, MultipleChoiceView, SliderView, RubricView, PollView, LikeDislikeView, ChecklistView, TrueFalseExamView, SingleChoiceExamView, MultipleChoiceExamView, SliderExamView, RubricExamView,	PollExamView, LikeDislikeExamView, ChecklistExamView,ActivityDashboardView, ActivityExamView, ActivityDashboardQuestionView, ActivityDashboardQuizView } from "../../../common/src/views";
import { QPresenter } from "../../../common/src/components/q-presenter";
import { QuestionChooser } from "../../../common/src/components/question-chooser";
import { SessionMenu } from "../../../common/src/components/session-menu";

import { ADALService } from "../services/adal-service";
import { SSOManager } from "../services/sso-manager";
import { ActivityChooser } from "../../../common/src/components/activity-chooser/index";
import { ClickOutside } from "../../../common/src/directive";


/*Marathon*/
import { ToolbarComponent }   from '../../../common/src/components/q-presenter/toolbar/toolbar.component';
import { NavbarComponent }    from '../../../common/src/components/q-presenter/navbar/navbar.component';

import { LosModule }          from '../../../common/src/components/q-presenter/los/los.module';
import { AppRoutingModule }   from '../../../common/src/components/q-presenter/app-routing.module';

import { NavbarService }      from "../../../common/src/components/q-presenter/navbar/navbar.service";
import { LoService }          from "../../../common/src/components/q-presenter/los/los.service";
import { AdjacentLOService }  from '../../../common/src/components/q-presenter/toolbar/adjacent-lo.service';



export const deepLinkConfig: DeepLinkConfig = {
	links: [
		{ component: Login, name: "Login Page", segment: "login" },
		{ component: Register, name: "Register Page", segment: "register" },
		{ component: Users, name: "Users Page", segment: "users" },
		{ component: CoursesView, name: "Courses Page", segment: "courses" },
		{ component: CourseView, name: "Course Page", segment: "courses/:cid", defaultHistory: [CoursesView] },
		{ component: BlockView, name: "Course Page", segment: "courses/:cid/blocks/:bid", defaultHistory: [CoursesView, CourseView] },
		{ component: DashboardAdmin, name: "Home Page", segment: "home" },
		{ component: DashboardTeacher, name: "Home Page", segment: "home" },
		{ component: DashboardStudent, name: "Home Page", segment: "home" },
		{ component: SessionsAdmin, name: "Sessions Page", segment: "sessions" },
		{ component: SessionsTeacher, name: "Sessions Page", segment: "sessions" },
		{ component: SessionsStudent, name: "Sessions Page", segment: "sessions" },
		{ component: SessionAdmin, name: "Session Page", segment: "sessions/:sid" },
		{ component: SessionTeacher, name: "Session Page", segment: "sessions/:sid" },
		{ component: SessionStudent, name: "Session Page", segment: "sessions/:sid" },
		{ component: ActivitiesView, name: "Quizes Page", segment: "quizes" },
		{ component: SessionAttendance, name: "Attendance", segment: "attendance"},
		{ component: SessionDisplay, name: "Session Display", segment: "presenter" }
	]
};

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: Http) {
	return new TranslateHttpLoader(http, ApplicationConfig.i18n_prefix, ApplicationConfig.i18n_suffix);
}

let cmps = <any[]>[
	MyApp,
	Login,
	Main,
	CoursesView,
	CourseView,
	Register,
	BlockView,
	Users,
	QPresenter,
	DashboardStudent,
	DashboardTeacher,
	DashboardAdmin,
	SessionsTeacher,
	SessionsStudent,
	SessionsAdmin,
	SessionTeacher,
	SessionStudent,
	SessionAdmin,
	CreateSessionView,
	UserView,
	SessionAttendance,
	SessionDisplay,
	ActivitiesView,
	ActivityEditView,
	TrueFalseView,
	SingleChoiceView,
	MultipleChoiceView,
	SliderView,
	RubricView,
	PollView,
	LikeDislikeView,
	ChecklistView,
	TrueFalseExamView,
	SingleChoiceExamView,
	MultipleChoiceExamView,
	SliderExamView,
	RubricExamView,
	PollExamView,
	LikeDislikeExamView,
	ChecklistExamView,
	QuestionChooser,
	ActivityChooser,
	ActivityDashboardView,
	ActivityExamView,
	ActivityDashboardQuestionView,
	ActivityDashboardQuizView,
	SessionMenu,
	CoursesSelectView,
	/*Marathon*/
	ToolbarComponent,
	NavbarComponent
];

let provs = [
	{ provide: ErrorHandler, useClass: IonicErrorHandler },
	{ provide: OAuthService, useClass: ADALService },
	{
		provide: APP_CONFIG,
		useValue: ApplicationConfig
	},
	{provide: SSOServiceManager, useClass: SSOManager},
	RESTClient,
	SocketClient,
	ModelStore,
	QNavigator,
	AppConfigurer,
	BusinessService,
	/*Marathon*/
	NavbarService,
	LoService,
	AdjacentLOService
];

@NgModule({
	declarations: cmps.concat(ClickOutside),
	imports: [
		BrowserModule,
		IonicModule.forRoot(MyApp, {
			locationStrategy: 'path'
		}, deepLinkConfig),
		IonicStorageModule.forRoot({
			name: 'qdb',
			storeName: "auth",
			driverOrder: ['localstorage', 'indexeddb', 'websql', 'sqlite']
		}),
		HttpModule,
		TranslateModule.forRoot({
			loader: {
				provide: TranslateLoader,
				useFactory: HttpLoaderFactory,
				deps: [Http]
			}
		}),
		/*Marathon*/
		LosModule,
		AppRoutingModule
	],
	bootstrap: [IonicApp],
	entryComponents: cmps,
	providers: provs
})
export class AppModule { }
