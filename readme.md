## Q mobile and web client

This is the client built for the Q Player server.

# Prerequisites

* **NodeJS** (>= 6) - https://nodejs.org/en/download/
* *Android SDK (optional)* - to deploy the mobile version to the Android simulator


# Setup
* install cordova, ioniv typescript:   
 ```npm install -g cordova ionic typescript```
* install modules:   
```npm install```

You're ready to go.

# Commands
* ```install``` -  Install the required modules for web and mobile client projects
* ```serve:browser``` -  Serve the web client in a local dev server
* ```build:browser``` -  Build the development distribution of the web client
* ```build:browser:prod``` -   Build the production distribution of the web client
* ```emulate``` -  Emulate the mobile client in Android simulator
* ```build:mobile``` -  Build development apk of Android mobile client 
* ```build:mobile:prod``` -  Build production apk of Android mobile client 
* ```build``` -  Build the development distribution of the web client and Android mobile client
* ```build:prod"``` -  Build the production distribution of the web client and Android mobile client

To go on development with live reload use ```npm run serve:browser```.